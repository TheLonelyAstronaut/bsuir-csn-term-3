#include "PC.h"
#include <iostream>
#include <string>
#include <iomanip>
#include "InputSizeException.h"
using namespace std;

string PC::GetCase()
{
	return this->Case;
}

void PC::SetCase(string _case)
{
	this->Case = _case;
}

istream& operator>>(istream& in, PC& object)
{
	in >> static_cast<StationaryMachine&>(object);
	cout << "Case: ";
	Exception::isString(object.Case);

	return in;
}

ostream& operator<<(ostream& out, const PC& object)
{
	out << static_cast<const StationaryMachine&>(object);
	out << setw(15) << object.Case << endl;
	return out;
}

void PC::printHeader()
{
	cout << setw(15) << left << "Manufacturer"
		<< setw(15) << "CPU"
		<< setw(15) << "GPU"
		<< setw(15) << "RAM"
		<< setw(15) << "Power Supply"
		<< setw(15) << "Case"
		<< endl;
}