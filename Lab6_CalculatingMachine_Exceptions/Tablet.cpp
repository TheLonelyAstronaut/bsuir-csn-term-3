#include "Tablet.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
using namespace std;

bool Tablet::GetWithCellurarModule()
{
	return this->WithCellurarModule;
}

void Tablet::SetWithCellurarModule(bool _withcellurarmodule)
{
	this->WithCellurarModule = _withcellurarmodule;
}

istream& operator>>(istream& in, Tablet& object)
{
	string exOnly;

	in >> static_cast<PortableMachine&>(object);
	cout << "With Cellurar Module (yes/no): ";
	Exception::isString(exOnly, 3);
	if (exOnly == "yes" || exOnly == "Yes") object.WithCellurarModule = true;
	else object.WithCellurarModule = false;

	return in;
}

ostream& operator<<(ostream& out, const Tablet& object)
{
	out << static_cast<const PortableMachine&>(object);
	out << setw(15) << (object.WithCellurarModule ? "Yes" : "No") << endl;
	return out;
}

void Tablet::printHeader()
{
	cout << setw(15) << left << "Manufacturer"
		<< setw(15) << "CPU"
		<< setw(15) << "GPU"
		<< setw(15) << "RAM"
		<< setw(15) << "Battery Capacity"
		<< setw(15) << "Touchable"
		<< setw(15) << "With Cellurar"
		<< endl;
}