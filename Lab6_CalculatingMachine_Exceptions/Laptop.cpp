#include "Laptop.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
using namespace std;

string Laptop::GetKeyboardType()
{
	return this->KeyboardType;
}

void Laptop::SetKeyboardType(string _KeyboardType)
{
	this->KeyboardType = _KeyboardType;
}

istream& operator>>(istream& in, Laptop& object)
{
	in >> static_cast<PortableMachine&>(object);
	cout << "KeyboardType: ";
	Exception::isString(object.KeyboardType);

	return in;
}

ostream& operator<<(ostream& out, const Laptop& object)
{
	out << static_cast<const PortableMachine&>(object);
	out << setw(15) << object.KeyboardType << endl;
	return out;
}

void Laptop::printHeader()
{
	cout << setw(15) << left << "Manufacturer"
		<< setw(15) << "CPU"
		<< setw(15) << "GPU"
		<< setw(15) << "RAM"
		<< setw(20) << "Battery Capacity"
		<< setw(15) << "Touchable"
		<< setw(15) << "Keyboard Type"
		<< endl;
}