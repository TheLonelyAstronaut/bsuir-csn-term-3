#include <iostream>
#include "Monoblock.h"
#include "PC.h"
#include "Tablet.h"
#include "Laptop.h"
#include "ComputingMachine.h"
#include "InputSizeException.h"
#include "ArgumentOutOfRangeException.h"
#include <iomanip>
using namespace std;

int main()
{
	Laptop* array = new Laptop[3];
	
	for (int i = 0; i < 3; ++i) cin >> array[i];
	
	Laptop::printHeader();
	
	for (int i = 0; i < 3; ++i) cout << array[i];

	return 0;
}