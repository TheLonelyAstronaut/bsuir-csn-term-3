#pragma once
#include "StationaryMachine.h"
#include <iostream>
#include <string>
using namespace std;

class Monoblock : public StationaryMachine
{
protected:
	string Touch;
public:
	Monoblock(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, string _powersupply = "", string _touch = "") :
		StationaryMachine(_manufacturer, _cpu, _gpu, _ram, _powersupply, true),
		Touch(_touch) {
		cout << "Monoblock Constructor" << endl << endl;
	}
	~Monoblock() { cout << "Monoblock Destructor" << endl; }
	static void printHeader();
public:
	string GetTouch();
public:
	void SetTouch(string _touch);
public:
	friend istream& operator>>(istream& in, Monoblock& object);
	friend ostream& operator<<(ostream& out, const Monoblock& object);
};

