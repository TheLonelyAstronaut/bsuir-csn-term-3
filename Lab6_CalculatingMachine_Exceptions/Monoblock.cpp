#include "Monoblock.h"
#include "ComputingMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
using namespace std;

string Monoblock::GetTouch()
{
	return this->Touch;
}

void Monoblock::SetTouch(string _touch)
{
	this->Touch = _touch;
}

istream& operator>>(istream& in, Monoblock& object)
{
	in >> static_cast<StationaryMachine&>(object);
	cout << "Touch: ";
	Exception::isString(object.Touch);

	return in;
}

ostream& operator<<(ostream& out, const Monoblock& object)
{
	out << static_cast<const StationaryMachine&>(object);
	out << setw(15) << object.Touch << endl;
	return out;
}

void Monoblock::printHeader()
{
	cout << setw(15) << left << "Manufacturer"
		<< setw(15) << "CPU"
		<< setw(15) << "GPU"
		<< setw(15) << "RAM"
		<< setw(15) << "Power Supply"
		<< setw(15) << "Touch"
		<< endl;
}