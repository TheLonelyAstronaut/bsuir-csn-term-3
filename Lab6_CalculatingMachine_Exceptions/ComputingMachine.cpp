#include "ComputingMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
#include "ArgumentOutOfRangeException.h"
using namespace std;

istream& operator>>(istream& in, ComputingMachine& object)
{
	string exOnly;

	cout << "Manufacturer: ";
	Exception::isString(object.Manufacturer, 80);

	cout << "CPU: ";
	Exception::isString(object.CPU, 80);

	cout << "GPU: ";
	Exception::isString(object.GPU, 80);

	cout << "RAM: ";
	Exception::isNumber(object.RAM, 256, 0);


	return in;
}

ostream& operator<<(ostream& out, const ComputingMachine& object)
{
	out << setw(15) << object.Manufacturer;
	out << setw(15) << object.CPU;
	out << setw(15) << object.GPU;
	out << setw(15) << object.RAM;
	return out;
}