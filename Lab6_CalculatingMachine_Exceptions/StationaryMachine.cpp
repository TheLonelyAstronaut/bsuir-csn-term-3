#include "StationaryMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
using namespace std;

istream& operator>>(istream& in, StationaryMachine& object)
{
	in >> static_cast<ComputingMachine&>(object);
	cout << "PowerSupply: ";
	Exception::isString(object.PowerSupply);

	return in;
}

ostream& operator<<(ostream& out,const StationaryMachine& object)
{
	out << static_cast<const ComputingMachine&>(object);
	out << setw(15) << object.PowerSupply;
	return out;
}