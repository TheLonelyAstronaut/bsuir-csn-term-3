#include "InvalidInputException.h"
#include <string>
using namespace std;

InvalidInputException::InvalidInputException(string _input, string _message) : input(_input), Exception(_message) {}

InvalidInputException::~InvalidInputException() {}

string InvalidInputException::Input() { return input; }