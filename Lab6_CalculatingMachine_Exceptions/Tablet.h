#pragma once
#include "Tablet.h"
#include "PortableMachine.h"
#include <iostream>
#include <string>
using namespace std;

class Tablet : public PortableMachine
{
protected:
	bool WithCellurarModule;
public:
	Tablet(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, int _batterycapacity = 0, bool _withcellurarmodule = false) :
		PortableMachine(_manufacturer, _cpu, _gpu, _ram, _batterycapacity, true),
		WithCellurarModule(_withcellurarmodule) {
		cout << "Tablet Constructor" << endl << endl;
	}
	~Tablet() { cout << "Tablet Destructor" << endl; }
	static void printHeader();
public:
	bool GetWithCellurarModule();
public:
	void SetWithCellurarModule(bool _withcellurarmodule);
public:
	friend istream& operator>>(istream& in, Tablet& object);
	friend ostream& operator<<(ostream& out, const Tablet& object);
};

