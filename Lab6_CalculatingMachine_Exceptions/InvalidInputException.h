#pragma once
#include "Exception.h"
#include <string>
using namespace std;

class InvalidInputException : public Exception
{
protected:
	string input;
public:
	InvalidInputException(string _input = "", string _message = "");
	virtual ~InvalidInputException();
	string Input();
};

