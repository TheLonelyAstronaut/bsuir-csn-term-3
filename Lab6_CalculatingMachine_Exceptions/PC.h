#pragma once
#include "StationaryMachine.h"
#include <iostream>
#include <string>
using namespace std;

class PC : public StationaryMachine
{
protected:
	string Case;
public:
	PC(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, string _powersupply = "", string _case = "") :
		StationaryMachine(_manufacturer, _cpu, _gpu, _ram, _powersupply, true),
		Case(_case) {
		cout << "PC Constructor" << endl << endl;
	}
	~PC() { cout << "PC Destructor" << endl; }
	static void printHeader();
public:
	string GetCase();
public:
	void SetCase(string _case);
public:
	friend istream& operator>>(istream& in, PC& object);
	friend ostream& operator<<(ostream& out, const PC& object);
};

