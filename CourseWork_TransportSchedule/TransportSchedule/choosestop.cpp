#include "choosestop.h"
#include "ui_choosestop.h"
#include "schedulewindowwidget.h"
#include "stop.h"

ChooseStop::ChooseStop(ScheduleWindowWidget *parent, QList<Stop> stops) :
    QWidget(parent),
    ui(new Ui::ChooseStop),
    stops(stops),
    window(parent)
{
    ui->setupUi(this);
    //ui->nextStop->resize(100,100);
    ui->nextStop->setCursor(QCursor(Qt::PointingHandCursor));
    ui->previousStop->setCursor(QCursor(Qt::PointingHandCursor));
    this->window->to = false;
    on_directionButton_clicked();
    ui->directionButton->setCursor(QCursor(Qt::PointingHandCursor));
    //setStop();
}

void ChooseStop::setStop()
{
    ui->stopName->setText(stops[currentStop].name());
    //if(this->window->editButton) this->window->editButton->setText("Редактировать");
    //if(this->window->table) this->window->table->browseMode();
    this->window->nextSchedule(this->window->generateScheduleTableByStopId(currentStop));
}

ChooseStop::~ChooseStop()
{
    delete ui;
}

void ChooseStop::on_nextStop_clicked()
{
    if((this->window->to && currentStop == stops.size()-1) || (!this->window->to && currentStop == 0)) return;

    if(this->window->to) currentStop++;
    else currentStop--;

    setStop();
}

void ChooseStop::on_previousStop_clicked()
{
    if((!this->window->to && currentStop == stops.size()-1) || (this->window->to && currentStop == 0)) return;

    if(this->window->to) currentStop--;
    else currentStop++;

    setStop();
}

void ChooseStop::on_directionButton_clicked()
{
    if(!this->window->to)
    {
        ui->directionButton->setText("В сторону " + (--stops.end())->name());
        this->window->to = true;
        currentStop = 0;
        setStop();
        //qDebug() << this->window->generateScheduleTableByStopId(stops.length()-1);
        //this->window->nextSchedule(this->window->generateScheduleTableByStopId(stops.length()-1));
    }
    else
    {
        ui->directionButton->setText("В сторону " + (stops.begin())->name());
        this->window->to = false;
        currentStop = stops.length() - 1;
        setStop();
    }
}
