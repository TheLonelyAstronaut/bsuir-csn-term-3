#ifndef OILVEHICLE_H
#define OILVEHICLE_H
#include "vehicle.h"

class OilVehicle : public Vehicle
{
protected:
    int engineVolume;
public:
    OilVehicle();
    OilVehicle(Route route, int _humanCapacity = 0, QString _model = "", int _engineVolume = 0, int _vehicleId = 0);
    void setEngineVolume(int _engineVolume);
    int getEngineVolume();
    virtual ~OilVehicle() {}
    friend bool operator==(OilVehicle& lhs, OilVehicle& rhs);
};

#endif // OILVEHICLE_H
