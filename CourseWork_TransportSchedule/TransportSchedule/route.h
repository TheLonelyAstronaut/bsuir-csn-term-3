#ifndef ROUTE_H
#define ROUTE_H
#include "stop.h"
#include "doublylinkedlist.h"
#include "enums.h"
#include <QList>

class Vehicle;

class Route
{
private:
    QList<Stop> stops;
    QString routeName;
    QString routeNumber;
    days day;
public:
    Route();
    Route(QString _number, QString _name, QList<Stop> _stops = QList<Stop>());
    void setDay(days _day = days::allweek);
    days getDay();
    QList<Stop> test() { return stops; }
    QString getRouteName();
    QString getRouteNumber();
    void addStop(Stop);
    QList<Stop> getStops();
    void setStops(QList<Stop>);
    static int Compare(QString, QString);
    static void WriteRouteToFile(Route, Vehicle*);
    static void RemoveRouteFromFile(Route);
    static QString GenerateStringRoute(Stop, QString);
    friend class Vehicle;
};

#endif // ROUTE_H
