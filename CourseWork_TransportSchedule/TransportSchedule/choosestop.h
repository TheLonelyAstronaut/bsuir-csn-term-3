#ifndef CHOOSESTOP_H
#define CHOOSESTOP_H

#include <QWidget>
#include "schedulewindowwidget.h"

namespace Ui {
class ChooseStop;
}

class ChooseStop : public QWidget
{
    Q_OBJECT

public:
    explicit ChooseStop(ScheduleWindowWidget *parent = nullptr, QList<Stop> stops = QList<Stop>());
    ~ChooseStop();

private slots:
    void on_nextStop_clicked();

    void on_previousStop_clicked();

    void on_directionButton_clicked();

private:
    Ui::ChooseStop *ui;
    QList<Stop> stops;
    ScheduleWindowWidget* window;
    int currentStop = 0;
    void setStop();
    friend class ScheduleWindowWidget;
    friend class MainWindow;
};

#endif // CHOOSESTOP_H
