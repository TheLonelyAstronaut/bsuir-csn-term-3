#ifndef TOPREVIOUSSCREEN_H
#define TOPREVIOUSSCREEN_H

#include <QWidget>
#include "mainwindow.h"

namespace Ui {
class ToPreviousScreen;
}

class ToPreviousScreen : public QWidget
{
    Q_OBJECT

public:
    explicit ToPreviousScreen(QWidget *parent = nullptr, MainWindow* window = nullptr, QString routeName = "");
    ~ToPreviousScreen();

private slots:
    void on_backButton_clicked();

private:
    Ui::ToPreviousScreen *ui;
    MainWindow* window;
};

#endif // TOPREVIOUSSCREEN_H
