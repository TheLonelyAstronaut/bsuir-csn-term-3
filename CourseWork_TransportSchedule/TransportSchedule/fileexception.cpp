#include "fileexception.h"

FileException::FileException(QString _message, QString _filePath) : Exception (_message), filePath(_filePath)
{

}

FileException::~FileException() {}

QString FileException::getFilePath()
{
    return filePath;
}
