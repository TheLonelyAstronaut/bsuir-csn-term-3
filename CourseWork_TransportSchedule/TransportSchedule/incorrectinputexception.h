#ifndef INCORRECTINPUTEXCEPTION_H
#define INCORRECTINPUTEXCEPTION_H
#include "Exception.h"

class IncorrectInputException :public Exception
{
    QString allowedSymbols;
public:
    IncorrectInputException(QString _message= "", QString _allowedSynbols = "");
    ~IncorrectInputException();
    QString getAllowedSymbols();
};

#endif // INCORRECTINPUTEXCEPTION_H
