#include "isalreadyincludedexception.h"

isAlreadyIncludedException::isAlreadyIncludedException(QString _message, int _includedValue) : Exception (_message), includedValue(_includedValue)
{

}

isAlreadyIncludedException::~isAlreadyIncludedException() {}

int isAlreadyIncludedException::getIncludedValue()
{
    return includedValue;
}
