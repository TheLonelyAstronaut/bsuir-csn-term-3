#include "mainwindowwidget.h"
#include "ui_mainwindowwidget.h"
#include <QDebug>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QScrollBar>
#include "mainwindow.h"
#include "transporttile.h"
#include "doublylinkedlist.h"
#include "doublylinkedlist.cpp"
#include "vehicle.h"
#include <QPushButton>

MainWindowWidget::MainWindowWidget(QWidget *parent, MainWindow* window) :
    QWidget(parent),
    ui(new Ui::MainWindowWidget),
    window(window)
{
    if(window)
    {
        //qDebug() << window->children();
        //setMouseTracking(true);
        //qDebug() << "kak0";
        ui->setupUi(this);
        statusBar = new StatusBar(nullptr, window);
        searchBar = new SearchAndSort(nullptr, window);
        QScrollArea* scrollArea = new QScrollArea(nullptr);
        QVBoxLayout* mainLayout = new QVBoxLayout(nullptr);
        this->setLayout(mainLayout);
        mainLayout->addWidget(statusBar);
        mainLayout->addWidget(searchBar);
        mainLayout->setContentsMargins(0,0,0,0);
        mainLayout->setSpacing(0);

        schedule = new QWidget(nullptr);
        QVBoxLayout* layout = new QVBoxLayout;
        layout->setContentsMargins(20,0,20,20);
        layout->setAlignment(Qt::AlignTop);
        schedule->setLayout(layout);
        scrollArea->setContentsMargins(0,0,0,0);

        scrollArea->setWidget(schedule);
        scrollArea->setWidgetResizable(true);
        scrollArea->setStyleSheet("border: 0px");
        scrollArea->verticalScrollBar()->setStyleSheet("background-color:'#34495e'; border: 0px;");
        mainLayout->addWidget(scrollArea);
        QPushButton* editButton = new QPushButton();
        editButton->setText("Добавить маршрут");
        editButton->setFont(QFont("MS Shell Dlg 2", 14));
        mainLayout->addWidget(editButton);
        editButton->setStyleSheet("border: 0; color: 'white'; background-color: #34495e;");
        editButton->setCursor(QCursor(Qt::PointingHandCursor));
        editButton->setMinimumHeight(40);
        this->connect(editButton, SIGNAL(clicked()), this, SLOT(AddButton_clicked()));
        RenderTransportComponents(this->window->busMap, true);

        //qDebug() << (this->window->busMap.begin() == this->window->busMap.end()) << this->window->busMap.size();
    }

}

MainWindowWidget::~MainWindowWidget()
{
    delete ui;
}

vehicleType MainWindowWidget::getCurrentType()
{
    return currentType;
}

void MainWindowWidget::setCurrentType(vehicleType type)
{
    currentType = type;
}

bool VehicleComparator(Vehicle* lhs, Vehicle* rhs)
{
    if(lhs->getRoute().getRouteNumber().toInt() >rhs->getRoute().getRouteNumber().toInt()) return true;
    return false;
}

bool BusComparator(Bus* lhs, Bus* rhs)
{
    return VehicleComparator(lhs, rhs);
}

bool TrolleyComparator(TrolleyBus* lhs, TrolleyBus* rhs)
{
    return VehicleComparator(lhs, rhs);
}

bool TramComparator(Tram* lhs, Tram* rhs)
{
    return VehicleComparator(lhs, rhs);
}

void MainWindowWidget::RenderTransportComponents(DoublyLinkedList<Vehicle *> transport, bool renderFullList)
{
    list.clear();

    DLLSort(this->window->busMap.begin(), this->window->busMap.end(), &BusComparator);
    DLLSort(this->window->trolleybusMap.begin(), this->window->trolleybusMap.end(), &TrolleyComparator);
    DLLSort(this->window->tramMap.begin(), this->window->tramMap.end(), &TramComparator);


    if(renderFullList)
    {
        //Route::Compare("123д", "12");
        auto busIterarorBegin = this->window->busMap.begin();
        auto trolleybusIteratorBegin = this->window->trolleybusMap.begin();
        auto tramIteratorBegin = this->window->tramMap.begin();

        auto busIterarorEnd = this->window->busMap.end();
        auto trolleybusIteratorEnd = this->window->trolleybusMap.end();
        auto tramIteratorEnd = this->window->tramMap.end();

        for (;busIterarorBegin != busIterarorEnd || trolleybusIteratorBegin != trolleybusIteratorEnd || tramIteratorBegin != tramIteratorEnd;)
        {
            auto bus = (*busIterarorBegin) ? (*busIterarorBegin)->GetRouteNumber() : "";
            auto trolleybus = (*trolleybusIteratorBegin) ? (*trolleybusIteratorBegin)->GetRouteNumber() : "";
            auto tram = (*tramIteratorBegin) ? (*tramIteratorBegin)->GetRouteNumber() : "";

            if(Route::Compare(bus, trolleybus) > 0 && Route::Compare(bus, tram) > 0)
            {
                TransportTile* tile = new TransportTile(this->window, (*busIterarorBegin));
                this->list.push_back(tile);
                busIterarorBegin++;
                continue;
            }
            else if(Route::Compare(trolleybus, bus) > 0 && Route::Compare(trolleybus, tram) > 0)
            {
                TransportTile* tile = new TransportTile(this->window, (*trolleybusIteratorBegin));
                this->list.push_back(tile);
                trolleybusIteratorBegin++;
                continue;
            }
            else if(Route::Compare(tram, bus) > 0 && Route::Compare(tram, trolleybus) > 0)
            {
                TransportTile* tile = new TransportTile(this->window, (*tramIteratorBegin));
                this->list.push_back(tile);
                tramIteratorBegin++;
                continue;
            }
        }
    }
    else
    {
        if(!transport.size())
        {
            ClearLayout(this->schedule);
            return;
        }

        auto test = transport.begin();
        qDebug() << transport.size() << "Here";

        for(;test!=transport.end(); ++test)
        {
            TransportTile* tile = new TransportTile(this->window, (*test));
            //qDebug() << tile;
            this->list.push_back(tile);
        }
    }

    ClearLayout(this->schedule);
    if(!list.size()) return;

    auto iterator= list.begin();

    for(; iterator != list.end() && list.size(); ++iterator)
    {
        //qDebug() << (*iterator);
         schedule->layout()->addWidget(*iterator);
         //qDebug() << "End!";
    }
}

void MainWindowWidget::ClearLayout(QWidget* widget)
{
    QLayoutItem *item;
        while((item = widget->layout()->takeAt(0)))
        {
            Q_ASSERT( ! item->layout() ); // otherwise the layout will leak
            delete item->widget();
            delete item;
        }
}

void MainWindowWidget::mouseMoveEvent(QMouseEvent *event){
    //if(event->x() <= 310 || event->y() <= 820) return;

    //mapped mouse relative to upper left of window
    rs_mpos=event->globalPos()-this->window->frameGeometry().topLeft();//general position tracker for resizing
    qDebug() << rs_mpos;
    qDebug() << event->pos();
    QTextStream out(stdout);

    //How much of the corner is considered a "resizing zone"
    //I was experiencing jumping behavior with rs_size is 10 so
    //I recommend rs_size=50
    int rs_size=50;

    //Big if statement checks if your mouse is in the upper left,
    //upper right, lower left, and lower right
    if ( (abs(rs_mpos.x()) < rs_size && abs(rs_mpos.y()) < rs_size) ||
     (abs(rs_mpos.x()) > this->window->width()-rs_size && abs(rs_mpos.y()) <rs_size) ||
     (abs(rs_mpos.x()) < rs_size && abs(rs_mpos.y())> this->window->height()-rs_size) ||
     (abs(rs_mpos.x()) > this->window->width()-rs_size && abs(rs_mpos.y())> this->window->height()-rs_size)

     ){


         //Below for debugging
         /*
         out << rs_mpos.x() << " , " << rs_mpos.y() << "\n";
         out << "window: " << this->window->width() << " , " << this->window->height() << "\n";
         out << "globalpos: " << event->globalPos().x() << " , "
             << event->globalPos().y() << "\n";*/


        //Use 2x2 matrix to adjust how much you are resizing and how much you
        //are moving. Since the default coordinates are relative to upper left
        //You cannot just have one way of resizing and moving the window.
        //It will depend on which corner you are referring to

        //adjXfac and adjYfac are for calculating the difference between your
        //current mouse position and where your mouse was when you clicked.
        //With respect to the upper left corner, moving your mouse to the right
        //is an increase in coordinates, moving mouse to the bottom is increase
        //etc.
        //However, with other corners this is not so and since I chose to subtract
        //This difference at the end for resizing, adjXfac and adjYfac should be
        //1 or -1 depending on whether moving the mouse in the x or y directions
        //increases or decreases the coordinates respectively.

        //transXfac transYfac is to move the window over. Resizing the window does not
        //automatically pull the window back toward your mouse. This is what
        //transfac is for (translate window in some direction). It will be either
        //0 or 1 depending on whether you need to translate in that direction.

        //Initiate matrix
        int adjXfac=0;
        int adjYfac=0;
        int transXfac=0;
        int transYfac=0;

        //Upper left corner section
        if ( (abs(rs_mpos.x()) < rs_size && abs(rs_mpos.y()) < rs_size)){
        this->window->setCursor(Qt::SizeFDiagCursor);



             //Upper left. No flipping of axis, no translating window
             adjXfac=1;
             adjYfac=1;

             transXfac=0;
             transYfac=0;



        }
        //Upper right corner section
        else if(abs(rs_mpos.x()) > this->window->width()-rs_size &&
                abs(rs_mpos.y()) <rs_size){
            this->window->setCursor(Qt::SizeBDiagCursor);


            //upper right. Flip displacements in mouse movement across x axis
            //and translate window left toward the mouse
            adjXfac=-1;
            adjYfac=1;

            transXfac = 1;
            transYfac =0;

         }

         //Lower left corner section
         else if(abs(rs_mpos.x()) < rs_size &&
                 abs(rs_mpos.y())> this->window->height()-rs_size){
            this->window->setCursor(Qt::SizeBDiagCursor);

            //lower left. Flip displacements in mouse movement across y axis
            //and translate window up toward mouse
            adjXfac=1;
            adjYfac=-1;

            transXfac=0;
            transYfac=1;


          }
          //Lower right corner section
          else if(abs(rs_mpos.x()) > this->window->width()-rs_size &&
                  abs(rs_mpos.y())> this->window->height()-rs_size){
              this->window->setCursor(Qt::SizeFDiagCursor);

             //lower right. Flip mouse displacements on both axis and
             //translate in both x and y direction left and up toward mouse.
             adjXfac=-1;
             adjYfac=-1;

             transXfac=1;
             transYfac=1;
            }

       if (event->buttons()==Qt::LeftButton ){

           //Calculation of displacement. adjXfac=1 means normal displacement
           //adjXfac=-1 means flip over axis
           int adjXdiff = adjXfac*(event->globalPos().x() - global_mpos.x());

           int adjYdiff = adjYfac*(event->globalPos().y() - global_mpos.y());

           //if transfac is 1 then movepoint of mouse is translated
           QPoint movePoint(mpos.x() - transXfac*adjXdiff, mpos.y()-transYfac*adjYdiff);
           this->window->move(event->globalPos()-movePoint);
           this->window->resize(storeWidth-adjXdiff, storeHeight-adjYdiff);

           event->accept();


         }

    }

    else{

             this->window->setCursor(Qt::ArrowCursor);


              //simple move section
              if (event->buttons()==Qt::LeftButton){
                  this->window->move(event->globalPos() - mpos);
                  event->accept();
              }


         }
}

void MainWindowWidget::mousePressEvent(QMouseEvent *event){
//From Qt Documentation:
//Reason why pos() wasn't working is because the global
//position at time of event may be very different
//This is why the mpos = event->pos(); line before was
//possibly causing jumping behavior

     if (event->button() == Qt::LeftButton){
         //Coordinates have been mapped such that the mouse position is relative to the
         //upper left of the main window
         mpos = event->globalPos() - this->window->frameGeometry().topLeft();

         //At the moment of mouse click, capture global position and
         //lock the size of window for resizing
         global_mpos = event->globalPos();
         storeWidth = this->window->width();
         storeHeight= this->window->height();

         event->accept();

     }

}

void MainWindowWidget::AddButton_clicked()
{
    this->window->NavigateToAddTransport();
}
