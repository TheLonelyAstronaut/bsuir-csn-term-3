#include "schedulewindowwidget.h"
#include "ui_schedulewindowwidget.h"
#include "mainwindow.h"
#include "statusbar.h"
#include <QScrollArea>
#include <QSpacerItem>
#include "topreviousscreen.h"
#include "vehicle.h"
#include <QPushButton>
#include <QTableWidget>
#include <QHeaderView>
#include <QLabel>
#include "choosestop.h"
#include <QStyledItemDelegate>
#include <QMessageBox>
#include "timetableelement.h"
#include "scheduletable.h"
#include "addtransport.h"
#include "Exception.h"
#include "ArgumentOutOfRangeException.h"
#include "isalreadyincludedexception.h"
#include "incorrectinputexception.h"
#include "fileexception.h"
#include "editroutecommand.h"

ScheduleWindowWidget::ScheduleWindowWidget(QWidget *parent, MainWindow* window, Vehicle* vehicle) :
    QWidget(parent),
    ui(new Ui::ScheduleWindowWidget),
    window(window),
    vehicle(vehicle)
{
    ui->setupUi(this);
    day = vehicle->getRoute().getDay();
    table = new ScheduleTable(this);
    //table->installEventFilter()
    this->connect(table, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(TableWidget_itemChanged(int, int)));
    this->connect(table, SIGNAL(itemSelectionChanged()), this, SLOT(TableWidget_SelectionChanged()));
    statusBar = new StatusBar(nullptr, window);
    toMainWidget = new ToPreviousScreen(nullptr, window, vehicle->GetRouteName());
    stops = vehicle->getRoute().getStops();
    QVBoxLayout* mainLayout = new QVBoxLayout(nullptr);
    chooseZone = new ChooseStop(this, stops);
    this->setLayout(mainLayout);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(statusBar);
    mainLayout->addWidget(toMainWidget);
    mainLayout->addWidget(chooseZone);

    ////////////////////////////////////
    QWidget* widget = new QWidget;
    QHBoxLayout* layout = new QHBoxLayout;
    widget->setLayout(layout);
    daysLabel = new QLabel(getDayString());
    QFont font = QFont("MS Shell Dlg 2", 14);
    daysLabel->setFont(font);
    daysLabel->setStyleSheet("color:white;");
    editDays = new QComboBox;
    editDays->addItem("По будням");
    editDays->addItem("По выходным");
    editDays->addItem("Ежедневно");
    editDays->setFont(font);
    editDays->setStyleSheet("border:0;color:white;");
    QSpacerItem* spacer1 = new QSpacerItem(1,1, QSizePolicy::MinimumExpanding);
    QSpacerItem* spacer2 = new QSpacerItem(1,1, QSizePolicy::MinimumExpanding);
    layout->addSpacerItem(spacer1);
    layout->addWidget(daysLabel);
    layout->addWidget(editDays);
    editDays->hide();
    layout->addSpacerItem(spacer2);
    ////////////////////////////////////

    ////////////////////////////////////
    showTransportSpecs=AddTransport::GenerateButton("Информация о машине");
    this->connect(showTransportSpecs, SIGNAL(clicked()), this, SLOT(showSpecs_clicked()));
    specsWidget = new transportSpecs(nullptr, vehicle->GetEnumType());
    ////////////////////////////////////

    mainLayout->addWidget(widget);
    mainLayout->addWidget(showTransportSpecs);
    mainLayout->addWidget(specsWidget);
    specsWidget->hide();
    mainLayout->addWidget(table);
    //to = false;
    update();
    editButton = new QPushButton();
    editButton->setText("Редактировать");
    editButton->setFont(QFont("MS Shell Dlg 2", 14));
    mainLayout->addWidget(editButton);
    editButton->setStyleSheet("border: 0; color: 'white'; background-color: #34495e;");
    editButton->setCursor(QCursor(Qt::PointingHandCursor));
    editButton->setMinimumHeight(40);
    this->connect(editButton, SIGNAL(clicked()), this, SLOT(EditButton_clicked()));
}

void ScheduleWindowWidget::update()
{
    nextSchedule(generateScheduleTableByStopId());
    if(vehicle->GetEnumType() == vehicleType::bus)
    {
        specsWidget->setSpecs(vehicle->getModel(), QString::number(vehicle->getHumanCapacity()), QString::number(dynamic_cast<Bus*>(vehicle)->getEngineVolume()), QString::number(dynamic_cast<Bus*>(vehicle)->getSectionCount()));
    }
    if(vehicle->GetEnumType() == vehicleType::trolleybus)
    {
        specsWidget->setSpecs(vehicle->getModel(), QString::number(vehicle->getHumanCapacity()), QString::number(dynamic_cast<TrolleyBus*>(vehicle)->getElectricPower()), QString::number(dynamic_cast<TrolleyBus*>(vehicle)->getBatteryCapacity()));
    }
    if(vehicle->GetEnumType() == vehicleType::tram)
    {
        specsWidget->setSpecs(vehicle->getModel(), QString::number(vehicle->getHumanCapacity()), QString::number(dynamic_cast<Tram*>(vehicle)->getElectricPower()), dynamic_cast<Tram*>(vehicle)->getLowFloor() ? "Да" : "Heт");
    }
}

QString ScheduleWindowWidget::getDayString()
{
    QString result;

    switch (day) {
    case days::weekends:{
        result = "По выходным";
        break;
    }
    case days::weekdays:{
        result = "По будням";
        break;
    }
    case days::allweek:{
        result = "Ежедневно";
        break;
    }
    }

    return result;
}


ScheduleWindowWidget::~ScheduleWindowWidget()
{
    delete ui;
}

QMultiMap<int, int> ScheduleWindowWidget::generateScheduleTableByStopId(int id)
{
    QList<ScheduleTime> timeTable;
    //timeTable.reserve(timeTable.length());
    currentStop = id;

    if(to) timeTable = stops[id].getToTimetable();
    else timeTable = stops[id].getBackTimetable();

    QMultiMap<int, int> uniqueHours = QMultiMap<int, int>();

    for(auto time = --timeTable.end(); time!=timeTable.begin(); --time)
    {
        //qDebug() << time->getMinute() << endl;
        uniqueHours.insert((*time).getHour(), (*time).getMinute());
    }

    uniqueHours.insert(timeTable.begin()->getHour(), timeTable.begin()->getMinute());

    qDebug() << uniqueHours;

    return uniqueHours;
}

void ScheduleWindowWidget::nextSchedule(QMultiMap<int, int> uniqueHours)
{
    qDebug() << uniqueHours;
    table->initTable(uniqueHours);
    table->setFocusPolicy(Qt::NoFocus);
}

void ScheduleWindowWidget::EditButton_clicked()
{
    if(editButton->text() == "Сохранить")
    {
        switch (editDays->currentIndex()) {
        case 0:{
            day = days::weekdays;
            break;
        }case 1:{
            day = days::weekends;
            break;
        }case 2:{
            day = days::allweek;
            break;
        }
        }
        daysLabel->setText(getDayString());
        editDays->hide();
        daysLabel->show();
        editButton->setText("Редактировать");
        TableWidget_SelectionChanged();
        auto specs = specsWidget->browseMode();
        //specsWidget->editMode();

        if(vehicle->GetEnumType() == vehicleType::bus)
        {
            try {
               Exception::isNumber(specs[1]);
               Exception::isNumber(specs[2]);
               Exception::isNumber(specs[3]);
            } catch (IncorrectInputException ex) {
                QMessageBox::warning(nullptr, "Проверьте параметры транспорта", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
                EditButton_clicked();
                return;
            }

            /*vehicle->setModel(specs[0]);
            vehicle->setHumanCapacity(specs[1].toInt());
            dynamic_cast<Bus*>(vehicle)->setEngineVolume(specs[2].toInt());
            dynamic_cast<Bus*>(vehicle)->setSectionCount(specs[3].toInt());

            try{
                Bus::WriteBusToFile(dynamic_cast<Bus*>(vehicle));
            }catch(FileException ex){
                QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
                return;
            }*/
        }
        if(vehicle->GetEnumType() == vehicleType::trolleybus)
        {
            try {
               Exception::isNumber(specs[1]);
               Exception::isNumber(specs[2]);
               Exception::isNumber(specs[3]);
            } catch (IncorrectInputException ex) {
                QMessageBox::warning(nullptr, "Проверьте параметры транспорта", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
                EditButton_clicked();
                return;
            }

            /*vehicle->setModel(specs[0]);
            vehicle->setHumanCapacity(specs[1].toInt());
            dynamic_cast<TrolleyBus*>(vehicle)->setElectricPower(specs[2].toInt());
            dynamic_cast<TrolleyBus*>(vehicle)->setBatteryCapacity(specs[3].toInt());

            try {
                TrolleyBus::WriteTrolleyBusToFile(dynamic_cast<TrolleyBus*>(vehicle));
            }catch(FileException ex){
                QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
                return;
            }*/
        }
        if(vehicle->GetEnumType() == vehicleType::tram)
        {
            try {
               Exception::isNumber(specs[1]);
               Exception::isNumber(specs[2]);
            } catch (IncorrectInputException ex) {
                QMessageBox::warning(nullptr, "Проверьте параметры транспорта", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
                EditButton_clicked();
                return;
            }

            /*vehicle->setModel(specs[0]);
            vehicle->setHumanCapacity(specs[1].toInt());
            dynamic_cast<Tram*>(vehicle)->setElectricPower(specs[3].toInt());
            dynamic_cast<Tram*>(vehicle)->setLowFloor(specs[3].toLower() == "да" ? true : false);

            try {
                Tram::WriteTramToFile(dynamic_cast<Tram*>(vehicle));
            }catch(FileException ex){
                QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
                return;
            }*/
        }

        Route route(vehicle->getRoute().getRouteNumber(), vehicle->getRoute().getRouteName(), stops);
        route.setDay(day);
        //vehicle->setRoute(route);

        this->window->undoStack.push(new EditRouteCommand(vehicle, route, specs[0],specs[1].toInt(),specs[2].toInt(), specs[3].toInt(), currentStop));
        table->browseMode();

        /*try {
            Stop::WriteTimetableToFile(stops[currentStop], vehicle);
            Route::WriteRouteToFile(route, vehicle);
        }catch(FileException ex){
            QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
            return;
        }*/

        //auto test = table->
    }
    else
    {
        editDays->show();
        daysLabel->hide();
        table->editMode();
        specsWidget->editMode();
        editButton->setText("Сохранить");
    }
}

void ScheduleWindowWidget::TableWidget_itemChanged(int i, int j)
{
    qDebug() << "item changed" <<endl;

   if(editButton->text()=="Сохранить" && j && i)
   {
       TableWidget_SelectionChanged();

       TimeTableElement* element = dynamic_cast<TimeTableElement*>(table->cellWidget(i,j));
       element->SetEditable();
   }
}

void ScheduleWindowWidget::TableWidget_SelectionChanged()
{
    //qDebug() << "SELECTION CHANGED" << endl;
    //qDebug() << table->currentRow() << " " << table->currentColumn() <<endl;

    for(int i=0; i<table->rowCount(); ++i)
    {
        for(int j=0; j<table->columnCount(); ++j)
        {
            qDebug() << "Here";
            TimeTableElement* element = dynamic_cast<TimeTableElement*>(table->cellWidget(i,j));
            if(element && element->IsInEditingMode())
            {
                qDebug() << "Not here";
                QString text = element->SetUneditable();
                QList<ScheduleTime> test;

                if(to) test = stops[currentStop].getToTimetable();
                else test = stops[currentStop].getBackTimetable();

                QList<ScheduleTime> newTimetable;

                QList<int> minutes;

                try {
                    minutes = Exception::isMinuteString(text);
                } catch(ArgumentOutOfRangeException ex){
                    QMessageBox::warning(nullptr, "Значение вне диапазона", ex.Message() + "\nЗначение должно быть в границах от " + QString::number(ex.MinimumValue()) + " до " + QString::number(ex.MaximumValue()));
                    //qDebug() << minutes.size();
                    //return;
                }catch(isAlreadyIncludedException ex){
                    QMessageBox::warning(nullptr, "Перезапись", ex.Message() + "\nЗначение: " + QString::number(ex.getIncludedValue()));
                    minutes.clear();
                }catch(IncorrectInputException ex){
                    QMessageBox::warning(nullptr, "Ошибка разбора", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
                    minutes.clear();
                }catch(...){
                    minutes.clear();
                }

                bool copied = false;

                for (auto it = test.begin(); it != test.end(); ++it) {
                    if(it->getHour() == i-1 &&  !copied)
                    {         
                        for(auto minIt = minutes.begin(); minIt != minutes.end(); ++minIt){
                            newTimetable.push_back(ScheduleTime(i-1, (*minIt)));
                        }

                        copied = true;
                    }else if(it->getHour() != i-1){
                        newTimetable.push_back((*it));
                    }
                }

                qDebug() << copied << endl;
                if(!copied)
                {
                    for(auto minIt = minutes.begin(); minIt != minutes.end(); ++minIt){
                        newTimetable.push_back(ScheduleTime(i-1, (*minIt)));
                    }
                }

                if(!newTimetable.size())
                {
                    QMessageBox::warning(this, tr("AstroSchedule"), tr("Должно быть как минимум одно поле!"));
                    this->EditButton_clicked();
                    return;
                }

                //for(auto it = newTimetable)

                if(to) stops[currentStop].setTimeTable(newTimetable, stops[currentStop].getBackTimetable());
                else stops[currentStop].setTimeTable(stops[currentStop].getToTimetable(), newTimetable);

                table->setTimeTable(generateScheduleTableByStopId(currentStop));
                table->initTable(generateScheduleTableByStopId(currentStop));
            }
        }
    }
}

void ScheduleWindowWidget::showSpecs_clicked()
{
    if(specsWidget->isHidden()) specsWidget->show();
    else specsWidget->hide();
}
