#include "ArgumentOutOfRangeException.h"
#include <string>

ArgumentOutOfRangeException::ArgumentOutOfRangeException(QString _message, int _maxValue, int _minValue) : Exception(_message), maxValue(_maxValue), minValue(_minValue) {}

ArgumentOutOfRangeException::~ArgumentOutOfRangeException() {}

int ArgumentOutOfRangeException::MaximumValue() { return maxValue; }

int ArgumentOutOfRangeException::MinimumValue() { return minValue; }
