#include "statusbar.h"
#include "mainwindow.h"
#include "ui_statusbar.h"
#include <QMoveEvent>
#include <QDesktopWidget>
#include <QStyleOption>
#include <QPainter>

StatusBar::StatusBar(QWidget *parent, MainWindow* window) :
    QWidget(parent),
    window(window),
    ui(new Ui::StatusBar)
{
    //setMouseTracking(true);
    ui->setupUi(this);
}

void StatusBar::mousePressEvent(QMouseEvent *event)
{
    deltaX = event->x();
    deltaY = event->y();
    this->wasDoubleclicked=false;
    //on_maximizeButton_clicked();
}

void StatusBar::mouseDoubleClickEvent(QMouseEvent *event)
{
    this->wasDoubleclicked = true;
    on_maximizeButton_clicked();
}

void StatusBar::moveEvent(QMoveEvent *event)
{
    QPoint pos = event->pos();
    //qDebug() << pos.x() << endl;
    //qDebug() << pos.y() << endl;
}

void StatusBar::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void StatusBar::mouseMoveEvent(QMouseEvent *event)
{
    if(this->wasDoubleclicked)
    {
        qDebug() << "Blocked!";
        return;
    }

    //this->window->mouseMoveEvent(event);
    QPoint pos = event->pos();
    qDebug() << "mouseMove " << event->globalX() << " " << event->y();
   // qDebug() << pos.x() << " " << pos.y();
    //qDebug() << pos.y() << endl;
    //qDebug() << this->window->pos().x() << " " << this->window->pos().y() << endl;
    this->window->move(event->globalX()-this->deltaX, event->globalY()-this->deltaY);
}

StatusBar::~StatusBar()
{
    delete ui;
}

void StatusBar::on_closeApp_clicked()
{
    QApplication::quit();
}

void StatusBar::on_minimizeButton_clicked()
{
    this->window->showMinimized();
    //QWidget::showMinimized();
}

void StatusBar::on_maximizeButton_clicked()
{
   maximize();
}

void StatusBar::maximize()
{
    QRect screenGeometry = QApplication::desktop()->screenGeometry();

    qDebug() << "Here";

    if(this->window->size() != QDesktopWidget().availableGeometry(this->window).size())
    {
        this->window->resize(QDesktopWidget().availableGeometry(this->window).size());
        this->window->move(0,0);
    }
    else {
        this->window->resize(400, 800);
        this->window->move((screenGeometry.width()-this->window->width()) / 2, (screenGeometry.height()-this->window->height()) / 2);
    }
}
