#ifndef ELECTRICVEHICLE_H
#define ELECTRICVEHICLE_H
#include "vehicle.h"

class ElectricVehicle : public Vehicle
{
protected:
    int electricPower;
public:
    ElectricVehicle();
    ElectricVehicle(Route _route, int _humanCapacity = 0, QString _model = "", int _electricPower = 0, int _vehicleId = 0);
    void setElectricPower(int _electricPower);
    int getElectricPower();
    virtual ~ElectricVehicle() {}
    friend bool operator==(ElectricVehicle& lhs, ElectricVehicle& rhs);
};

#endif // ELECTRICVEHICLE_H
