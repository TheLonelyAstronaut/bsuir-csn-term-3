#ifndef ADDVEHICLECOMMAND_H
#define ADDVEHICLECOMMAND_H
#include <QUndoCommand>
#include "vehicle.h"
#include "mainwindow.h"

class AddVehicleCommand : public QUndoCommand
{
    Vehicle* vehicle;
    MainWindow* window;
public:
    AddVehicleCommand(MainWindow* _mainWindow, Vehicle* vehicle);
    void undo() override;
    void redo() override;
};

#endif // ADDVEHICLECOMMAND_H
