#include "editroutecommand.h"
#include "enums.h"
#include <QMessageBox>
#include "fileexception.h"
#include "bus.h"
#include "trolleybus.h"
#include "tram.h"

EditRouteCommand::EditRouteCommand(Vehicle *vehicle, Route route, QString model, int humanCapacity, int firstParameter, int secondParameter, int currentStop)
{
    this->vehicle = vehicle;
    this->route = route;
    this->model = model;
    this->humanCapacity = humanCapacity;
    this->firstParameter = firstParameter;
    this->secondParameter = secondParameter;
    this->currentStop = currentStop;
}

void EditRouteCommand::undo()
{
    if(vehicle)
    {
        redo();
    }
}

void EditRouteCommand::redo()
{
    if(vehicle)
    {
        QString tempModel = vehicle->getModel();
        int tempHumanCapacity = vehicle->getHumanCapacity();
        int tempFirstParameter, tempSecondParameter;
        Route tempRoute = vehicle->getRoute();
        vehicle->setRoute(route);
        route = tempRoute;

        if(vehicle->GetEnumType()== vehicleType::bus)
        {
            //Bus* bus = dynamic_cast<Bus*>(vehicle);

            tempFirstParameter = dynamic_cast<Bus*>(vehicle)->getEngineVolume();
            tempSecondParameter = dynamic_cast<Bus*>(vehicle)->getSectionCount();

            dynamic_cast<Bus*>(vehicle)->setModel(model);
            dynamic_cast<Bus*>(vehicle)->setHumanCapacity(humanCapacity);
            dynamic_cast<Bus*>(vehicle)->setEngineVolume(firstParameter);
            dynamic_cast<Bus*>(vehicle)->setSectionCount(secondParameter);

            try{
                Bus::WriteBusToFile(dynamic_cast<Bus*>(vehicle));
            }catch(FileException ex){
                QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
                return;
            }
        }
        if(vehicle->GetEnumType() == vehicleType::trolleybus)
        {
            //TrolleyBus* trolleybus = dynamic_cast<TrolleyBus*>(vehicle);

            tempFirstParameter = dynamic_cast<TrolleyBus*>(vehicle)->getElectricPower();
            tempSecondParameter = dynamic_cast<TrolleyBus*>(vehicle)->getBatteryCapacity();

            dynamic_cast<TrolleyBus*>(vehicle)->setModel(model);
            dynamic_cast<TrolleyBus*>(vehicle)->setHumanCapacity(humanCapacity);
            dynamic_cast<TrolleyBus*>(vehicle)->setElectricPower(firstParameter);
            dynamic_cast<TrolleyBus*>(vehicle)->setBatteryCapacity(secondParameter);

            try {
                TrolleyBus::WriteTrolleyBusToFile(dynamic_cast<TrolleyBus*>(vehicle));
            }catch(FileException ex){
                QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
                return;
            }
        }
        if(vehicle->GetEnumType() == vehicleType::tram)
        {
            //Tram* tram = dynamic_cast<Tram*>(vehicle);

            tempFirstParameter = dynamic_cast<Tram*>(vehicle)->getElectricPower();
            tempSecondParameter = dynamic_cast<Tram*>(vehicle)->getLowFloor();

            dynamic_cast<Tram*>(vehicle)->setModel(model);
            dynamic_cast<Tram*>(vehicle)->setHumanCapacity(humanCapacity);
            dynamic_cast<Tram*>(vehicle)->setElectricPower(firstParameter);
            dynamic_cast<Tram*>(vehicle)->setLowFloor(secondParameter);
        }

        model = tempModel;
        humanCapacity = tempHumanCapacity;
        firstParameter = tempFirstParameter;
        secondParameter = tempSecondParameter;

        try {
            Stop::WriteTimetableToFile(vehicle->getRoute().getStops()[currentStop], vehicle);
            Route::WriteRouteToFile(vehicle->getRoute(), vehicle);
        }catch(FileException ex){
            QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
            return;
        }
    }
}
