#include "timetableelement.h"
#include "ui_timetableelement.h"
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QLabel>
#include <QDebug>

TimeTableElement::TimeTableElement(QString time, bool hour, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimeTableElement),
    hours(hour),
    time(time)
{
    ui->setupUi(this);
    QHBoxLayout* layout = new QHBoxLayout(nullptr);
    this->setLayout(layout);
    //this->setStyleSheet("background-color:#ecf0f1;");
    this->setStyleSheet("border-bottom: 1 solid #2c3e50;");
    label = new QLabel(time);
    layout->setMargin(0);
    label->setStyleSheet("color: #ecf0f1;");
    QFont f("Arial", 16);
    label->setFont(f);
    QSpacerItem* spacer2 = new QSpacerItem(1,1, QSizePolicy::Minimum, QSizePolicy::Expanding);

    if(hours)
    {
        QSpacerItem* spacer1 = new QSpacerItem(1,1, QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        layout->addSpacerItem(spacer1);
        spacer2 = new QSpacerItem(1,1, QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    }

    layout->addWidget(label);
    edit = new QLineEdit;
    edit->setFont(QFont("Arial", 16));
    edit->setStyleSheet("color: #ecf0f1;");
    layout->addWidget(edit);
    edit->hide();
    layout->addSpacerItem(spacer2);
    this->connect(edit, SIGNAL(editingFinished()), this, SLOT(LineEdit_editingFinished()));
}

TimeTableElement::~TimeTableElement()
{
    delete ui;
}

void TimeTableElement::SetEditable()
{
    edit->show();
    edit->setFocusPolicy(Qt::StrongFocus);
    edit->setText(label->text());
    label->hide();
    this->setStyleSheet("border-bottom: 1 solid #ecf0f1;");
    //dynamic_cast<QSpacerItem*>(layout()->children()[layout()->children().length()-1])->changeSize(0,0);
    editing = true;
}

QString TimeTableElement::SetUneditable()
{
    //if(!editing) return "";
    edit->hide();
    label->show();
    label->setText(edit->text());
    editing = false;
    this->setStyleSheet("border-bottom: 1 solid #2c3e50;");
    return label->text();
}

bool TimeTableElement::IsInEditingMode()
{
    return editing;
}
/*void TimeTableElement::mouseDoubleClickEvent ( QMouseEvent * event )
{
    if(edit->isHidden())
    {
        edit->show();
        edit->setText(label->text());
        label->hide();
    }
    else
    {
        edit->hide();
        label->show();
        label->setText(edit->text());
    }
}*/

void TimeTableElement::LineEdit_editingFinished()
{
    SetUneditable();
}
