#ifndef TRAM_H
#define TRAM_H
#include "electricvehicle.h"
#include "enums.h"
#include "doublylinkedlist.h"

class Tram : public ElectricVehicle
{
    bool lowFloor;
public:
    Tram();
    Tram(Route route, int _humanCapacity = 0, QString _model = "", int _electricPower = 0, bool _lowFloor = true, int _vehicleId = 0);
    ~Tram() {}
    void setLowFloor(bool _lowFloor);
    bool getLowFloor();
    QString GetRouteNumber();
    QString GetRouteName();
    vehicleType GetEnumType();
    static void WriteTramToFile(Tram* bus);
    static void RemoveTramFromFile(Tram* bus);
    static DoublyLinkedList<Tram*> ReadTramFromFile();
    friend bool operator==(Tram& lhs, Tram& rhs);
};

#endif // TRAM_H
