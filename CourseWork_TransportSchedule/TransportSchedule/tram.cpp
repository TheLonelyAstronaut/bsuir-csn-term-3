#include "tram.h"
#include "enums.h"
#include "doublylinkedlist.cpp"
#include "fileexception.h"

Tram::Tram()
{

}

Tram::Tram(Route _route, int _humanCapacity, QString _model, int _enginePower, bool _lowFloor, int _vehicleId) : ElectricVehicle(_route, _humanCapacity, _model, _enginePower, _vehicleId)
{
    this->lowFloor = _lowFloor;
}

QString Tram::GetRouteNumber()
{
    return this->vehicleRoute.getRouteNumber();
}

QString Tram::GetRouteName()
{
    return this->vehicleRoute.getRouteName();
}

vehicleType Tram::GetEnumType()
{
    return vehicleType(tram);
}

void Tram::setLowFloor(bool _lowFloor)
{
    this->lowFloor = _lowFloor;
}

bool Tram::getLowFloor()
{
    return lowFloor;
}

void Tram::WriteTramToFile(Tram *bus)
{
    auto dataInFile = ReadTramFromFile();
    DoublyLinkedList<Tram*> toFile;
    bool inList = false;
    int counter = 1;

    for(auto it = dataInFile.begin(); it !=dataInFile.end() && dataInFile.size(); ++it)
    {
        if((*it)->getVehicleId() == bus->getVehicleId())
        {
            toFile.push_back(bus);
            inList = true;
            continue;
        }
        else if(*(*it) == *bus)
        {
            bus->setVehicleId((*it)->getVehicleId());
            toFile.push_back(bus);
            inList = true;
        }
        else
        {
            toFile.push_back((*it));
        }
        counter++;
    }

    if(!inList)
    {
        bus->setVehicleId(counter);
        toFile.push_back(bus);
    }

    QFile file("binarydatabase/tram.bin");
    if(!file.open(QIODevice::WriteOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/tram.bin");
    QDataStream stream(&file);

    for (auto it = toFile.begin(); it != toFile.end(); ++it) {

        int temp =  (*it)->model.length(),
                humanCapacity = (*it)->humanCapacity,
                electricPower = (*it)->electricPower,
                lowFloor = (*it)->lowFloor,
                id = (*it)->getVehicleId();
        stream.writeRawData(reinterpret_cast<char*>(&id), sizeof(int));
        stream.writeRawData(reinterpret_cast<char*>(&humanCapacity), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&temp), sizeof(int));
        stream.writeRawData((*it)->model.toStdString().c_str(), (*it)->model.length());
        stream.writeRawData(reinterpret_cast<char*>(&electricPower), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&lowFloor), sizeof (bool));
    }

    file.close();
}

void Tram::RemoveTramFromFile(Tram *bus)
{
    auto dataInFile = ReadTramFromFile();
    DoublyLinkedList<Tram*> toFile;
    //bool inList = false;

    for(auto it = dataInFile.begin(); it !=dataInFile.end() && dataInFile.size(); ++it)
    {
        if(*(*it) == *bus || (*it)->getVehicleId() == bus->getVehicleId())
        {

        }
        else
        {
            toFile.push_back((*it));
        }
    }

    //if(!inList) toFile.push_back(bus);

    QFile file("binarydatabase/tram.bin");
    if(!file.open(QIODevice::WriteOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/tram.bin");
    QDataStream stream(&file);

    for (auto it = toFile.begin(); it != toFile.end(); ++it) {

        int temp =  (*it)->model.length(),
                humanCapacity = (*it)->humanCapacity,
                electricPower = (*it)->electricPower,
                lowFloor = (*it)->lowFloor,
                id = (*it)->getVehicleId();
        stream.writeRawData(reinterpret_cast<char*>(&id), sizeof(int));
        stream.writeRawData(reinterpret_cast<char*>(&humanCapacity), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&temp), sizeof(int));
        stream.writeRawData((*it)->model.toStdString().c_str(), (*it)->model.length());
        stream.writeRawData(reinterpret_cast<char*>(&electricPower), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&lowFloor), sizeof (bool));
    }

    file.close();
}

DoublyLinkedList<Tram*> Tram::ReadTramFromFile()
{
    QFile file("binarydatabase/tram.bin");
    if(!file.open(QIODevice::ReadOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/tram.bin");
    QDataStream stream(&file);
    DoublyLinkedList<Tram*> returnable;

    //qDebug() << stream.atEnd();

    while(!stream.atEnd())
    {
        Tram *bus = new Tram;
        stream.readRawData(reinterpret_cast<char*>(&(bus->vehicleId)), sizeof(int));
        stream.readRawData(reinterpret_cast<char*>(&(bus->humanCapacity)), sizeof (int));
        int stringSize;
        stream.readRawData(reinterpret_cast<char*>(&stringSize), sizeof(int));
        char string[stringSize+1];
        string[stringSize] = '\0';
        stream.readRawData(string, stringSize);
        stream.readRawData(reinterpret_cast<char*>(&(bus->electricPower)), sizeof(int));
        stream.readRawData(reinterpret_cast<char*>(&(bus->lowFloor)), sizeof(bool));
        bus->setModel(string);

        returnable.push_back(bus);
    }

    file.close();
    return returnable;
}

bool operator==(Tram& lhs, Tram& rhs)
{
    //qDebug() << lhs.sectionCount << rhs.sectionCount;
    if(static_cast<ElectricVehicle&>(lhs) == static_cast<ElectricVehicle&>(rhs) && lhs.lowFloor == rhs.lowFloor) return true;
    else return false;
}


