#ifndef TRANSPORTSPECS_H
#define TRANSPORTSPECS_H

#include <QWidget>
#include "enums.h"

namespace Ui {
class transportSpecs;
}

class transportSpecs : public QWidget
{
    Q_OBJECT

public:
    explicit transportSpecs(QWidget* widget, vehicleType type);
    ~transportSpecs();
    void init();
    void editMode();
    QVector<QString> browseMode();
    void setType(vehicleType);
    void setSpecs(QString, QString, QString, QString);
private:
    vehicleType type;
    Ui::transportSpecs *ui;
    bool edit = false;
};

#endif // TRANSPORTSPECS_H
