#ifndef DELETEVEHICLECOMMAND_H
#define DELETEVEHICLECOMMAND_H
#include <QUndoCommand>
#include "mainwindow.h"
#include "vehicle.h"

class DeleteVehicleCommand : public QUndoCommand
{
    Vehicle* vehicle;
    MainWindow* window;
public:
    DeleteVehicleCommand(MainWindow* _mainWindow, Vehicle* vehicle);
    void undo() override;
    void redo() override;
};

#endif // DELETEVEHICLECOMMAND_H
