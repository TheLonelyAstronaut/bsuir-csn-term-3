#include "electricvehicle.h"

ElectricVehicle::ElectricVehicle()
{

}

ElectricVehicle::ElectricVehicle(Route _route, int _humanCapacity, QString _model, int _electricPower, int _vehicleId) : Vehicle(_route, _humanCapacity, _model, _vehicleId)
{
    this->electricPower = _electricPower;
}

int ElectricVehicle::getElectricPower()
{
    return electricPower;
}

void ElectricVehicle::setElectricPower(int _electricPower)
{
    this->electricPower = _electricPower;
}

bool operator==(ElectricVehicle& lhs, ElectricVehicle& rhs)
{
    if(static_cast<Vehicle&>(lhs) == static_cast<Vehicle&>(rhs) && lhs.electricPower == rhs.electricPower) return true;
    else return false;
}
