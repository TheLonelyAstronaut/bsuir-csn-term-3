#pragma once
#include <QString>
using namespace std;

class Exception : public exception
{
protected:
    QString message;
public:
    Exception(QString _message = "");
    virtual ~Exception();
    virtual QString Message();
    static QList<int> isMinuteString(QString);
    static int isNumber(QString);
};

