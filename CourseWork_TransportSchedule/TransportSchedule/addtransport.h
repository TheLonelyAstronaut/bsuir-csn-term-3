#ifndef ADDTRANSPORT_H
#define ADDTRANSPORT_H

#include <QWidget>
#include "mainwindow.h"
#include "statusbar.h"
#include <QPushButton>
#include "scheduletable.h"
#include <QSpacerItem>
#include "transportspecs.h"

namespace Ui {
class AddTransport;
}

class AddTransport : public QWidget
{
    Q_OBJECT

public:
    explicit AddTransport(QWidget *parent = nullptr, MainWindow* window = nullptr);
    ~AddTransport() override;
    static QPushButton* GenerateButton(QString);

private:
    Ui::AddTransport *ui;
    MainWindow* window;

    ////////////////////////////////////////////
    StatusBar* statusBar = nullptr;
    ToPreviousScreen* toMainWidget = nullptr;
    QPushButton* editButton;
    QLineEdit* stopName;
    QPushButton* parameterButton;
    QPushButton* stopButton;
    QPushButton* addStop;
    QPushButton* swapTable;
    ScheduleTable* table;
    QWidget* stopWidget;
    QSpacerItem* spacer;
    QList<Stop> stops;
    bool to;
    int uniqueStops = 0;
    QMultiMap<int, int> toMap;
    QMultiMap<int, int> backMap;
    transportSpecs* specsWidget;
private slots:
    void EditButton_clicked();
    void AddStop_clicked();
    void TableWidget_itemChanged(int, int);
    void TableWidget_SelectionChanged();
    void on_showParameters_clicked();
    void on_addStop_clicked();
    void SwapTable_clicked();
    void on_setTransportType_currentIndexChanged(int index);
};

#endif // ADDTRANSPORT_H
