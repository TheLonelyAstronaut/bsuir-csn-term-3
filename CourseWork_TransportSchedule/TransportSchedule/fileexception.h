#ifndef FILEEXCEPTION_H
#define FILEEXCEPTION_H
#include "Exception.h"

class FileException : public Exception
{
    QString filePath;
public:
    FileException(QString _message = "", QString _filePath = "");
    ~FileException();
    QString getFilePath();
};

#endif // FILEEXCEPTION_H
