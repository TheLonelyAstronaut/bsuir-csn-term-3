#include "InvalidLanguageException.h"

InvalidLanguageException::InvalidLanguageException(QString message, QString defaultLNG) : Exception(message), language(defaultLNG) {}

InvalidLanguageException::~InvalidLanguageException() {}

string InvalidLanguageException::Language() { return language; }
