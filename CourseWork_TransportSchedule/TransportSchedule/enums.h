#ifndef ENUMS_H
#define ENUMS_H

enum vehicleType
{
  bus = 0,
  trolleybus = 1,
  tram = 2,
  all = 3
};

enum days
{
    weekdays = 0,
    weekends = 1,
    allweek = 2
};

#endif // ENUMS_H
