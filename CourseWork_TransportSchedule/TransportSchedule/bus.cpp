#include "bus.h"
#include "enums.h"
#include <QFile>
#include "doublylinkedlist.cpp"
#include "fileexception.h"

Bus::Bus()
{

}

Bus::Bus(Route _route, int _humanCapacity, QString _model, int _engineVolume, int _sectionCount, int _vehicleId) : OilVehicle(_route, _humanCapacity, _model, _engineVolume, _vehicleId)
{
    this->sectionCount = _sectionCount;
}

QString Bus::GetRouteNumber()
{
    return this->vehicleRoute.getRouteNumber();
}

QString Bus::GetRouteName()
{
    return this->vehicleRoute.getRouteName();
}

vehicleType Bus::GetEnumType()
{
    return vehicleType(bus);
}

int Bus::getSectionCount()
{
    return sectionCount;
}

void Bus::setSectionCount(int _sectionCount)
{
    this->sectionCount = _sectionCount;
}

void Bus::WriteBusToFile(Bus *bus)
{
    auto dataInFile = ReadBusFromFile();
    DoublyLinkedList<Bus*> toFile;
    bool inList = false;
    int counter = 1;
    //qDebug() << dataInFile.size() << "SIZE" << bus->getVehicleId();

    for(auto it = dataInFile.begin(); it !=dataInFile.end() && dataInFile.size(); ++it)
    {
        if((*it)->getVehicleId() == bus->getVehicleId())
        {
            toFile.push_back(bus);
            inList = true;
            continue;
        }
        else if(*(*it) == *bus && !inList)
        {
            //qDebug() << "GOTCHA" << (*it)->getSectionCount() << bus->getSectionCount();
            bus->setVehicleId((*it)->getVehicleId());
            toFile.push_back(bus);
            inList = true;
            continue;
        }
        else
        {
            toFile.push_back((*it));
        }
        counter++;
    }

    //qDebug() << "inList" << inList << "counter" << counter;
    if(!inList)
    {
        bus->setVehicleId(counter);
        toFile.push_back(bus);
    }

    QFile file("binarydatabase/bus.bin");
    if(!file.open(QIODevice::WriteOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/bus.bin");
    QDataStream stream(&file);

    for (auto it = toFile.begin(); it != toFile.end(); ++it) {

        int temp =  (*it)->model.length(),
                humanCapacity = (*it)->humanCapacity,
                engineVolume = (*it)->engineVolume,
                sectionCount = (*it)->sectionCount,
                id = (*it)->getVehicleId();
        stream.writeRawData(reinterpret_cast<char*>(&id), sizeof(int));
        stream.writeRawData(reinterpret_cast<char*>(&humanCapacity), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&temp), sizeof(int));
        stream.writeRawData((*it)->model.toStdString().c_str(), (*it)->model.length());
        stream.writeRawData(reinterpret_cast<char*>(&engineVolume), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&sectionCount), sizeof (int));
    }

    file.close();
}

void Bus::RemoveBusFromFile(Bus *bus)
{
    auto dataInFile = ReadBusFromFile();
    DoublyLinkedList<Bus*> toFile;

    for(auto it = dataInFile.begin(); it !=dataInFile.end() && dataInFile.size(); ++it)
    {
        if(*(*it) == *bus || (*it)->getVehicleId() == bus->getVehicleId())
        {

        }
        else
        {
            toFile.push_back((*it));
        }
    }

    //if(!inList) toFile.push_back(bus);

    QFile file("binarydatabase/bus.bin");
    if(!file.open(QIODevice::WriteOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/bus.bin");
    QDataStream stream(&file);

    for (auto it = toFile.begin(); it != toFile.end(); ++it) {

        int temp =  (*it)->model.length(),
                humanCapacity = (*it)->humanCapacity,
                engineVolume = (*it)->engineVolume,
                sectionCount = (*it)->sectionCount,
                id = (*it)->getVehicleId();
        stream.writeRawData(reinterpret_cast<char*>(&id), sizeof(int));
        stream.writeRawData(reinterpret_cast<char*>(&humanCapacity), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&temp), sizeof(int));
        stream.writeRawData((*it)->model.toStdString().c_str(), (*it)->model.length());
        stream.writeRawData(reinterpret_cast<char*>(&engineVolume), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&sectionCount), sizeof (int));
    }

    file.close();
}

DoublyLinkedList<Bus*> Bus::ReadBusFromFile()
{
    QFile file("binarydatabase/bus.bin");
    if(!file.open(QIODevice::ReadOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/bus.bin");
    QDataStream stream(&file);
    DoublyLinkedList<Bus*> returnable;

    //qDebug() << stream.atEnd();

    while(!stream.atEnd())
    {
        Bus *bus = new Bus;
        stream.readRawData(reinterpret_cast<char*>(&(bus->vehicleId)), sizeof(int));
        stream.readRawData(reinterpret_cast<char*>(&(bus->humanCapacity)), sizeof (int));
        int stringSize;
        stream.readRawData(reinterpret_cast<char*>(&stringSize), sizeof(int));
        char string[stringSize+1];
        string[stringSize] = '\0';
        stream.readRawData(string, stringSize);
        stream.readRawData(reinterpret_cast<char*>(&(bus->engineVolume)), sizeof(int));
        stream.readRawData(reinterpret_cast<char*>(&(bus->sectionCount)), sizeof(int));
        bus->setModel(string);

        returnable.push_back(bus);
    }
    //qDebug() << "=======";

    file.close();
    return returnable;
}

bool operator==(Bus& lhs, Bus& rhs)
{
    //qDebug() << lhs.sectionCount << rhs.sectionCount;
    if(static_cast<OilVehicle&>(lhs) == static_cast<OilVehicle&>(rhs) && lhs.sectionCount == rhs.sectionCount) return true;
    else return false;
}
