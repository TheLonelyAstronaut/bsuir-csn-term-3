#ifndef TRANSPORTTILE_H
#define TRANSPORTTILE_H

#include <QWidget>
#include "vehicle.h"
#include "mainwindow.h"
#include <QPushButton>

namespace Ui {
class TransportTile;
}

class TransportTile : public QWidget
{
    Q_OBJECT

public:
    explicit TransportTile(MainWindow *parent = nullptr, Vehicle* vehicle = nullptr);
    ~TransportTile();
private:
    Ui::TransportTile *ui;
    MainWindow* window;
    Vehicle* vehicle;
    QPushButton* deleteButton;
protected:
    //void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent ( QMouseEvent * event ) override;
private slots:
    void onDelete_click();
};

#endif // TRANSPORTTILE_H
