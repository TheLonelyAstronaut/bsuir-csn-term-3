#include "route.h"
#include <QList>
#include <QFile>
#include "stop.h"
#include "doublylinkedlist.h"
#include "doublylinkedlist.cpp"
#include "vehicle.h"
#include "enums.h"
#include <QMessageBox>
#include <fstream>
#include <QDebug>
#include "bus.h"
#include "tram.h"
#include "trolleybus.h"

using namespace std;

Route::Route(QString _number, QString _name, QList<Stop> _stops)
{
    routeNumber = _number;
    routeName = _name;
    stops = _stops;
    //stops = DoublyLinkedList<Stop>(_stops);
}

void Route::setDay(days _day)
{
    day = _day;
}

days Route::getDay()
{
    return day;
}

Route::Route()
{

}

QString Route::getRouteName()
{
    return routeName;
}

QString Route::getRouteNumber()
{
    return routeNumber;
}

void Route::addStop(Stop stop)
{
    stops.push_back(stop);
}

int Route::Compare(QString route1, QString route2)
{
    if(route1 == route2)
    {
        if(route1 == "") return 0;
    }
    else if(route1 == "") return -1;
    else if(route2 == "") return 1;

    QRegExp numRegex("[0-9]+"), letterRegex("[а-я]*");
    numRegex.indexIn(route1);
    letterRegex.indexIn(route1);
    int route1Num = numRegex.capturedTexts()[0].toInt();
    QString route1Letter = letterRegex.capturedTexts().size() ? letterRegex.capturedTexts()[0] : "";
    numRegex.indexIn(route2);
    letterRegex.indexIn(route2);
    int route2Num = numRegex.capturedTexts()[0].toInt();
    QString route2Letter = letterRegex.capturedTexts().size() ? letterRegex.capturedTexts()[0] : "";


    if(route1Num > route2Num) return -1;
    else if(route2Num > route1Num) return 1;
    else
    {
        if(route1Letter > route2Letter) return -1;
        else if(route2Letter > route1Letter) return 1;
    }

    return 0;
}

QList<Stop> Route::getStops()
{
    return stops;
}

void Route::setStops(QList<Stop> stops)
{
    this->stops = stops;
}

void Route::WriteRouteToFile(Route route, Vehicle* vehicle)
{
    vehicleType type = vehicle->GetEnumType();

    int vehicleId = 0;


    QFile routes("textdatabase/routes.txt");
    if(!routes.open(QIODevice::ReadOnly | QIODevice::WriteOnly | QIODevice::Text )) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << routes.errorString();
        //return;
    }

    //routes.seek(0);
    QTextStream routesIn(&routes);
    QString stringType, stringDay;
    routesIn.seek(0);

    if(type == vehicleType::bus)
    {
         stringType = "bus";
    }
    else if(type == vehicleType::trolleybus)
    {
         stringType="trol";
    }
    else
    {
         stringType="tram";
    }

    if(route.getDay() == days::weekdays) stringDay = "weekdays";
    if(route.getDay() == days::weekends) stringDay = "weekends";
    if(route.getDay() == days::allweek) stringDay = "allweek";

    QString toFile = route.getRouteNumber() + ';' + route.getRouteName() + ';' + stringType + ';'+ stringDay+';'+QString::number(vehicle->getVehicleId());

    QVector<QString> routesVector;

    bool sended = false;

    while(!routesIn.atEnd())
    {
        QString line = routesIn.readLine();
        QStringList splitted = line.split(';');

        if(route.getRouteNumber() == splitted[0] && splitted[1] == route.getRouteName())
        {
            routesVector.push_back(toFile);
            sended = true;
        }else{
            routesVector.push_back(line);
        }
    }

    if(!sended) routesVector.push_back(toFile);

    routes.close();

    routes.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
    QTextStream rewrite(&routes);

    for (auto it = routesVector.begin(); it != routesVector.end(); ++it) {
        rewrite << (*it) << endl;
    }

    routes.close();
}

QString Route::GenerateStringRoute(Stop stop, QString number)
{
    QString toFile = stop.id()+';'+number+";{\"A>B\": {";

    QList<ScheduleTime> to = stop.getToTimetable();
    QMultiMap<int, int> sortingMap;

    for (auto it = --to.end(); it != to.begin(); --it) {
        sortingMap.insert(it->getHour(), it->getMinute());
    }

    sortingMap.insert(to.begin()->getHour(), to.begin()->getMinute());

    for (int i=0; i<sortingMap.uniqueKeys().length(); ++i) {
        auto uniqueIt = sortingMap.find(sortingMap.uniqueKeys()[i]);
        toFile += "\"" + QString::number(sortingMap.uniqueKeys()[i]) + "\": [";

        for(int j=0; j<sortingMap.count(sortingMap.uniqueKeys()[i]); ++j, ++uniqueIt)
        {
            toFile += "\"" + QString::number((*uniqueIt)) + "\",";
        }

        toFile = toFile.left(toFile.lastIndexOf(','));
        toFile += "], ";
    }

    toFile = toFile.left(toFile.lastIndexOf(','));
    toFile += "}, \"A<B\":{";
    sortingMap.clear();

    to = stop.getBackTimetable();

    for (auto it = --to.end(); it != to.begin(); --it) {
        sortingMap.insert(it->getHour(), it->getMinute());
    }

    sortingMap.insert(to.begin()->getHour(), to.begin()->getMinute());

    for (int i=0; i<sortingMap.uniqueKeys().length(); ++i) {
        auto uniqueIt = sortingMap.find(sortingMap.uniqueKeys()[i]);
        toFile += "\"" + QString::number(sortingMap.uniqueKeys()[i]) + "\": [";

        for(int j=0; j<sortingMap.count(sortingMap.uniqueKeys()[i]); ++j, ++uniqueIt)
        {
            toFile += "\"" + QString::number((*uniqueIt)) + "\",";
        }

        toFile = toFile.left(toFile.lastIndexOf(','));
        toFile += "], ";
    }

    toFile = toFile.left(toFile.lastIndexOf(','));
    toFile += "}}";
    return toFile;
}

void Route::RemoveRouteFromFile(Route route)
{
    QFile routes("textdatabase/routes.txt");
    if(!routes.open(QIODevice::ReadOnly | QIODevice::Text )) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << routes.errorString();
        //return;
    }

    QTextStream routesIn(&routes);
    QVector<QString> file;
    routesIn.seek(0);

    file.push_back(routesIn.readLine());

    while (!routesIn.atEnd()) {
        QString line = routesIn.readLine();
        QStringList splitted = line.split(';');

        //qDebug() << splitted[0].toInt() << route.getRouteNumber();

        if(splitted[0].toInt() != route.getRouteNumber().toInt())
        {
            file.push_back(line);
        }
    }

    routes.close();

    if(!routes.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << routes.errorString();
        //return;
    }

    QTextStream routesOut(&routes);

    for (auto it = file.begin(); it != file.end(); ++it) {
        //qDebug() << (*it) << endl;
        routesOut << (*it) << endl;
    }

    routes.close();
}
