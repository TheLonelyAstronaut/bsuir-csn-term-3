#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QMap>
#include "doublylinkedlist.h"
#include "stop.h"
#include "bus.h"
#include "trolleybus.h"
#include "tram.h"
#include "enums.h"
#include <QUndoStack>

namespace Ui {
class MainWindow;
}

class TransportTile;
class MainWindowWidget;
class ScheduleWindowWidget;
class ToPreviousScreen;
class AddTransport;
class AddVehicleCommand;

class MainWindow : public QMainWindow
{
    Q_OBJECT
protected:
    void keyReleaseEvent(QKeyEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    Ui::MainWindow *ui;
    QString currentScreen;

    DoublyLinkedList<Bus*> busMap;
    DoublyLinkedList<TrolleyBus*> trolleybusMap;
    DoublyLinkedList<Tram*> tramMap;

    MainWindowWidget* mainWidget = nullptr;
    ScheduleWindowWidget* scheduleWidget = nullptr;
    AddTransport* addtransportWidget = nullptr;

    void initializeSchedule();
    void NavigateToMainScreen(MainWindow*);
    void NavigateToSchedule(Vehicle*);
    void NavigateToAddTransport();
    Vehicle* getVehicleById(QString id);
    int stopCounter;
    void deleteVehicle(Vehicle*);

    QUndoStack undoStack;
    QSet<int> pressedKeys;

    friend class SearchAndSort;
    friend class TransportTile;
    friend class MainWindowWidget;
    friend class ScheduleWindowWidget;
    friend class ToPreviousScreen;
    friend class AddTransport;
    friend class AddVehicleCommand;
    friend class DeleteVehicleCommand;
};

#endif // MAINWINDOW_H
