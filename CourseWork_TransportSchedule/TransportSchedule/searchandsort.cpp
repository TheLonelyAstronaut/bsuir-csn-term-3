#include "searchandsort.h"
#include "ui_searchandsort.h"
#include "mainwindow.h"
#include <QDebug>
#include "doublylinkedlist.h"
#include "doublylinkedlist.cpp"
#include "mainwindowwidget.h"
#include "vehicle.h"
#include <QStyle>
#include <QPushButton>
#include "enums.h"

SearchAndSort::SearchAndSort(QWidget *parent, MainWindow* window) :
    QWidget(parent),
    ui(new Ui::SearchAndSort),
    window(window)
{
    ui->setupUi(this);
    ui->setBus->setCursor(QCursor(Qt::PointingHandCursor));
    ui->setTram->setCursor(QCursor(Qt::PointingHandCursor));
    ui->setTrolleybus->setCursor(QCursor(Qt::PointingHandCursor));
    ui->setAll->setCursor(QCursor(Qt::PointingHandCursor));
    //QPushButton* button = new QPushButton();
    //button->setC
    //ui->textEdit->setAlignment(Qt::AlignTop);
}

SearchAndSort::~SearchAndSort()
{
    delete ui;
}

void SearchAndSort::on_lineEdit_textChanged(const QString &arg1)
{
    if(this->window->mainWidget->getCurrentType() == vehicleType(bus))
    {
        this->window->mainWidget->RenderTransportComponents(vehicleSort(arg1, this->window->busMap));
    }
    if(this->window->mainWidget->getCurrentType() == vehicleType(trolleybus))
    {
        this->window->mainWidget->RenderTransportComponents(vehicleSort(arg1, this->window->trolleybusMap));
    }
    if(this->window->mainWidget->getCurrentType() == vehicleType(tram))
    {
        this->window->mainWidget->RenderTransportComponents(vehicleSort(arg1, this->window->tramMap));
    }
    if(this->window->mainWidget->getCurrentType() == vehicleType(all))
    {
        this->window->mainWidget->RenderTransportComponents(allVehicleSort(arg1));
    }
}

DoublyLinkedList<Vehicle*> SearchAndSort::vehicleSort(QString parameter, DoublyLinkedList<Vehicle*> request)
{
    auto iterator = request.begin();
    DoublyLinkedList<Vehicle*> response = DoublyLinkedList<Vehicle*>();

    for(; iterator != request.end(); ++iterator)
    {
        if((*iterator)->GetRouteName().contains(parameter) || (*iterator)->GetRouteNumber().contains(parameter))
        {
            response.push_back((*iterator));
        }
    }

    return response;
}

DoublyLinkedList<Vehicle*> SearchAndSort::allVehicleSort(QString parameter)
{
    DoublyLinkedList<Vehicle*> response = DoublyLinkedList<Vehicle*>();

    auto busIterarorBegin = this->window->busMap.begin();
    auto trolleybusIteratorBegin = this->window->trolleybusMap.begin();
    auto tramIteratorBegin = this->window->tramMap.begin();

    auto busIterarorEnd = this->window->busMap.end();
    auto trolleybusIteratorEnd = this->window->trolleybusMap.end();
    auto tramIteratorEnd = this->window->tramMap.end();

    for (;busIterarorBegin != busIterarorEnd || trolleybusIteratorBegin != trolleybusIteratorEnd || tramIteratorBegin != tramIteratorEnd;)
    {
        auto bus = (*busIterarorBegin) ? (*busIterarorBegin)->GetRouteNumber() : "";
        auto trolleybus = (*trolleybusIteratorBegin) ? (*trolleybusIteratorBegin)->GetRouteNumber() : "";
        auto tram = (*tramIteratorBegin) ? (*tramIteratorBegin)->GetRouteNumber() : "";

        if(Route::Compare(bus, trolleybus) > 0 && Route::Compare(bus, tram) > 0)
        {
            if((*busIterarorBegin)->GetRouteName().contains(parameter) || (*busIterarorBegin)->GetRouteNumber().contains(parameter)) response.push_back((*busIterarorBegin));
            busIterarorBegin++;
            continue;
        }
        else if(Route::Compare(trolleybus, bus) > 0 && Route::Compare(trolleybus, tram) > 0)
        {
            if((*trolleybusIteratorBegin)->GetRouteName().contains(parameter) || (*trolleybusIteratorBegin)->GetRouteNumber().contains(parameter)) response.push_back((*trolleybusIteratorBegin));
            trolleybusIteratorBegin++;
            continue;
        }
        else if(Route::Compare(tram, bus) > 0 && Route::Compare(tram, trolleybus) > 0)
        {
            if((*tramIteratorBegin)->GetRouteName().contains(parameter) || (*tramIteratorBegin)->GetRouteNumber().contains(parameter)) response.push_back((*tramIteratorBegin));
            tramIteratorBegin++;
            continue;
        }
    }

    return response;
}

void SearchAndSort::on_setBus_clicked()
{
    ui->setBus->setStyleSheet("#setBus{border: 0px; color: red;}");
    ui->setTram->setStyleSheet("#setTram{border: 0px; color: white;} #setTram:hover{color: red;}");
    ui->setTrolleybus->setStyleSheet("#setTrolleybus{border: 0px; color: white;} #setTrolleybus:hover{color: red;}");
    ui->setAll->setStyleSheet("#setAll{border: 0px; color: white;} #setAll:hover{color: red;}");

    this->window->mainWidget->setCurrentType(vehicleType(bus));
    this->window->mainWidget->RenderTransportComponents(this->window->busMap);
}

void SearchAndSort::on_setTrolleybus_clicked()
{
    ui->setTrolleybus->setStyleSheet("#setTrolleybus{border: 0px; color: red;}");
    ui->setTram->setStyleSheet("#setTram{border: 0px; color: white;} #setTram:hover{color: red;}");
    ui->setBus->setStyleSheet("#setBus{border: 0px; color: white;} #setBus:hover{color: red;}");
    ui->setAll->setStyleSheet("#setAll{border: 0px; color: white;} #setAll:hover{color: red;}");

    this->window->mainWidget->setCurrentType(vehicleType(trolleybus));
    this->window->mainWidget->RenderTransportComponents(this->window->trolleybusMap);
}

void SearchAndSort::on_setTram_clicked()
{
    ui->setTram->setStyleSheet("#setTram{border: 0px; color: red;}");
    ui->setBus->setStyleSheet("#setBus{border: 0px; color: white;} #setBus:hover{color: red;}");
    ui->setTrolleybus->setStyleSheet("#setTrolleybus{border: 0px; color: white;} #setTrolleybus:hover{color: red;}");
    ui->setAll->setStyleSheet("#setAll{border: 0px; color: white;} #setAll:hover{color: red;}");

    this->window->mainWidget->setCurrentType(vehicleType(tram));
    this->window->mainWidget->RenderTransportComponents(this->window->tramMap);
}

void SearchAndSort::on_setAll_clicked()
{
    ui->setAll->setStyleSheet("#setAll{border: 0px; color: red;}");
    ui->setBus->setStyleSheet("#setBus{border: 0px; color: white;} #setBus:hover{color: red;}");
    ui->setTrolleybus->setStyleSheet("#setTrolleybus{border: 0px; color: white;} #setTrolleybus:hover{color: red;}");
    ui->setTram->setStyleSheet("#setTram{border: 0px; color: white;} #setTram:hover{color: red;}");

    this->window->mainWidget->setCurrentType(vehicleType(all));
    this->window->mainWidget->RenderTransportComponents(this->window->busMap, true);
}
