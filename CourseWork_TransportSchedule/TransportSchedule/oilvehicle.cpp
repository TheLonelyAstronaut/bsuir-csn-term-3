#include "oilvehicle.h"
#include <QDebug>

OilVehicle::OilVehicle()
{

}

OilVehicle::OilVehicle(Route _route, int _humanCapacity, QString _model, int _engineVolume, int _vehicleId) : Vehicle (_route, _humanCapacity, _model, _vehicleId)
{
    this->engineVolume = _engineVolume;
}

int OilVehicle::getEngineVolume()
{
    return  engineVolume;
}

void OilVehicle::setEngineVolume(int _engineVolume)
{
    this->engineVolume = _engineVolume;
}

bool operator==(OilVehicle& lhs, OilVehicle& rhs)
{
    if(static_cast<Vehicle&>(lhs) == static_cast<Vehicle&>(rhs) && lhs.engineVolume == rhs.engineVolume) return true;
    else return false;
}
