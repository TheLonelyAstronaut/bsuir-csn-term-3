#ifndef STOP_H
#define STOP_H

#include <QString>
#include <QList>
#include "doublylinkedlist.h"
#include "scheduletime.h"


class Vehicle;

class Stop
{
private:
    QString ID, Name;
    QList<ScheduleTime> to, back;
public:
    Stop(){}
    Stop(QString id, QString name);
    ~Stop();
    QString id();
    QString name();
    //QList<ScheduleTime> test() {return to;}
    friend class Route;
    void setTimeTable(QList<ScheduleTime>, QList<ScheduleTime>);
    static void WriteTimetableToFile(Stop, Vehicle*);
    static void WriteStopToFile(Stop);
    static QList<Stop> ReadStopsFromFile();
    static void RemoveTimetableFromFile(Stop, Vehicle*);
    QList<ScheduleTime> getToTimetable();
    QList<ScheduleTime> getBackTimetable();
};

#endif // STOP_H
