#include "scheduletime.h"
#include <QDebug>

ScheduleTime::ScheduleTime(short _hour, short _minute)
{
    hour = _hour;
    minute = _minute;
}

short ScheduleTime::getHour()
{
    return hour;
}

short ScheduleTime::getMinute()
{
    return minute;
}

bool ScheduleTime::operator==(const ScheduleTime& time)
{
    if(time.hour == this->hour && time.minute == this->minute) return true;
    return false;
}
