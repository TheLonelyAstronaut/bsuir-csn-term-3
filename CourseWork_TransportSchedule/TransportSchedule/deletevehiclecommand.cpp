#include "deletevehiclecommand.h"
#include "fileexception.h"
#include <QMessageBox>
#include "doublylinkedlist.cpp"

DeleteVehicleCommand::DeleteVehicleCommand(MainWindow* _mainWindow, Vehicle* vehicle)
{
    this->vehicle = vehicle;
    this->window = _mainWindow;
}

void DeleteVehicleCommand::redo()
{
    //qDebug() << "Undo";
    this->window->NavigateToMainScreen(this->window);
    this->window->deleteVehicle(vehicle);
}

void DeleteVehicleCommand::undo()
{
    QList<Stop> stops = vehicle->getRoute().getStops();

    if(vehicle->GetEnumType() == vehicleType::bus)
    {
        Bus* bus = dynamic_cast<Bus*>(vehicle);

        try {
            Bus::WriteBusToFile(bus);
            this->window->busMap.push_back(bus);
            //qDebug() << bus->getRoute().getStops().length();
            Route::WriteRouteToFile(bus->getRoute(), bus);

            for(auto it = stops.begin(); it != stops.end(); ++it)
            {
                //qDebug() << (*it).id();
                Stop::WriteTimetableToFile((*it), bus);
            }
        } catch (FileException ex) {
            QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
            return;
        }
    }
    if(vehicle->GetEnumType() == vehicleType::trolleybus)
    {
        TrolleyBus* trolley = dynamic_cast<TrolleyBus*>(vehicle);

        try {
            TrolleyBus::WriteTrolleyBusToFile(trolley);
            this->window->trolleybusMap.push_back(trolley);
            Route::WriteRouteToFile(trolley->getRoute(), trolley);
            for(auto it = stops.begin(); it != stops.end(); ++it)
            {
                Stop::WriteTimetableToFile((*it), trolley);
            }
        } catch (FileException ex) {
            QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
            return;
        }
    }
    if(vehicle->GetEnumType() == vehicleType::tram)
    {
        Tram* tram = dynamic_cast<Tram*>(vehicle);

        try {
            Tram::WriteTramToFile(tram);
            this->window->tramMap.push_back(tram);
            Route::WriteRouteToFile(tram->getRoute(), tram);
            for(auto it = stops.begin(); it != stops.end(); ++it)
            {
                Stop::WriteTimetableToFile((*it), tram);
            }
        } catch (FileException ex) {
            QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
            return;
        }
    }

    try{
        for(auto it = stops.begin(); it != stops.end(); ++it)
        {
            Stop::WriteStopToFile((*it));
        }
    }catch (FileException ex) {
        QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
        return;
    }

    this->window->NavigateToMainScreen(this->window);
}
