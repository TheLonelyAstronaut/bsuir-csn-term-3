#ifndef TIMETABLEELEMENT_H
#define TIMETABLEELEMENT_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>

namespace Ui {
class TimeTableElement;
}

class TimeTableElement : public QWidget
{
    Q_OBJECT

public:
    explicit TimeTableElement(QString time, bool hours = false, QWidget *parent = nullptr);
    ~TimeTableElement();
    void SetEditable();
    QString SetUneditable();
    bool IsInEditingMode();
private:
    Ui::TimeTableElement *ui;
    //void mouseDoubleClickEvent ( QMouseEvent * event ) override;
    QLabel* label;
    QLineEdit* edit;
    bool editing = false;
    bool hours;
    QString time;
private slots:
    void LineEdit_editingFinished();
};

#endif // TIMETABLEELEMENT_H
