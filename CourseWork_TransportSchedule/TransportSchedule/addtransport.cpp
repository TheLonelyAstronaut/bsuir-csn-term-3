#include "addtransport.h"
#include "ui_addtransport.h"
#include "topreviousscreen.h"
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QMessageBox>
#include <QSpacerItem>
#include "scheduletable.h"
#include "timetableelement.h"
#include "doublylinkedlist.cpp"
#include "bus.h"
#include "tram.h"
#include "trolleybus.h"
#include "enums.h"
#include <QScrollArea>
#include <QMap>
#include <QMessageBox>
#include <QString>
#include "Exception.h"
#include "ArgumentOutOfRangeException.h"
#include "isalreadyincludedexception.h"
#include "incorrectinputexception.h"
#include "fileexception.h"
#include "addvehiclecommand.h"

AddTransport::AddTransport(QWidget *parent, MainWindow* window) :
    QWidget(parent),
    ui(new Ui::AddTransport),
    window(window)
{
    ui->setupUi(this);
    statusBar = new StatusBar(nullptr, window);
    //this->layout()->i
    ui->mainLayout->insertWidget(0, statusBar);
    toMainWidget = new ToPreviousScreen(nullptr, window, "Добавить маршрут");
    ui->mainLayout->insertWidget(1, toMainWidget);

    stopWidget = new QWidget;
    stopWidget->setContentsMargins(0,0,0,0);
    QVBoxLayout* tempLayout = new QVBoxLayout;
    stopWidget->setLayout(tempLayout);

    QScrollArea* scrollArea = new QScrollArea;
    table = new ScheduleTable(nullptr);
    addStop = GenerateButton("Добавить остановку");

    scrollArea->setWidgetResizable(true);
    scrollArea->setStyleSheet("border: 0px");
    scrollArea->verticalScrollBar()->setStyleSheet("background-color:'#34495e'; border: 0px;");
    scrollArea->setContentsMargins(0,0,0,0);


    specsWidget = new transportSpecs(nullptr, vehicleType::bus);
    specsWidget->editMode();
    ui->test->setContentsMargins(0,0,0,0);
    ui->test->addWidget(specsWidget);


    table->setStyleSheet("border: 0px");

    table->setRowCount(25);
    table->setColumnCount(2);

    //QMultiMap<int, int> times;

    for(int i=0; i<24; ++i)
    {
        toMap.insert(i, -1);
        backMap.insert(i, -1);
        //table->AddHour(i+1, 0, QString::number(i));
        //table->AddMinutes(i+1,1, "");
    }

    table->initTable(toMap);
    table->editMode();

    table->setFocusPolicy(Qt::NoFocus);
    //table->setSelectionMode(QAbstractItemView::NoSelection);

    scrollArea->setWidget(table);
    this->layout()->setContentsMargins(0,0,0,0);

    editButton = GenerateButton("Добавить");

    ////////////////////////////////////////////
    QWidget* stopNameWidget = new QWidget;
    QHBoxLayout* stopNameLayout = new QHBoxLayout;
    stopNameWidget->setLayout(stopNameLayout);
    QLabel* label = new QLabel("Название");
    stopNameLayout->addWidget(label);
    stopName = new QLineEdit;
    stopName->setStyleSheet("border-top:0;\nborder-left:0;\nborder-right:0;\nborder-bottom: 1 solid white;\ncolor: white;");
    QFont font = QFont("MS Shell Dlg 2", 14);
    stopName->setFont(font);
    label->setFont(font);
    label->setStyleSheet("color: white;");
    stopNameLayout->addWidget(stopName);
    stopNameLayout->setSpacing(20);
    stopNameLayout->setContentsMargins(20,0,20,0);
    ////////////////////////////////////////////

    ui->ParameterWidget->hide();
    swapTable = GenerateButton("Сменить направление");
    ui->showParameters->setCursor(QCursor(Qt::PointingHandCursor));
    ui->addStop->setCursor(QCursor(Qt::PointingHandCursor));
    spacer = new QSpacerItem(1,1,QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    tempLayout->addWidget(stopNameWidget);
    tempLayout->addWidget(swapTable);
    tempLayout->addWidget(scrollArea);
    tempLayout->setContentsMargins(0,0,0,0);
    ui->mainLayout->addWidget(stopWidget);
    tempLayout->addWidget(addStop);
    ui->mainLayout->addSpacerItem(spacer);
    stopWidget->hide();
    ui->mainLayout->addWidget(editButton);
    this->setFocusPolicy(Qt::StrongFocus);
    this->connect(swapTable, SIGNAL(clicked()), this, SLOT(SwapTable_clicked()));
    this->connect(addStop, SIGNAL(clicked()), this, SLOT(AddStop_clicked()));
    this->connect(editButton, SIGNAL(clicked()), this, SLOT(EditButton_clicked()));
    this->connect(table, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(TableWidget_itemChanged(int, int)));
    this->connect(table, SIGNAL(itemSelectionChanged()), this, SLOT(TableWidget_SelectionChanged()));
}

AddTransport::~AddTransport()
{
    delete ui;
}

void AddTransport::AddStop_clicked()
{
    if(!stopName->text().length()){
        QMessageBox::warning(nullptr, "Ошибка", "Введите название остановки");
        return;
    }

    QList<Stop> list;
    try {
        list = Stop::ReadStopsFromFile();
    } catch (FileException ex) {
        QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
        return;
    }

    QString index = "";
    for (auto it = list.begin(); it !=list.end();++it) {
        if((*it).name() == stopName->text())
        {
            index = (*it).id();
            break;
        }
    }

    if(index == "")
    {
        uniqueStops++;
        index = QString::number(list.length() + uniqueStops);
    }

    Stop stop = Stop(index, stopName->text());

    QList<ScheduleTime> toSchedule, backSchedule;

    for(int i=0; i<toMap.uniqueKeys().length(); ++i)
    {
        auto uniqueIt = toMap.find(toMap.uniqueKeys()[i]);

        for (int j=0; j<toMap.count(toMap.uniqueKeys()[i]); ++j, ++uniqueIt) {

            if((*uniqueIt) != -1)
            {
                qDebug() << toMap.uniqueKeys()[i] << (*uniqueIt);
                toSchedule.push_back(ScheduleTime(toMap.uniqueKeys()[i], (*uniqueIt)));
            }
        }
    }

    //qDebug() << "===========";

    for(int i=0; i<backMap.uniqueKeys().length(); ++i)
    {
        auto uniqueIt = backMap.find(backMap.uniqueKeys()[i]);

        for (int j=0; j<backMap.count(backMap.uniqueKeys()[i]); ++j, ++uniqueIt) {

            if((*uniqueIt) != -1)
            {
                qDebug() << backMap.uniqueKeys()[i] << (*uniqueIt);
                backSchedule.push_back(ScheduleTime(backMap.uniqueKeys()[i], (*uniqueIt)));
            }
        }
    }

    if(backSchedule.length() && toSchedule.length())
    {
        stop.setTimeTable(toSchedule, backSchedule);
        stops.push_back(stop);

        stopName->setText("");

        toMap.clear();
        backMap.clear();

        for(int i=0; i<24; ++i)
        {
            toMap.insert(i, -1);
            backMap.insert(i, -1);
        }

        table->initTable(toMap);
    }else{
        QMessageBox::warning(nullptr, "Ошибка", "Заполните оба маршрута");
    }
}

void AddTransport::EditButton_clicked()
{
    //qDebug() << stops.length() << ui->setTransportType->currentText() << endl;
    if(!ui->To->text().length() || !ui->From->text().length() || !ui->RouteNumber->text().length())
    {
        QMessageBox::warning(nullptr, "Ошибка", "Заполните все поля маршрута!");
        return;
    }

    for(auto it = this->window->busMap.begin(); it != this->window->busMap.end(); ++it)
    {
        if((*it)->GetRouteNumber()== ui->RouteNumber->text() || (*it)->GetRouteName() == ui->From->text() + " - " + ui->To->text())
        {
            QMessageBox::warning(nullptr,"Ошибка", "Маршрут с таким номером или названием уже существует!");
            return;
        }
    }

    for(auto it = this->window->trolleybusMap.begin(); it != this->window->trolleybusMap.end(); ++it)
    {
        if((*it)->GetRouteNumber()== ui->RouteNumber->text() || (*it)->GetRouteName() == ui->From->text() + " - " + ui->To->text())
        {
            QMessageBox::warning(nullptr,"Ошибка", "Маршрут с таким номером или названием уже существует!");
            return;
        }
    }

    for(auto it = this->window->tramMap.begin(); it != this->window->tramMap.end(); ++it)
    {
        if((*it)->GetRouteNumber()== ui->RouteNumber->text() || (*it)->GetRouteName() == ui->From->text() + " - " + ui->To->text())
        {
            QMessageBox::warning(nullptr,"Ошибка", "Маршрут с таким номером или названием уже существует!");
            return;
        }
    }

    if(stops.length() < 2)
    {
        QMessageBox::warning(nullptr, "Ошибка", "Добавьте как минимум 2 остановки!");
        return;
    }

    try {
        Exception::isNumber(ui->RouteNumber->text());
    } catch (IncorrectInputException ex) {
        QMessageBox::warning(nullptr, "Проверьте номер маршрута", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
        return;
    }

    days day = days::allweek;
    QVector<QString> specs = specsWidget->browseMode();

    switch(ui->comboBox->currentIndex()){
    case 0:{  
        day = days::weekdays;
        break;
    }
    case 1:{
        day = days::weekends;
        break;
    }
    case 2:{
        day = days::allweek;
        break;
    }
    }

    Route route(ui->RouteNumber->text(), ui->From->text() + " - " + ui->To->text(), stops);
    route.setDay(day);

    switch(ui->setTransportType->currentIndex()){
    case 0:{
        try {
           Exception::isNumber(specs[1]);
           Exception::isNumber(specs[2]);
           Exception::isNumber(specs[3]);
        } catch (IncorrectInputException ex) {
            QMessageBox::warning(nullptr, "Проверьте параметры транспорта", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
            return;
        }

        Bus* bus = new Bus(route, specs[1].toInt(), specs[0], specs[2].toInt(), specs[3].toInt());

        this->window->undoStack.push(new AddVehicleCommand(this->window, bus));
        break;
    }
    case 1:{
        try {
           Exception::isNumber(specs[1]);
           Exception::isNumber(specs[2]);
           Exception::isNumber(specs[3]);
        } catch (IncorrectInputException ex) {
            QMessageBox::warning(nullptr, "Проверьте параметры транспорта", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
            return;
        }

        TrolleyBus* trolley = new TrolleyBus(route, specs[1].toInt(), specs[0], specs[2].toInt(), specs[3].toInt());

        this->window->undoStack.push(new AddVehicleCommand(this->window, trolley));
        break;
    }
    case 2:{
        //Tram()
        try {
           Exception::isNumber(specs[1]);
           Exception::isNumber(specs[2]);
        } catch (IncorrectInputException ex) {
            QMessageBox::warning(nullptr, "Проверьте параметры транспорта", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
            return;
        }

        Tram* tram = new Tram(route, specs[1].toInt(), specs[0], specs[2].toInt(), specs[3].toLower() == "да" ? true : false);

        this->window->undoStack.push(new AddVehicleCommand(this->window, tram));
        break;
    }
    }
}

void AddTransport::TableWidget_itemChanged(int i, int j)
{
    qDebug() << "item changed" <<endl;

    for(int k=0; k<table->rowCount(); ++k)
    {
        for(int f=0; f<table->columnCount(); ++f)
        {
            TimeTableElement* element = dynamic_cast<TimeTableElement*>(table->cellWidget(k,f));
            if(element && element->IsInEditingMode())
            {
                element->SetUneditable();
            }
        }
    }

   if(j && i)
   {
       TimeTableElement* element = dynamic_cast<TimeTableElement*>(table->cellWidget(i,j));
       element->SetEditable();
   }
}

void AddTransport::TableWidget_SelectionChanged()
{
    //qDebug() << table->currentRow() << " " << table->currentColumn() <<endl;

    for(int i=0; i<table->rowCount(); ++i)
    {
        for(int j=0; j<table->columnCount(); ++j)
        {
            //qDebug() << "Casting...";
            TimeTableElement* element = dynamic_cast<TimeTableElement*>(table->cellWidget(i,j));
            //qDebug() << "Done!";
            if(element && element->IsInEditingMode())
            {
                QString text = element->SetUneditable();
                auto timetable = table->getTimeTable();
                QMultiMap<int, int> newTimetable;

                QList<int> minutes;

                try {
                    minutes = Exception::isMinuteString(text);
                } catch(ArgumentOutOfRangeException ex){
                    QMessageBox::warning(nullptr, "Значение вне диапазона", ex.Message() + "\nЗначение должно быть в границах от " + QString::number(ex.MinimumValue()) + " до " + QString::number(ex.MaximumValue()));
                    //qDebug() << minutes.size();
                    //return;
                }catch(isAlreadyIncludedException ex){
                    QMessageBox::warning(nullptr, "Перезапись", ex.Message() + "\nЗначение: " + QString::number(ex.getIncludedValue()));
                    minutes.clear();
                }catch(IncorrectInputException ex){
                    QMessageBox::warning(nullptr, "Ошибка разбора", ex.Message() + "\nРазрешенные символы: " + ex.getAllowedSymbols());
                    minutes.clear();
                }catch(...){
                    minutes.clear();
                }

                for(int k=0; k<timetable.uniqueKeys().length(); ++k)
                {
                    if(timetable.uniqueKeys()[k] == i-1)
                    {
                        if(minutes.begin() == minutes.end()){
                             newTimetable.insert(timetable.uniqueKeys()[k], -1);
                             continue;
                        }

                        for(auto it = --minutes.end(); it != minutes.begin(); --it)
                        {
                            newTimetable.insert(i-1, (*it));
                        }

                        if(minutes.size()) newTimetable.insert(i-1, (*minutes.begin()));
                    }
                    else {
                        auto it = timetable.find(timetable.uniqueKeys()[k]);
                        QList<int> toSort;

                        for (int f=0; f<timetable.count(timetable.uniqueKeys()[k]); ++f, ++it)
                        {
                            toSort.push_back((*it));
                        }

                        std::sort(toSort.begin(), toSort.end());
                        //toSort.re;
                        if(!toSort.size()) continue;

                        for(auto it1 = --toSort.end(); it1 != toSort.begin(); --it1)
                        {
                            newTimetable.insert(timetable.uniqueKeys()[k], (*it1));

                            qDebug() << (*newTimetable.find(timetable.uniqueKeys()[k]));
                        }

                        newTimetable.insert(timetable.uniqueKeys()[k],(*toSort.begin()));
                    }
                }

                if(to)
                {
                    toMap = newTimetable;
                    table->initTable(toMap);
                }
                else
                {
                     backMap = newTimetable;
                     table->initTable(backMap);
                }

                table->setTimeTable(newTimetable);


                qDebug() << "Working!";
            }
        }
    }
}

QPushButton* AddTransport::GenerateButton(QString text)
{
    QPushButton* button = new QPushButton();
    button->setText(text);
    button->setFont(QFont("MS Shell Dlg 2", 14));
    //ui->mainLayout->addWidget(editButton);
    button->setStyleSheet("border: 0; color: 'white'; background-color: #34495e;");
    button->setCursor(QCursor(Qt::PointingHandCursor));
    button->setMinimumHeight(40);
    return  button;
}



void AddTransport::on_showParameters_clicked()
{
    if(ui->ParameterWidget->isHidden())
    {
        ui->ParameterWidget->show();
    }
    else
    {
        ui->ParameterWidget->hide();
    }
}

void AddTransport::on_addStop_clicked()
{
    if(stopWidget->isHidden())
    {
        ui->mainLayout->removeItem(spacer);
        stopWidget->show();
    }
    else
    {
        ui->mainLayout->removeWidget(editButton);
        ui->mainLayout->addSpacerItem(spacer);
        ui->mainLayout->addWidget(editButton);
        stopWidget->hide();
    }
}

void AddTransport::SwapTable_clicked()
{
    if(to){
        to = false;
        table->initTable(backMap);
    }else{
        to = true;
        table->initTable(toMap);
    }
}

void AddTransport::on_setTransportType_currentIndexChanged(int index)
{
    switch (index) {
    case 0:{
        specsWidget->setType(vehicleType::bus);
        break;
    }case 1:{
        specsWidget->setType(vehicleType::trolleybus);
        break;
    }case 2:{
        specsWidget->setType(vehicleType::tram);
        break;
    }
    }
    specsWidget->init();
}
