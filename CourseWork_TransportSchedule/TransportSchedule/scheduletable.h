#ifndef SCHEDULETABLE_H
#define SCHEDULETABLE_H
#include <QTableWidget>

class ScheduleTable : public QTableWidget
{
    Q_OBJECT
private:
    QMultiMap<int, int> elements;
    bool editingMode = false;
    void RenderTable();
public:
    explicit ScheduleTable(QWidget* parent);
    ~ScheduleTable();
    void SetTableHeaders(QString, QString, int);
    void AddHour(int, int, QString);
    void AddMinutes(int, int, QString);
    void initTable(QMultiMap<int, int>);
    void setTimeTable(QMultiMap<int, int>);
    QMultiMap<int, int> getTimeTable();
    void editMode();
    void browseMode();
private slots:
    void itemChanged(int, int);
    void SelectionChanged();
};

#endif // SCHEDULETABLE_H
