#ifndef SCHEDULEWINDOWWIDGET_H
#define SCHEDULEWINDOWWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include "mainwindow.h"
#include "statusbar.h"
#include "vehicle.h"
#include "topreviousscreen.h"
#include "stop.h"
#include <QTableWidget>
#include <QPushButton>
#include "scheduletable.h"
#include "transportspecs.h"

class ChooseStop;

namespace Ui {
class ScheduleWindowWidget;
}

class ScheduleWindowWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ScheduleWindowWidget(QWidget *parent = nullptr, MainWindow* window = nullptr, Vehicle* vehicle = nullptr);
    ~ScheduleWindowWidget();
    QMultiMap<int, int> generateScheduleTableByStopId(int id = 0);
    void nextSchedule(QMultiMap<int, int>);
private:
    Ui::ScheduleWindowWidget *ui;
    MainWindow* window;

    StatusBar* statusBar = nullptr;
    ToPreviousScreen* toMainWidget = nullptr;
    Vehicle* vehicle;

    bool to;
    days day;

    QList<Stop> stops;
    ScheduleTable* table;
    int currentStop = 0;
    QPushButton* editButton;
    QPushButton* showTransportSpecs;
    transportSpecs* specsWidget;

    ChooseStop* chooseZone;

    QLabel* daysLabel;
    QComboBox* editDays;

    QString getDayString();
    void update();

    friend MainWindow;
    friend ChooseStop;
private slots:
    void EditButton_clicked();
    void TableWidget_itemChanged(int, int);
    void TableWidget_SelectionChanged();
    void showSpecs_clicked();
};

#endif // SCHEDULEWINDOWWIDGET_H
