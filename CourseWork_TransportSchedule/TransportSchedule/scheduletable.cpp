#include "scheduletable.h"
#include <QHBoxLayout>
#include <QLabel>
#include "timetableelement.h"
#include <QHeaderView>
#include <QPalette>
#include <QDebug>

ScheduleTable::ScheduleTable(QWidget* parent) : QTableWidget (parent)
{
    //this->connect(this, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(itemChanged(int, int)));
    //this->connect(this, SIGNAL(itemSelectionChanged()), this, SLOT(SelectionChanged()));
}

ScheduleTable::~ScheduleTable()
{

}

void ScheduleTable::initTable(QMultiMap<int, int> times)
{
    if(editingMode)
    {
        this->setRowCount(25);
    }
    else {
        this->setRowCount(times.uniqueKeys().size()+1);
    }

    //qDebug() << times.size() << endl;

    this->setColumnCount(2);
    elements = times;
    QPalette palette  = this->palette();
    palette.setBrush(QPalette::Highlight,QBrush(QColor("#2c3e50")));
    this->setPalette(palette);
    this->setShowGrid(false);
    this->setStyleSheet("border:0px");
    this->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    this->horizontalHeader()->setHidden(true);
    this->verticalHeader()->setHidden(true);
    //this->setFocusPolicy(Qt::NoFocus);
    //this->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    SetTableHeaders("Час", "background-color:#34495e; color: 'white';", 0);
    SetTableHeaders("Минуты", "background-color:#34495e; color: 'white';", 1);

    auto keys = times.uniqueKeys();

    if(editingMode)
    {
        for(int i=1; i<25; ++i)
        {
            AddHour(i, 0, QString::number(i-1));
            auto it = times.find(i-1);


            if(it != times.end())
            {
                QString minutes = "";

                for(int j=0; j<times.count(i-1); ++j)
                {
                    if((*it)== -1)
                    {
                        minutes = "";
                        break;
                    }
                    if(!minutes.length()) minutes += QString::number((*it));
                    else minutes += ", " + QString::number((*it));
                    ++it;
                }

                AddMinutes(i,1, minutes);
            }
            else
            {
                AddMinutes(i,1,"");
            }
        }
    }
    else
    {
        for(int i=1; i<keys.length()+1; ++i)
        {
            //qDebug() << keys.length() << endl;
            AddHour(i, 0, QString::number(keys[i-1]));

            auto it = times.find(keys[i-1]);

            QString minutes = "";


            for(int j=0; j<times.count(keys[i-1]); ++j)
            {
                if((*it) == -1)
                {
                    minutes = "";
                    break;
                }
                if(!minutes.length()) minutes += QString::number((*it));
                else minutes += ", " + QString::number((*it));
                ++it;
            }

            AddMinutes(i,1, minutes);
        }
    }
}

void ScheduleTable::SetTableHeaders(QString text, QString style, int columnId)
{
    QWidget* time = new QWidget(nullptr);
    QHBoxLayout* timeLayout = new QHBoxLayout(nullptr);
    timeLayout->setContentsMargins(0,0,0,0);
    QSpacerItem* spacer1 = new QSpacerItem(12,12, QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    QSpacerItem* spacer2 = new QSpacerItem(12,12, QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    QLabel* label = new QLabel(text);
    label->setFont(QFont("MS Shell Dlg 2", 12));
    timeLayout->addSpacerItem(spacer1);
    timeLayout->addWidget(label);
    timeLayout->addSpacerItem(spacer2);
    time->setStyleSheet(style);
    time->setLayout(timeLayout);
    this->setCellWidget(0,columnId,time);
}

void ScheduleTable::AddHour(int row, int column, QString time)
{
    TimeTableElement* element = new TimeTableElement(time, true);
    this->setCellWidget(row,column,element);
}

void ScheduleTable::AddMinutes(int row, int column, QString time)
{
    TimeTableElement* element = new TimeTableElement(time);
    this->setCellWidget(row,column,element);
}

void ScheduleTable::editMode()
{
    editingMode = true;
    initTable(elements);
}

void ScheduleTable::browseMode()
{
    editingMode = false;
    initTable(elements);
}

void ScheduleTable::setTimeTable(QMultiMap<int, int> times)
{
    elements = times;
}

void ScheduleTable::itemChanged(int i, int j)
{
    //editingMode
    if(editingMode && i && j)
    {
        SelectionChanged();

        TimeTableElement* element = dynamic_cast<TimeTableElement*>(this->cellWidget(i,j));
        element->SetEditable();
    }
}

void ScheduleTable::SelectionChanged()
{
    for(int i=0; i<this->rowCount(); ++i)
    {
        for(int j=0; j<this->colorCount(); ++j)
        {
            TimeTableElement* element = dynamic_cast<TimeTableElement*>(this->cellWidget(i,j));
            if(element && element->IsInEditingMode())
            {
                QString text = element->SetUneditable();
            }
        }
    }
}

QMultiMap<int, int> ScheduleTable::getTimeTable()
{
    return elements;
}
