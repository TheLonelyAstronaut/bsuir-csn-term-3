#ifndef SCHEDULETIME_H
#define SCHEDULETIME_H
#include <QList>

class ScheduleTime
{
private:
    short hour, minute;
public:
    ScheduleTime() {}
    ScheduleTime(short _hour, short _minute);
    short getHour();
    short getMinute();
    //friend class Stop;
    bool operator==(const ScheduleTime&);
};

#endif // SCHEDULETIME_H
