#ifndef ISALREADYINCLUDEDEXCEPTION_H
#define ISALREADYINCLUDEDEXCEPTION_H
#include "Exception.h"


class isAlreadyIncludedException : public Exception
{
protected:
    int includedValue;
public:
    isAlreadyIncludedException(QString _message = "", int _includedValue = 0);
    ~isAlreadyIncludedException();
    int getIncludedValue();
};

#endif // ISALREADYINCLUDEDEXCEPTION_H
