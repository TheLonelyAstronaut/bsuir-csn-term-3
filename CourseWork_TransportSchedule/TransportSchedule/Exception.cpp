#include "Exception.h"
#include <QString>
#include <QList>
#include <iostream>
#include "ArgumentOutOfRangeException.h"
#include "isalreadyincludedexception.h"
#include "InputSizeException.h"
#include "InvalidLanguageException.h"
#include "incorrectinputexception.h"
using namespace std;

Exception::Exception(QString _message) : message(_message) {}

Exception::~Exception() {}

QString Exception::Message() { return message;  }

QList<int> Exception::isMinuteString(QString time)
{
    QList<int> minutes;

    auto minutesArray = time.split(",");

    for (auto it = minutesArray.begin(); it < minutesArray.end(); ++it) {
        int test = (*it).toInt();

        if(test > 59 || test < 0) throw ArgumentOutOfRangeException("Некорректный формат!", 59, 0);
        if(minutes.count(test)) throw isAlreadyIncludedException("Данное число уже записано!", test);

        if(!test)
        {
            bool isZero = false;

            for (auto strIt = it->begin(); strIt != it->end(); ++strIt) {
                if((*strIt) == '0' && !isZero)
                {
                     isZero = true;
                     continue;
                }
                else if(((*strIt) == '0' && isZero) || (*strIt) != ' ')
                {
                    isZero = false;
                    throw IncorrectInputException("Введены некорректные данные!", "0-9, ',', ' '");
                }
            }

            if((*it).length()) minutes.push_back(0);

        }else{
            minutes.push_back(test);
        }
    }

    std::sort(minutes.begin(), minutes.end());
    return minutes;
}

int Exception::isNumber(QString parseble)
{
    for(auto it=parseble.begin(); it!=parseble.end(); ++it){
        if((*it) < '0' || (*it) > '9') throw IncorrectInputException("Введены некорректные данные!", "0-9");
    }

    int number = parseble.toInt();

    return number;
}
