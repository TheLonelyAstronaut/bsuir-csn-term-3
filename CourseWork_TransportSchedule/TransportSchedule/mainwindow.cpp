#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtOpenGL>
#include <QDebug>
#include "transporttile.h"
#include "schedulewindowwidget.h"
#include "statusbar.h"
#include "mainwindowwidget.h"
#include "searchandsort.h"
#include "doublylinkedlist.h"
#include "doublylinkedlist.cpp"
#include "stop.h"
#include "bus.h"
#include "trolleybus.h"
#include "tram.h"
#include "vehicle.h"
#include "oilvehicle.h"
#include "route.h"
#include "enums.h"
#include "addtransport.h"
#include "choosestop.h"
#include "fileexception.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include "addvehiclecommand.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    setMouseTracking(true);

    try {
        this->initializeSchedule();
    } catch (FileException ex) {
        QMessageBox::warning(nullptr, ex.getFilePath(), ex.Message());
    }

    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->resize(400, 850);
    this->NavigateToMainScreen(this);
    ui->centralWidget->setContentsMargins(0,0,0,0);
    //this->NavigateToSchedule(new Bus());
}

void MainWindow::NavigateToMainScreen(MainWindow* window)
{
    currentScreen = "Main";
    mainWidget = new MainWindowWidget(this, window);
    this->setCentralWidget(mainWidget);
}

void MainWindow::NavigateToSchedule(Vehicle * vehicle)
{
    currentScreen = "Schedule";
    scheduleWidget = new ScheduleWindowWidget(this, this, vehicle);
    this->setCentralWidget(scheduleWidget);
}

void MainWindow::NavigateToAddTransport()
{
    currentScreen="Add";
    addtransportWidget = new AddTransport(this, this);
    this->setCentralWidget(addtransportWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initializeSchedule()
{
    auto busList = Bus::ReadBusFromFile();
    auto trolleyList = TrolleyBus::ReadTrolleyBusFromFile();
    auto tramList = Tram::ReadTramFromFile();

    QFile stops("textdatabase/stops.txt");
    QFile routes("textdatabase/routes.txt");
    QFile timetable("textdatabase/timetable.txt");
    if(!stops.open(QIODevice::ReadOnly) ||  !timetable.open(QIODevice::ReadOnly) || !routes.open(QIODevice::ReadOnly)) {
        QString filename=stops.isOpen() ? "stops.txt" : (timetable.isOpen() ? "timetable.txt": "routes.txt");
        throw FileException("Невозможно открыть файл", "textdatabase/" + filename);
        //QMessageBox::information(0, "error", stops.errorString());
        //qDebug() << "Opening error!";
    }

    QMap<QString, Stop> stopsMap;

    QTextStream stopsIn(&stops);
    QTextStream routesIn(&routes);
    QTextStream timetableIn(&timetable);

    QString line = stopsIn.readLine();

    while(!stopsIn.atEnd()) {
        try {
            line = stopsIn.readLine();
            QStringList splitted = line.split(';');
            if(splitted.length() != 2) throw exception();
            Stop stop = Stop(splitted[0], splitted[1].split('\r')[0]);
            stopsMap[splitted[0]] = stop;
        } catch (...) {
            stops.close();
            routes.close();
            timetable.close();
            throw FileException("Проверьте сигнатуру файла", "textdatabase/stops.txt");
        }
    }

    stopCounter = stopsMap.keys().length();
    stops.close();

    line = routesIn.readLine();

    while(!routesIn.atEnd()) {
        try {
            line = routesIn.readLine();
            QStringList splitted = line.split(';');
            qDebug() << splitted;


            days day = days::allweek;
            if(splitted[3] == "weekdays") day = days::weekdays;
            if(splitted[3] == "weekends") day = days::weekends;
            if(splitted[3] == "allweek") day = days::allweek;

            Route route = Route(splitted[0], splitted[1]);
            route.setDay(day);

            if(splitted[2] == "bus")
            {
                //int i = 1;
                auto it = busList.begin();

                if(splitted[4].toInt() == 0) throw exception();

                for(; (*it)->getVehicleId() != splitted[4].toInt(); ++it);


                busMap.push_back(new Bus(route, (*it)->getHumanCapacity(), (*it)->getModel(), (*it)->getEngineVolume(), (*it)->getSectionCount(), (*it)->getVehicleId()));
            }
            if(splitted[2] == "trol"){

                //int i = 1;
                auto it = trolleyList.begin();
                for(; (*it)->getVehicleId() != splitted[4].toInt(); ++it);

                trolleybusMap.push_back(new TrolleyBus(route, (*it)->getHumanCapacity(), (*it)->getModel(), (*it)->getElectricPower(), (*it)->getBatteryCapacity(), (*it)->getVehicleId()));
            }
            if(splitted[2] == "tram")
            {
                //int i = 1;
                auto it = tramList.begin();
                for(; (*it)->getVehicleId() != splitted[4].toInt(); ++it);

                tramMap.push_back(new Tram(route, (*it)->getHumanCapacity(), (*it)->getModel(), (*it)->getElectricPower(), (*it)->getLowFloor(), (*it)->getVehicleId()));
            }
        } catch (...) {
            tramMap.clear();
            busMap.clear();
            trolleybusMap.clear();
            routes.close();
            timetable.close();
            throw FileException("Проверьте сигнатуру файла", "textdatabase/routes.txt");
        }
    }

    routes.close();

    line = timetableIn.readLine();

    while(!timetableIn.atEnd())
    {
        try {
            line = timetableIn.readLine();

            QStringList splitted = line.split(';');

            Vehicle* vehicle = getVehicleById(splitted[1]);

            Stop currentStop = stopsMap[splitted[0]];

            QList<ScheduleTime> timeToList = QList<ScheduleTime>();
            QList<ScheduleTime> timeBackList = QList<ScheduleTime>();


            QJsonDocument doc = QJsonDocument::fromJson(splitted[2].toUtf8());
            QJsonObject jObject = doc.object();
            auto timeTo = jObject.toVariantMap()["A>B"].toMap();
            auto timeBack = jObject.toVariantMap()["A<B"].toMap();

            if(!timeTo.size() || !timeBack.size() || !vehicle) throw exception();

            foreach(auto key, timeTo.keys())
            {
                auto list = timeTo[key].toList();
                foreach(auto minute, list)
                {
                    timeToList.push_back(ScheduleTime(key.toShort(), minute.toInt()));
                }
            }

            //qDebug() << timeBack;
            foreach(auto key, timeBack.keys())
            {
                auto list = timeBack[key].toList();

                foreach(auto minute, list)
                {
                    timeBackList.push_back(ScheduleTime(key.toShort(), minute.toInt()));
                }
            }

            currentStop.setTimeTable(timeToList, timeBackList);
            vehicle->addStopToRoute(currentStop);
        } catch (...) {
            tramMap.clear();
            busMap.clear();
            trolleybusMap.clear();
            timetable.close();
            throw FileException("Проверьте сигнатуру файла", "textdatabase/timetable.txt");
        }
    }

    timetable.close();
    //stops.close();
}

Vehicle* MainWindow::getVehicleById(QString id)
{
    foreach(auto vehicle, busMap)
    {
        if(vehicle->GetRouteNumber() == id) return vehicle;
    }

    foreach(auto vehicle, trolleybusMap)
    {
        if(vehicle->GetRouteNumber() == id) return vehicle;
    }

    foreach(auto vehicle, tramMap)
    {
        if(vehicle->GetRouteNumber() == id) return vehicle;
    }

    return nullptr;
}

void MainWindow::deleteVehicle(Vehicle *vehicle)
{
    if(vehicle->GetEnumType() == vehicleType::bus)
    {
        for(auto it = busMap.begin(); it != busMap.end(); ++it)
        {
            //qDebug() <<(*it)->GetRouteName();
            if(vehicle->getVehicleId() == (*it)->getVehicleId() && vehicle->getRoute().getRouteNumber() == (*it)->getRoute().getRouteNumber())
            {
                Route::RemoveRouteFromFile(vehicle->getRoute());
                auto stops = vehicle->getRoute().getStops();

                for(auto stopsIt = stops.begin(); stopsIt != stops.end(); ++stopsIt)
                {
                    Stop::RemoveTimetableFromFile((*stopsIt), vehicle);
                }


                busMap.erase(it);
                break;
            }
        }

        mainWidget->RenderTransportComponents(busMap, true);
    }
    if(vehicle->GetEnumType() == vehicleType::trolleybus)
    {
        for(auto it = trolleybusMap.begin(); it != trolleybusMap.end(); ++it)
        {
            if(vehicle->getVehicleId() == (*it)->getVehicleId() && vehicle->getRoute().getRouteNumber() == (*it)->getRoute().getRouteNumber())
            {
                Route::RemoveRouteFromFile(vehicle->getRoute());
                auto stops = vehicle->getRoute().getStops();

                for(auto stopsIt = stops.begin(); stopsIt != stops.end(); ++stopsIt)
                {
                    Stop::RemoveTimetableFromFile((*stopsIt), vehicle);
                }

                trolleybusMap.erase(it);
                break;
            }
        }

        mainWidget->RenderTransportComponents(busMap, true);
    }
    if(vehicle->GetEnumType() == vehicleType::tram)
    {
        for(auto it = tramMap.begin(); it != tramMap.end(); ++it)
        {
            if(vehicle->getVehicleId() == (*it)->getVehicleId() && vehicle->getRoute().getRouteNumber() == (*it)->getRoute().getRouteNumber())
            {
                Route::RemoveRouteFromFile(vehicle->getRoute());
                auto stops = vehicle->getRoute().getStops();

                for(auto stopsIt = stops.begin(); stopsIt != stops.end(); ++stopsIt)
                {
                    Stop::RemoveTimetableFromFile((*stopsIt), vehicle);
                }

                tramMap.erase(it);
                break;
            }
        }

        mainWidget->RenderTransportComponents(busMap, true);
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    pressedKeys -= event->key();
    //qDebug() << event->key();

    //qDebug() << "Stack size: " << undoStack.count();
    /*if(event->key() == 16777234 && currentScreen == "Schedule")
    {
        scheduleWidget->chooseZone->on_previousStop_clicked();

        //if(!mainWidget->isHidden()) mainWidget->hide();
    }
    if(event->key() == 16777236 && currentScreen == "Schedule")
    {
        scheduleWidget->chooseZone->on_nextStop_clicked();
        //mainWidget->hide();
        //if(!mainWidget->isHidden()) mainWidget->hide();
    }*/
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << event->key() << "Here";
    //pressedKeys -= event->key();

    if(event->key() == Qt::Key_Escape && currentScreen != "Main")
    {
        NavigateToMainScreen(this);
    }
    pressedKeys += event->key();
    if(pressedKeys.contains(Qt::Key_Z) && pressedKeys.contains(Qt::Key_Control))
    {
        undoStack.undo();

        if(currentScreen == "Schedule")
        {
            scheduleWidget->stops = scheduleWidget->vehicle->getRoute().getStops();
            scheduleWidget->update();
        }

        return;
    }
    if(pressedKeys.contains(Qt::Key_X) && pressedKeys.contains(Qt::Key_Control))
    {
        //qDebug() << "Redo?";
        undoStack.redo();

        if(currentScreen == "Schedule")
        {
            scheduleWidget->stops = scheduleWidget->vehicle->getRoute().getStops();
            scheduleWidget->update();
        }

        return;
    }
}
