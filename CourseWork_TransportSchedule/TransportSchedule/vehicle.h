#ifndef VEHICLE_H
#define VEHICLE_H
#include <QString>
#include "route.h"
#include "enums.h"

class Vehicle
{
protected:
    Route vehicleRoute;
    int humanCapacity;
    QString model;
    int vehicleId;
public:
    Vehicle();
    Vehicle(Route _route, int _humanCapacity = 0, QString _model = "", int _vehicleId = 0);
    int getHumanCapacity();
    void setHumanCapacity(int _capacity);
    void setModel(QString _model);
    QString getModel();
    void addStopToRoute(Stop);
    Route getRoute();
    void setRoute(Route);
    void setStops(QList<Stop>);
    void setVehicleId(int);
    int getVehicleId();
    virtual ~Vehicle() {}
    virtual QString GetRouteNumber() = 0;
    virtual QString GetRouteName() = 0;
    virtual vehicleType GetEnumType() = 0;
    friend bool operator==(Vehicle& lhs, Vehicle& rhs);
};

#endif // VEHICLE_H
