#include "transporttile.h"
#include "mainwindow.h"
#include "ui_transporttile.h"
#include "vehicle.h"
#include <QDebug>
#include <QMouseEvent>
#include <QLabel>
#include <QFont>
#include "enums.h"
#include <QMenu>
#include "deletevehiclecommand.h"
TransportTile::TransportTile(MainWindow *parent, Vehicle* vehicle) :
    QWidget(parent),
    ui(new Ui::TransportTile),
    window(parent)
{
    setMouseTracking(true);
    ui->setupUi(this);
    this->setCursor(Qt::PointingHandCursor);
    this->vehicle = vehicle;
    QString color = this->vehicle->GetEnumType() == vehicleType(bus) ? "red" : ( this->vehicle->GetEnumType() == vehicleType(trolleybus) ? "blue" : "white");
    ui->transportTile->setStyleSheet("#transportTile{ \
                                     border: 2px solid " + color  + "; \
                                     border-radius: 5px; \
                                 }");
    QLabel* transportRoute = new QLabel(this->vehicle->GetRouteName());
    QLabel* transportNumber = new QLabel(this->vehicle->GetRouteNumber());
    QString style = "font: 14pt; color: white;";
    transportRoute->setStyleSheet(style);
    transportNumber->setStyleSheet(style);
    QHBoxLayout* layout = new QHBoxLayout(nullptr);
    QSpacerItem* spacer = new QSpacerItem(0,0, QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
    deleteButton = new QPushButton();
    deleteButton->setText("");
    deleteButton->setStyleSheet("height: 15;\
                                width: 15;\
                                border-image: url(':/icons/close.png');");

    ui->transportTile->layout()->addWidget(transportNumber);
    ui->transportTile->layout()->addWidget(transportRoute);
    ui->horizontalLayout_2->addSpacerItem(spacer);
    ui->horizontalLayout_2->addWidget(deleteButton);

    this->connect(deleteButton, SIGNAL(clicked()), this, SLOT(onDelete_click()));
    //connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(CustomMenu_show(const QPoint&)));
    //ui->transportTile->setLayout(layout);
}

TransportTile::~TransportTile()
{
    delete ui;
}

void TransportTile::mousePressEvent ( QMouseEvent * event )
{
    auto test = vehicle->getRoute().getStops()[0].getBackTimetable();

            for(auto it = test.begin(); it !=test.end(); ++it)
            {
                qDebug () << it->getHour() << it->getMinute();
            }

            this->window->NavigateToSchedule(vehicle);
}

void TransportTile::onDelete_click()
{
    //qDebug() << vehicle->GetRouteName();
    this->window->undoStack.push(new DeleteVehicleCommand(this->window, vehicle));
}
