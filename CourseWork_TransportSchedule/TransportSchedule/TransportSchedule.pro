#-------------------------------------------------
#
# Project created by QtCreator 2019-09-10T21:40:57
#
#-------------------------------------------------

QT += core gui
QT += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TransportSchedule
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        ArgumentOutOfRangeException.cpp \
        Exception.cpp \
        InputSizeException.cpp \
        addtransport.cpp \
        addvehiclecommand.cpp \
        bus.cpp \
        choosestop.cpp \
        deletevehiclecommand.cpp \
        doublylinkedlist.cpp \
        editroutecommand.cpp \
        electricvehicle.cpp \
        fileexception.cpp \
        incorrectinputexception.cpp \
        isalreadyincludedexception.cpp \
        main.cpp \
        mainwindow.cpp \
        mainwindowwidget.cpp \
        oilvehicle.cpp \
        route.cpp \
        scheduletable.cpp \
        scheduletime.cpp \
        schedulewindowwidget.cpp \
        searchandsort.cpp \
        statusbar.cpp \
        stop.cpp \
        timetableelement.cpp \
        topreviousscreen.cpp \
        tram.cpp \
        transportspecs.cpp \
        transporttile.cpp \
        trolleybus.cpp \
        vehicle.cpp

HEADERS += \
        ArgumentOutOfRangeException.h \
        Exception.h \
        InputSizeException.h \
        Node.h \
        addtransport.h \
        addvehiclecommand.h \
        bus.h \
        choosestop.h \
        deletevehiclecommand.h \
        doublylinkedlist.h \
        editroutecommand.h \
        electricvehicle.h \
        enums.h \
        fileexception.h \
        incorrectinputexception.h \
        isalreadyincludedexception.h \
        mainwindow.h \
        mainwindowwidget.h \
        oilvehicle.h \
        route.h \
        scheduletable.h \
        scheduletime.h \
        schedulewindowwidget.h \
        searchandsort.h \
        statusbar.h \
        stop.h \
        timetableelement.h \
        topreviousscreen.h \
        tram.h \
        transportspecs.h \
        transporttile.h \
        trolleybus.h \
        vehicle.h

FORMS += \
        addtransport.ui \
        choosestop.ui \
        mainwindow.ui \
        mainwindowwidget.ui \
        schedulewindowwidget.ui \
        searchandsort.ui \
        statusbar.ui \
        timetableelement.ui \
        topreviousscreen.ui \
        transportspecs.ui \
        transporttile.ui

RESOURCES +=\
        application.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
