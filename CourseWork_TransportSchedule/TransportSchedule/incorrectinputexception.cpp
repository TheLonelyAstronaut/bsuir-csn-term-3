#include "incorrectinputexception.h"

IncorrectInputException::IncorrectInputException(QString _message, QString  _allowedSymbols): Exception (_message), allowedSymbols(_allowedSymbols)
{

}

IncorrectInputException::~IncorrectInputException() {}

QString IncorrectInputException::getAllowedSymbols()
{
    return allowedSymbols;
}
