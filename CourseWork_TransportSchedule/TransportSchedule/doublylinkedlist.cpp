#include "doublylinkedlist.h"
#include <QDebug>

// default constructor w/ no parameters
template<class T>
DoublyLinkedList<T>::const_iterator::const_iterator() : current{nullptr}
{
}
// operator* returns the element (to be retrieve later)  (protected)
template<class T>
const T & DoublyLinkedList<T>::const_iterator::operator*() const
{
    return retrieve();
}
// operators various
template<class T>
typename DoublyLinkedList<T>::const_iterator & DoublyLinkedList<T>::const_iterator::operator++()
{  // increment operator, pointer to the next nodo
    current = current->next;    return *this;
    // basically returns the instance of this class, * = dereference, and "this" is the pointer to the instance
}
template<class T>
typename DoublyLinkedList<T>::const_iterator DoublyLinkedList<T>::const_iterator::operator++(int)  // ++(int) postfix increment
{ // return a copy of original object and then increment by 1
    auto cpy = *this;  //using dereferencing and member access.
    current = current->next; 	return cpy;
}
template<class T>
typename DoublyLinkedList<T>::const_iterator & DoublyLinkedList<T>::const_iterator::operator--() // decrement
{
    current = current->prev; 	return *this;
}
template<class T>
typename DoublyLinkedList<T>::const_iterator DoublyLinkedList<T>::const_iterator::operator--(int) //--(int) posfix decrement
{
    auto cpy = *this;
    current = current->prev;	return *this;
}
// comparisons
template<class T>
bool DoublyLinkedList<T>::const_iterator::operator==(const typename DoublyLinkedList<T>::const_iterator & rhs) const
{
    return current == rhs.current; // refer to the same element? ==
}
template<class T>
bool DoublyLinkedList<T>::const_iterator::operator!=(const typename DoublyLinkedList<T>::const_iterator & rhs) const
{
    return current != rhs.current; // they do not refer to the same element? !=
}
// protected constructor
template<class T>
DoublyLinkedList<T>::const_iterator::const_iterator(typename DoublyLinkedList<T>::Node* p) : current{p}
{
}
//friend class DoublyLinkedList<T>;

// nested iterator class begin, it will return a modifiable reference of current Node
template<class T>
DoublyLinkedList<T>::iterator::iterator()  // constructor of derived iterator class
{
}
template<class T>
T & DoublyLinkedList<T>::iterator::operator*() // this operator* is the one will return a modifiable
//reference of the data in current Node
{
    return this->current->data;
}
// retrieve the element refers to current node's data
template<class T>
T & DoublyLinkedList<T>::const_iterator::retrieve() const
{
    return current->data;
}
template<class T>
const T & DoublyLinkedList<T>::iterator::operator*() const
{
    return this->retrieve();
}
// operators needed again
template<class T>
typename DoublyLinkedList<T>::iterator & DoublyLinkedList<T>::iterator::operator++()
{  //this->current to call base's protected data
    this->current = this->current->next; 	return *this;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::iterator::operator++(int)
{
    auto cpy = *this;
    this->current = this->current->next; 	return cpy;
}
template<class T>
typename DoublyLinkedList<T>::iterator & DoublyLinkedList<T>::iterator::operator--()
{
    this->current = this->current->prev;	return *this;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::iterator::operator--(int)
{
    auto cpy = *this;
    this->current = this->current->prev;	return *this;
}
template<class T>
DoublyLinkedList<T>::iterator::iterator(typename DoublyLinkedList<T>::Node* p) : DoublyLinkedList<T>::const_iterator{p}
{
}
template<class T>
DoublyLinkedList<T>::DoublyLinkedList()    // constructor, no parameter and will call member function
{
    init(); // initialization private function
}
template<class T>
DoublyLinkedList<T>::~DoublyLinkedList() // destructor
{
    clear(); // housekeeping job, // delete elements as needed
    delete head;  // head node
    delete tail;  // tail node
}
template<class T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T> & rhs) // copy constructor
{
    init();  // call function to do the copy need it
    for(auto itr = rhs.begin(); itr != rhs.end(); ++itr)
        push_back(*itr); // move
}

template <class T>
template <class K>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<K>& p)
{
    init();
    auto iterator = p.begin();


    for(;iterator!=p.end(); ++iterator)
    {
        this->push_back((*iterator));
    }
}

template<class T> // move constructor
DoublyLinkedList<T>::DoublyLinkedList( DoublyLinkedList<T> && rhs ) : theSize(rhs.theSize), head{rhs.head}, tail{rhs.tail}
{
    rhs.theSize = 0;	rhs.head = nullptr;		rhs.tail = nullptr;
}
template<class T> // copy assignment
const DoublyLinkedList<T> & DoublyLinkedList<T>::operator=(const DoublyLinkedList<T> & rhs)
{
    this->clear();

    for(auto it = rhs.begin(); it != rhs.end(); ++it)
    {
        this->push_back((*it));
    }

    return *this;
}
template<class T>	// move assignment
DoublyLinkedList<T> & DoublyLinkedList<T>::operator=(DoublyLinkedList<T> && rhs)
{
    qDebug() << "Operator=";
    init();
    DoublyLinkedList<T> test = rhs;

    for(auto it = test.begin(); it != test.end(); ++it)
    {
        this->push_back((*it));
    }

    rhs.clear();

    return *this;
}
template<class T>
DoublyLinkedList<T>::DoublyLinkedList(int num, const T & val)
{
    init(); int index;
    for(index = 0; index < num; index++)
        push_back(val); // note  // insert val ahead of itr
}
template<class T> // constructor w/ element [start, end)
DoublyLinkedList<T>::DoublyLinkedList(typename DoublyLinkedList<T>::const_iterator start, typename DoublyLinkedList<T>::const_iterator end)
{
    init();
    for(auto itr= start; itr != end; ++itr)
        push_back( *itr );
}
template<class T>   // number of elements
int DoublyLinkedList<T>::size() const
{
    return theSize;
}
template<class T>
void DoublyLinkedList<T>::reverse() // to reverse the order of elements if not empty
{
    if(!empty()) // check to be valid
    {
        auto current_ptr=head;
        while(current_ptr != nullptr)
        {
            std::swap(current_ptr->next, current_ptr->prev);
            current_ptr=current_ptr->prev;
        }
    }
    std::swap(head, tail); // awesome
}
template<class T> // first element
T & DoublyLinkedList<T>::front()
{
    return *begin();  // ref. iterator to first element
}
template<class T>
const T & DoublyLinkedList<T>::front() const // now, w/ no permission to modify 1st elem.
{
    return *begin();
}
template<class T> //  last element
T & DoublyLinkedList<T>::back()
{
    return *(--end());
}
template<class T> // now, w/ no permission to modify last elem.
const T & DoublyLinkedList<T>::back() const
{
    return *(--end());
}
template<class T>
void DoublyLinkedList<T>::push_front( T && val)
{
    insert(begin(), std::move( val));
}
// insert to the beginning, alias, insert right after Node* head
template<class T>
void DoublyLinkedList<T>::push_front(const T & val)
{
    insert(begin(), val);  // insert after Node head
}
template<class T>
void DoublyLinkedList<T>::push_back(T && val) // move version insert_back
{
    insert( end(), std::move( val));
}
template<class T>
void DoublyLinkedList<T>::pop_front() // delete first element
{
    erase(begin());
}
template<class T>
void DoublyLinkedList<T>::clear()
{
    while(!empty())
        pop_back(); // to delete all the elements
}
// insert to the end, alias, insert right before Node* tail
template<class T>
void DoublyLinkedList<T>::push_back(const T & val)
{
    insert(end(), val);
}
template<class T>
void DoublyLinkedList<T>::pop_back()
{
    erase(--end()); // erase one element, the last one.
}
template<class T>
bool DoublyLinkedList<T>::empty() const
{
    return size() == 0; // ==0  check if list is empty of nodes
}
template<class T>
void DoublyLinkedList<T>::remove(const T & val) // remove all elements with value val
{
    for(auto itr = begin(); itr != end(); ++itr)
    {
        if (*itr == val)
        {
            erase(itr);
        }
    }
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::begin()
{
    typename DoublyLinkedList<T>::iterator itr{head->next};
    return itr; // begining node (copy)
}
template<class T>
typename DoublyLinkedList<T>::const_iterator DoublyLinkedList<T>::begin() const
{
    typename DoublyLinkedList<T>::const_iterator const_itr{head->next};
    return const_itr;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::end()
{
    typename DoublyLinkedList<T>::iterator itr{ tail };
    return itr; // return the end node (copy)
}
template<class T>
typename DoublyLinkedList<T>::const_iterator DoublyLinkedList<T>::end() const
{
    typename DoublyLinkedList<T>::const_iterator const_itr{tail};
    return const_itr;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::insert(typename DoublyLinkedList<T>::iterator itr, const T & val)
{ // pointer "p" is copy of current node
    auto* p = itr.current;  theSize++;
    auto* nptr = new DoublyLinkedList<T>::Node{val, p->prev, p};
    p->prev->next = nptr;
    p->prev = nptr;
    typename DoublyLinkedList<T>::iterator iter{nptr};	return iter;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::insert(typename DoublyLinkedList<T>::iterator itr, T && val)
{
    auto* p = itr.current;	theSize++;
    auto* nptr = new DoublyLinkedList<T>::Node{ std::move( val ), p->prev, p};
    p->prev->next = nptr;
    p->prev = nptr;
    typename DoublyLinkedList<T>::iterator iter{nptr};	return iter;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::erase(typename DoublyLinkedList<T>::iterator start, typename DoublyLinkedList<T>::iterator end)
{
    for(auto itr = start; itr != end;) // erase from start to end
        itr = erase(itr); //  not including end. Alias, erase [start, end)

    return end;
}
template<class T>
void DoublyLinkedList<T>::init()  // init. private function
{
    theSize = 0;
    head = new Node();	tail = new Node();
    head->next = tail;	tail->prev = head;
}
template<class T>
typename DoublyLinkedList<T>::iterator DoublyLinkedList<T>::erase(typename DoublyLinkedList<T>::iterator itr)
{  // erase one element
    auto* p = itr.current;
    auto* test = itr.current;
    typename DoublyLinkedList<T>::iterator value{p->next};
    p->prev->next = test->next;
    p->next->prev = test->prev;
    theSize--;		return value;
}
// overloading comparison
template<class T>
bool operator!=(const DoublyLinkedList<T> & lhs, const DoublyLinkedList<T> & rhs)
{
    return !(lhs == rhs);
}
template<class T>
bool operator==(const DoublyLinkedList<T> & lhs, const DoublyLinkedList<T> & rhs)
{
    bool flag = true;
    if( lhs.size() == rhs.size())
    {
        auto rhs_itr = rhs.begin();
        for(auto lhs_itr=lhs.begin(); lhs_itr !=lhs.end(); ++lhs_itr)
        {
            if(*lhs_itr != *rhs_itr)
               flag = false; break;
            ++rhs_itr;
        }
        return flag;
    }
    else
        return false;
}
// overloading output
template<class T>
std::ostream & operator<<(std::ostream & os, const DoublyLinkedList<T> & l)
{
    l.print(os);	return os;
}
// print out all elements
template<class T>
void DoublyLinkedList<T>::print(std::ostream & os, char ofc) const
{
    for(auto itr = begin(); itr != end(); ++itr)
        os << *itr << ofc;  // ofc is the deliminitor
}

template<class T>
bool DoublyLinkedList<T>::iterator::operator>(DoublyLinkedList<T>::iterator rhs)
{
    typename DoublyLinkedList<T>::Node* pointer = rhs.current;

    for (; pointer != this->current && pointer->next; pointer = pointer->next);


    if (pointer->next)
    {
        return false;
    }
    else return true;
}

template<class T>
bool DoublyLinkedList<T>::iterator::operator<(DoublyLinkedList<T>::iterator rhs)
{
    if ((*this) > rhs) return false;
    else return true;
}

template<class T>
int DoublyLinkedList<T>::iterator::operator-(DoublyLinkedList<T>::iterator rhs)
{
    int distance = 0;
    DoublyLinkedList<T>::iterator ths = (*this);

    for (; ths.current && (ths < rhs); ++distance, ++rhs);

    return distance;
}

template<class T>
void DLLSort(typename DoublyLinkedList<T>::iterator begin, typename DoublyLinkedList<T>::iterator end, bool (*comparator)(T, T))
{
    //qDebug() << "Here";
    int size = (--end) - begin, i = 0, j = 0;
    qDebug() << size;

        if (!size) return;

        for (auto frst = begin; i < size - 1; i++, ++frst, j=0) {
            for (auto scnd = begin; j < size - i - 1; j++, ++scnd) {
                auto temp = ++scnd;
                --scnd;
                if((*comparator)((*scnd), (*temp)))
                {
                    T value = (*scnd);
                    (*scnd) = (*temp);
                    (*temp) = value;
                }
            }
        }
}
