#ifndef MAINWINDOWWIDGET_H
#define MAINWINDOWWIDGET_H

#include <QWidget>
#include "mainwindow.h"
#include "statusbar.h"
#include "searchandsort.h"
#include "doublylinkedlist.h"
#include "enums.h"
#include <QWidget>

namespace Ui {
class MainWindowWidget;
}

class MainWindowWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindowWidget(QWidget *parent = nullptr, MainWindow* window = nullptr);
    ~MainWindowWidget() override;
    void RenderTransportComponents(DoublyLinkedList<Vehicle*> transport = DoublyLinkedList<Vehicle*>(), bool renderFullList = false);
    vehicleType getCurrentType();
    void setCurrentType(vehicleType);
private:
    Ui::MainWindowWidget *ui;
    MainWindow* window;

    ////////////////////////////////////////////
    StatusBar* statusBar = nullptr;
    SearchAndSort* searchBar = nullptr;
    QWidget* schedule = nullptr;
    ////////////////////////////////////////////
    DoublyLinkedList<TransportTile*> list;
    void ClearLayout(QWidget*);
    vehicleType currentType = vehicleType(all);
    ///////////////////////////////////////////
    QPoint mpos; //For dragging, relative mouse position to upper left
    QPoint global_mpos; //For resizing, global mouse position at mouse click
    QPoint rs_mpos; //for resizing
    int storeWidth; //fix window size at mouseclick for resizing
    int storeHeight;
    ///////////////////////////////////////////
protected:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent* event) override;
private slots:
    void AddButton_clicked();
};

#endif // MAINWINDOWWIDGET_H
