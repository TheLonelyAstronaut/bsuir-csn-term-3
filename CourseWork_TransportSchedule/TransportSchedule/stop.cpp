#include "stop.h"
#include "doublylinkedlist.cpp"
#include "scheduletime.h"
#include <QDebug>
#include <QFile>
#include "vehicle.h"

Stop::Stop(QString id, QString name)
{
    this->ID = id;
    this->Name = name;
    //this->to = DoublyLinkedList<ScheduleTime>();
}

Stop::~Stop()
{

}

QString Stop::id()
{
    return this->ID;
}

QString Stop::name()
{
    return this->Name;
}

void Stop::setTimeTable(QList<ScheduleTime> to, QList<ScheduleTime> back)
{
    this->to = to;
    this->back = back;
}

QList<ScheduleTime> Stop::getToTimetable()
{
    return to;
}

QList<ScheduleTime> Stop::getBackTimetable()
{
    return back;
}

void Stop::WriteStopToFile(Stop stop)
{
    QFile stops("textdatabase/stops.txt");

    QList<Stop> stopsList = ReadStopsFromFile();

    if(!stops.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << stops.errorString();
        //return;
    }

    stops.seek(0);
    QTextStream stopsIn(&stops);
    stopsIn << "StopId;StopName" << endl;
    bool inTXT = false;

    for (auto it = stopsList.begin();it != stopsList.end(); ++it) {
        if((*it).name() == stop.name())
        {
            inTXT =true;
        }
        stopsIn << (*it).id() << ';' << (*it).name() << endl;
    }

    if(!inTXT) stopsIn << stop.id() << ';' << stop.name();

    stops.close();
}

QList<Stop> Stop::ReadStopsFromFile()
{
    QFile stops("textdatabase/stops.txt");
    QList<Stop> returnable;

    if(!stops.open(QIODevice::ReadOnly | QIODevice::Text )) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << stops.errorString();
        //return;
    }

    QTextStream stopsIn(&stops);
    stopsIn.readLine();

    while(!stopsIn.atEnd())
    {
        QString line = stopsIn.readLine();
        returnable.push_back(Stop(line.split(';')[0], line.split(';')[1]));
    }

    stops.close();
    return returnable;
}

void Stop::WriteTimetableToFile(Stop stop, Vehicle *vehicle)
{
    QVector<QString> routes;
    QList<Stop> currentStops = vehicle->getRoute().getStops();

    auto test = stop.getToTimetable();

    for(auto it = test.begin(); it != test.end(); ++ it)
    {
        qDebug() << it->getHour() << it->getMinute();
    }

    QFile timetable("textdatabase/timetable.txt");
    if(!timetable.open(QIODevice::ReadOnly | QIODevice::WriteOnly | QIODevice::Text )) {
        qDebug() << timetable.errorString();
        //return;
    }

    timetable.seek(0);
    QTextStream timetableIn(&timetable);
    timetableIn.seek(0);
    bool sended = false;

    while(!timetableIn.atEnd())
    {
        QString line = timetableIn.readLine();
        QStringList splitted = line.split(';');

        if(stop.id() == splitted[0] && splitted[1] == vehicle->GetRouteNumber())
        {
            QString toFile = Route::GenerateStringRoute(stop, splitted[1]);
            routes.push_back(toFile);
            sended = true;
        }else{
            routes.push_back(line);
        }
    }

    if(!sended) routes.push_back(Route::GenerateStringRoute(stop, vehicle->GetRouteNumber()));

    timetable.close();

    timetable.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
    QTextStream rewrite(&timetable);

    for (auto it = routes.begin(); it != routes.end(); ++it) {
        rewrite << (*it) << endl;
    }

    timetable.close();

    qDebug() << vehicle->GetRouteName() << vehicle->GetRouteNumber() << stop.id() << endl;
}

void Stop::RemoveTimetableFromFile(Stop stop, Vehicle *vehicle)
{
    QFile stops("textdatabase/timetable.txt");
    if(!stops.open(QIODevice::ReadOnly | QIODevice::Text )) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << stops.errorString();
        //return;
    }

    QTextStream stopsIn(&stops);
    QVector<QString> file;
    stops.seek(0);

    file.push_back(stopsIn.readLine());

    while (!stopsIn.atEnd()) {
        QString line = stopsIn.readLine();
        QStringList splitted = line.split(';');

        //qDebug() << splitted[0].toInt() << route.getRouteNumber();

        if(splitted[0].toInt() != stop.id().toInt() || splitted[1].toInt() != vehicle->GetRouteNumber().toInt())
        {
            file.push_back(line);
        }
    }

    stops.close();

    if(!stops.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        //QMessageBox::information(0, "error", timetable.errorString());
        qDebug() << stops.errorString();
        //return;
    }

    QTextStream stopsOut(&stops);

    for (auto it = file.begin(); it != file.end(); ++it) {
        stopsOut << (*it) << endl;
    }

    stops.close();
}
