#ifndef TROLLEYBUS_H
#define TROLLEYBUS_H
#include "electricvehicle.h"
#include "enums.h"
#include "doublylinkedlist.h"

class TrolleyBus : public ElectricVehicle
{
    int BatteryCapacity;
public:
    TrolleyBus();
    TrolleyBus(Route route, int _humanCapacity = 0, QString _model = "", int _electricPower = 0, int _batteryCapacity = 0, int _vehicleId = 0);
    ~TrolleyBus() {}
    void setBatteryCapacity(int _batteryCapacity);
    int getBatteryCapacity();
    QString GetRouteNumber();
    QString GetRouteName();
    vehicleType GetEnumType();
    static void WriteTrolleyBusToFile(TrolleyBus* bus);
    static DoublyLinkedList<TrolleyBus*> ReadTrolleyBusFromFile();
    static void RemoveTrolleyBusFromFile(TrolleyBus* bus);
    friend bool operator==(TrolleyBus& lhs, TrolleyBus& rhs);
};

#endif // TROLLEYBUS_H
