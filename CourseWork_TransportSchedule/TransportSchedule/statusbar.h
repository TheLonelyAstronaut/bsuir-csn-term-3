#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <QWidget>
#include <QDebug>
#include "mainwindow.h"

namespace Ui {
class StatusBar;
}

class StatusBar : public QWidget
{
    Q_OBJECT

public:
    explicit StatusBar(QWidget *parent = nullptr, MainWindow *window = nullptr);
    ~StatusBar();

private slots:
    void on_closeApp_clicked();

    void on_minimizeButton_clicked();

    void on_maximizeButton_clicked();
private:
    Ui::StatusBar *ui;
    MainWindow* window;
    int deltaX, deltaY;
    bool wasDoubleclicked = false;
    void maximize();
protected:
    void mousePressEvent(QMouseEvent * event) override;

    void mouseDoubleClickEvent(QMouseEvent * event) override;

    void moveEvent(QMoveEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

    void paintEvent(QPaintEvent *event) override;
};

#endif // STATUSBAR_H
