#ifndef EDITROUTECOMMAND_H
#define EDITROUTECOMMAND_H
#include <QUndoCommand>
#include "vehicle.h"

class EditRouteCommand : public QUndoCommand
{
    Vehicle* vehicle;
    Route route;
    QString model;
    int humanCapacity;
    int firstParameter;
    int secondParameter;
    int currentStop;
public:
    EditRouteCommand(Vehicle *vehicle, Route route, QString model, int humanCapacity, int firstParameter, int secondParameter, int currentStop);
    void undo() override;
    void redo() override;
};

#endif // EDITROUTECOMMAND_H
