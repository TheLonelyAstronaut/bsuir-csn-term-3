#ifndef SEARCHANDSORT_H
#define SEARCHANDSORT_H

#include <QWidget>
#include "mainwindow.h"
#include "vehicle.h"
#include "doublylinkedlist.h"


namespace Ui {
class SearchAndSort;
}

class SearchAndSort : public QWidget
{
    Q_OBJECT

public:
    explicit SearchAndSort(QWidget *parent = nullptr, MainWindow* window = nullptr);
    ~SearchAndSort();

private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_setBus_clicked();

    void on_setTrolleybus_clicked();

    void on_setTram_clicked();

    void on_setAll_clicked();

private:
    DoublyLinkedList<Vehicle*> vehicleSort(QString parameter, DoublyLinkedList<Vehicle*> request);
    DoublyLinkedList<Vehicle*> allVehicleSort(QString parameter);
    Ui::SearchAndSort *ui;
    MainWindow* window;
};

#endif // SEARCHANDSORT_H
