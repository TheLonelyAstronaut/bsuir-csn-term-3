#include "vehicle.h"
#include "route.h"
#include <QDebug>

Vehicle::Vehicle()
{
}

Vehicle::Vehicle(Route _route, int _humanCapacity, QString _model, int _vehicleId) : vehicleRoute(_route),
    humanCapacity(_humanCapacity), model(_model), vehicleId(_vehicleId) {}

void Vehicle::addStopToRoute(Stop stop)
{
    this->vehicleRoute.addStop(stop);
}

Route Vehicle::getRoute()
{
    return vehicleRoute;
}

void Vehicle::setStops(QList<Stop> stops)
{
    this->vehicleRoute.setStops(stops);
}

void Vehicle::setRoute(Route route)
{
    this->vehicleRoute = route;
}

int Vehicle::getHumanCapacity()
{
    return humanCapacity;
}

QString Vehicle::getModel()
{
    return model;
}

void Vehicle::setHumanCapacity(int _capacity)
{
    this->humanCapacity = _capacity;
}

void Vehicle::setModel(QString _model)
{
    this->model = _model;
}

bool operator==(Vehicle& lhs, Vehicle& rhs)
{
    //qDebug() << lhs.humanCapacity << rhs.humanCapacity;
    //qDebug() << lhs.model << rhs.model;
    if(lhs.humanCapacity == rhs.humanCapacity && lhs.model == rhs.model) return true;
    else return false;
}

int Vehicle::getVehicleId()
{
    return vehicleId;
}

void Vehicle::setVehicleId(int id)
{
    vehicleId = id;
}
