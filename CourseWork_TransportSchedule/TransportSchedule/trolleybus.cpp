#include "trolleybus.h"
#include "enums.h"
#include "doublylinkedlist.cpp"
#include "doublylinkedlist.h"
#include "fileexception.h"

TrolleyBus::TrolleyBus()
{

}

TrolleyBus::TrolleyBus(Route _route, int _humanCapacity, QString _model, int _enginePower, int _batteryCapacity, int _vehicleId) : ElectricVehicle(_route, _humanCapacity, _model, _enginePower, _vehicleId)
{
    this->BatteryCapacity=_batteryCapacity;
}

QString TrolleyBus::GetRouteNumber()
{
    return this->vehicleRoute.getRouteNumber();
}

QString TrolleyBus::GetRouteName()
{
    return this->vehicleRoute.getRouteName();
}

vehicleType TrolleyBus::GetEnumType()
{
    return vehicleType(trolleybus);
}

void TrolleyBus::setBatteryCapacity(int _batteryCapacity)
{
    this->BatteryCapacity=_batteryCapacity;
}

int TrolleyBus::getBatteryCapacity()
{
    return BatteryCapacity;
}

void TrolleyBus::WriteTrolleyBusToFile(TrolleyBus *trolleybus)
{
    auto dataInFile = ReadTrolleyBusFromFile();
    DoublyLinkedList<TrolleyBus*> toFile;
    bool inList = false;
    int counter = 1;

    for(auto it = dataInFile.begin(); it !=dataInFile.end() && dataInFile.size(); ++it)
    {
        if((*it)->getVehicleId() == trolleybus->getVehicleId())
        {
            toFile.push_back(trolleybus);
            inList = true;
            continue;
        }
        else if(*(*it) == *trolleybus)
        {
            trolleybus->setVehicleId((*it)->getVehicleId());
            toFile.push_back(trolleybus);
            inList = true;
        }
        else
        {
            toFile.push_back((*it));
        }
        counter++;
    }

    if(!inList)
    {
        trolleybus->setVehicleId(counter);
        toFile.push_back(trolleybus);
    }

    QFile file("binarydatabase/trolleybus.bin");
    if(!file.open(QIODevice::WriteOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/trolleybus.bin");
    QDataStream stream(&file);

    for (auto it = toFile.begin(); it != toFile.end(); ++it) {

        int temp =  (*it)->model.length(),
                humanCapacity = (*it)->humanCapacity,
                electricPower = (*it)->electricPower,
                batteryCapacity = (*it)->BatteryCapacity,
                id = (*it)->getVehicleId();
        stream.writeRawData(reinterpret_cast<char*>(&id), sizeof(int));
        stream.writeRawData(reinterpret_cast<char*>(&humanCapacity), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&temp), sizeof(int));
        stream.writeRawData((*it)->model.toStdString().c_str(), (*it)->model.length());
        stream.writeRawData(reinterpret_cast<char*>(&electricPower), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&batteryCapacity), sizeof (int));
    }

    file.close();
}

void TrolleyBus::RemoveTrolleyBusFromFile(TrolleyBus *bus)
{
    auto dataInFile = ReadTrolleyBusFromFile();
    DoublyLinkedList<TrolleyBus*> toFile;

    for(auto it = dataInFile.begin(); it !=dataInFile.end() && dataInFile.size(); ++it)
    {
        if(*(*it) == *bus || (*it)->getVehicleId() == bus->getVehicleId())
        {

        }
        else
        {
            toFile.push_back((*it));
        }
    }

    //if(!inList) toFile.push_back(bus);

    QFile file("binarydatabase/trolleybus.bin");
    if(!file.open(QIODevice::WriteOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/trolleybus.bin");
    QDataStream stream(&file);

    for (auto it = toFile.begin(); it != toFile.end(); ++it) {

        int temp =  (*it)->model.length(),
                humanCapacity = (*it)->humanCapacity,
                electricPower = (*it)->electricPower,
                batteryCapacity = (*it)->BatteryCapacity,
                id = (*it)->getVehicleId();
        stream.writeRawData(reinterpret_cast<char*>(&id), sizeof(int));
        stream.writeRawData(reinterpret_cast<char*>(&humanCapacity), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&temp), sizeof(int));
        stream.writeRawData((*it)->model.toStdString().c_str(), (*it)->model.length());
        stream.writeRawData(reinterpret_cast<char*>(&electricPower), sizeof (int));
        stream.writeRawData(reinterpret_cast<char*>(&batteryCapacity), sizeof (int));
    }

    file.close();
}

DoublyLinkedList<TrolleyBus*> TrolleyBus::ReadTrolleyBusFromFile()
{
    QFile file("binarydatabase/trolleybus.bin");
    if(!file.open(QIODevice::ReadOnly)) throw FileException("Невозможно открыть файл", "binarydatabase/trolleybus.bin");
    QDataStream stream(&file);
    DoublyLinkedList<TrolleyBus*> returnable;

    //qDebug() << stream.atEnd();

    while(!stream.atEnd())
    {
        TrolleyBus *bus = new TrolleyBus;
        stream.readRawData(reinterpret_cast<char*>(&(bus->vehicleId)), sizeof(int));
        stream.readRawData(reinterpret_cast<char*>(&(bus->humanCapacity)), sizeof (int));
        int stringSize;
        stream.readRawData(reinterpret_cast<char*>(&stringSize), sizeof(int));
        char string[stringSize+1];
        string[stringSize] = '\0';
        stream.readRawData(string, stringSize);
        stream.readRawData(reinterpret_cast<char*>(&(bus->electricPower)), sizeof(int));
        stream.readRawData(reinterpret_cast<char*>(&(bus->BatteryCapacity)), sizeof(int));
        bus->setModel(string);

        returnable.push_back(bus);
    }

    file.close();
    return returnable;
}

bool operator==(TrolleyBus& lhs, TrolleyBus& rhs)
{
    if(static_cast<ElectricVehicle&>(lhs) == static_cast<ElectricVehicle&>(rhs) && lhs.BatteryCapacity == rhs.BatteryCapacity) return true;
    else return false;
}
