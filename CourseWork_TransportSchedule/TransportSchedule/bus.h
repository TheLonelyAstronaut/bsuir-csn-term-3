#ifndef BUS_H
#define BUS_H
#include "oilvehicle.h"
#include "enums.h"
#include "doublylinkedlist.h"

class Bus : public OilVehicle
{
    int sectionCount;
public:
    Bus();
    Bus(Route route, int _humanCapacity = 0, QString _model = "", int _engineVolume = 0, int _sectionCount = 0, int _vehicleId = 0);
    ~Bus() {}
    void setSectionCount(int _sectionCount);
    int getSectionCount();
    static void WriteBusToFile(Bus* bus);
    static DoublyLinkedList<Bus*> ReadBusFromFile();
    static void RemoveBusFromFile(Bus* bus);
    friend bool operator==(Bus& lhs, Bus& rhs);
    QString GetRouteNumber();
    QString GetRouteName();
    vehicleType GetEnumType();
};

#endif // BUS_H
