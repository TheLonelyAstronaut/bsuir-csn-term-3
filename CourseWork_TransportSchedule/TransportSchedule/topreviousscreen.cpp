#include "topreviousscreen.h"
#include "ui_topreviousscreen.h"
#include "mainwindow.h"

ToPreviousScreen::ToPreviousScreen(QWidget *parent, MainWindow* window, QString routeName) :
    QWidget(parent),
    ui(new Ui::ToPreviousScreen),
    window(window)
{
    ui->setupUi(this);
    ui->routeName->setText(routeName);
    ui->backButton->setCursor(QCursor(Qt::PointingHandCursor));
}

ToPreviousScreen::~ToPreviousScreen()
{
    delete ui;
}

void ToPreviousScreen::on_backButton_clicked()
{
    this->window->NavigateToMainScreen(this->window);
}
