
#include "transportspecs.h"
#include "ui_transportspecs.h"

transportSpecs::transportSpecs(QWidget *parent, vehicleType type) :
    QWidget(parent),
    ui(new Ui::transportSpecs),
    type(type)
{
    ui->setupUi(this);
    ui->ModelLine->hide();
    ui->HumanCapacityLine->hide();
    ui->Parameter1Line->hide();
    ui->Parameter2Line->hide();

    switch (type) {
    case vehicleType::bus:{
        ui->Parameter1Label->setText("Обьем дв.");
        ui->Parameter2Label->setText("Кол-во секций");
        break;
    }case vehicleType::tram:{
        ui->Parameter1Label->setText("Питание");
        ui->Parameter2Label->setText("Низкопольный");
        break;
    }case vehicleType::trolleybus:{
        ui->Parameter1Label->setText("Питание");
        ui->Parameter2Label->setText("Батарея");
        break;
    }
    default:{
        break;
    }
    }
}

transportSpecs::~transportSpecs()
{
    delete ui;
}

void transportSpecs::init()
{
    switch (type) {
    case vehicleType::bus:{
        ui->Parameter1Label->setText("Обьем дв.");
        ui->Parameter2Label->setText("Кол-во секций");
        break;
    }case vehicleType::tram:{
        ui->Parameter1Label->setText("Питание");
        ui->Parameter2Label->setText("Низкопольный");
        break;
    }case vehicleType::trolleybus:{
        ui->Parameter1Label->setText("Питание");
        ui->Parameter2Label->setText("Батарея");
        break;
    }
    default:{
        break;
    }
    }

    if(edit)
    {
        ui->CurrentModelLabel->hide();
        ui->ModelLine->setText(ui->CurrentModelLabel->text());
        ui->ModelLine->show();
        ui->CurrentHumanCapaity->hide();
        ui->HumanCapacityLine->setText(ui->CurrentHumanCapaity->text());
        ui->HumanCapacityLine->show();
        ui->CurrentParameter1Label->hide();
        ui->Parameter1Line->setText(ui->CurrentParameter1Label->text());
        ui->Parameter1Line->show();
        ui->CurrentParameter2Label->hide();
        ui->Parameter2Line->setText(ui->CurrentParameter2Label->text());
        ui->Parameter2Line->show();
    }else{
        ui->ModelLine->hide();
        ui->CurrentModelLabel->setText(ui->ModelLine->text());
        ui->CurrentModelLabel->show();
        ui->HumanCapacityLine->hide();
        ui->CurrentHumanCapaity->setText(ui->HumanCapacityLine->text());
        ui->CurrentHumanCapaity->show();
        ui->Parameter1Line->hide();
        ui->CurrentParameter1Label->setText(ui->Parameter1Line->text());
        ui->CurrentParameter1Label->show();
        ui->Parameter2Line->hide();
        ui->CurrentParameter2Label->setText(ui->Parameter2Line->text());
        ui->CurrentParameter2Label->show();
    }
}

void transportSpecs::setType(vehicleType type){
    this->type = type;
}

void transportSpecs::editMode()
{
    this->edit = true;
    init();
}

QVector<QString> transportSpecs::browseMode()
{
    this->edit = false;
    init();
    QVector<QString> returnable;
    returnable.push_back(ui->CurrentModelLabel->text());
    returnable.push_back(ui->CurrentHumanCapaity->text());
    returnable.push_back(ui->CurrentParameter1Label->text());
    returnable.push_back(ui->CurrentParameter2Label->text());
    return returnable;
}

void transportSpecs::setSpecs(QString param1, QString param2, QString param3, QString param4)
{
    ui->CurrentModelLabel->setText(param1);
    ui->CurrentHumanCapaity->setText(param2);
    ui->CurrentParameter1Label->setText(param3);
    ui->CurrentParameter2Label->setText(param4);
}
