#include <iostream>
#include "String.h"

using namespace std;

int main()
{
	system("color 3");

	cout << "Creating strings by using char arrays..." << endl << endl;

	char* str = new char[500];
	for (int i = 0; i < 500; ++i)
	{
		str[i] = 'a';
	}

	char* str1 = new char[500];
	for (int i = 0; i < 500; ++i)
	{
		str1[i] = 'b';
	}

	String first_string = String(str, 500);
	String second_string = String(str1, 500);
	
	cout << "First string (overloaded <<):" << endl;
	cout << first_string << endl << endl << endl;

	cout << "Second string (overloaded <<):" << endl;
	cout << second_string << endl << endl << endl;;

	cout << "Enter first string (overloaded >>):" << endl;
	cin >> first_string;

	cout << endl << "Enter second string (overloaded >>):" << endl;
	cin >> second_string;
	
	cout << endl  << "First string (overloaded <<):" << endl;
	cout << first_string << " " << first_string.Length() << endl << endl;

	cout << "Second string (overloaded <<):" << endl;
	cout << second_string << " " << second_string.Length() << endl << endl;

	cout << "First string + Second string (overloaded +):" << endl;
	cout << first_string + second_string << endl << endl;

	cout << "First string += Second string (overloaded +=):" << endl;
	cout << (first_string += second_string) << endl << endl;

	cout << "First string (using overloaded []):" << endl;
	for (unsigned i = 0; i < first_string.Length(); ++i)
	{
		cout << first_string[i];
	}
	cout << endl << endl;

	cout << "Second string (using overloaded []):" << endl;
	for (unsigned i = 0; i < second_string.Length(); ++i)
	{
		cout << second_string[i];
	}
	cout << endl << endl;

	cout << "++First string (overloaded ++ and overloaded <<):" << endl;
	cout << ++first_string << endl << endl;

	cout << "First string++ (overloaded ++ and overloaded <<):" << endl;
	cout << first_string++ << endl << endl;

	cout << "First string-- (overloaded -- and overloaded <<):" << endl;
	cout << first_string-- << endl << endl;

	cout << "--First string (overloaded -- and overloaded <<):" << endl;
	cout << --first_string << endl << endl;

	cout << "First string - Second string (overloaded - and overloaded <<):" << endl;
	cout << first_string - second_string << endl << endl;

	cout << "First string -= Second string (overloaded -= and ovaerloaded <<):" << endl;
	cout << (first_string -= second_string) << endl << endl;

	cout << "First string == Second string (overloaded == and ovaerloaded <<):" << endl;
 	cout << (first_string == second_string) << endl << endl;

	cout << "First string > Second string (overloaded > and ovaerloaded <<):" << endl;
	cout << (first_string > second_string) << endl << endl;

	cout << "First string < Second string (overloaded < and ovaerloaded <<):" << endl;
	cout << (first_string < second_string) << endl << endl;

	unsigned left, right;
	cout << "Enter left substring border: ";
	cin >> left;
	cout << "Enter right substring border: ";
	cin >> right;
	cout << endl;

	cout << "Substring (from the first string) (overloaded [], overloaded () and overloaded <<):" << endl;
	cout << first_string(left, right) << endl << endl;

	cout << "First string = Second string and First string == Second string (overloaded ==, overloaded = and overloaded <<):" << endl;
	first_string = second_string;
	cout << (first_string == second_string) << endl << endl;

	cout << "First string (overloaded <<):" << endl;
	cout << first_string << endl << endl << endl;

	cout << "Second string (overloaded <<):" << endl;
	cout << second_string << endl << endl;

	cout << "First string && Second string (overloaded && and overloaded <<):" << endl;
	cout << (first_string && second_string) << endl << endl;

	cout << "First string || Second string (overloaded || and overloaded <<):" << endl;
	cout << (first_string || second_string) << endl << endl;

	cout << "!First string (overloaded ! and overloaded <<):" << endl;
	cout << !first_string << endl << endl;

	String name = "Vadim", surname = "Filippovitch";
	String table = "FKSiS " + name + " " + surname + " " + name[0] += " " + surname;
	cout << (table -= "FKSiS") << endl;
}