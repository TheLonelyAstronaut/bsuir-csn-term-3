#include "String.h"
#include <iostream>
#include <string.h>
#include <cstring>
#define STRING_DEFAULT_SIZE 256

String::String()
{
	length = 0;
	in_memory = 0;
	this->c_str = nullptr;
}

String::String(const char symbol)
{
	this->length = 1;
	in_memory = STRING_DEFAULT_SIZE;
	this->c_str = new char[STRING_DEFAULT_SIZE];
	memset(this->c_str, 0, STRING_DEFAULT_SIZE);
	this->c_str[0] = symbol;
}

String::String(const char string[])
{
	unsigned i = 1;

	for (i; strnlen_s(string, i * STRING_DEFAULT_SIZE) == i * STRING_DEFAULT_SIZE; ++i);

	this->length = strnlen_s(string, i*STRING_DEFAULT_SIZE);
	in_memory = i * STRING_DEFAULT_SIZE;
	this->c_str = new char[i*STRING_DEFAULT_SIZE];
	memset(this->c_str, 0, i*STRING_DEFAULT_SIZE);
	memcpy(this->c_str, string, this->length);
}

String::String(char* string, unsigned length)
{
	unsigned i = 1;

	if(length >= STRING_DEFAULT_SIZE)
		for (i; length >= i * STRING_DEFAULT_SIZE; ++i);

	this->length = length;
	in_memory = i * STRING_DEFAULT_SIZE;
	this->c_str = new char[i * STRING_DEFAULT_SIZE];
	memset(this->c_str, 0, i * STRING_DEFAULT_SIZE);
	memcpy(this->c_str, string, length);
}

String::String(const String& string)
{
	this->c_str = new char[string.in_memory];
	this->length = string.length;
	this->in_memory = string.in_memory;
	memcpy(this->c_str, string.c_str, string.in_memory);
}

String::~String()
{
	delete[] this->c_str;
}

unsigned String::Length()
{
	return this->length;
}

String& String::operator+=(const String& string)
{
	if (string.length + this->length < this->in_memory)
	{
		for (int i = 0; i < string.length; ++i)
		{
			this->c_str[this->length + i] = string[i];
		}

		this->length += string.length;

		return *this;
	}
	else
	{
		(*this) = (*this) + string;
		return *this;
	}
}

String operator+(const String& first_string, const String& second_string)
{
	String new_string = String();
	new_string.length = first_string.length + second_string.length;

	unsigned i = 1;
	for (i; first_string.length + second_string.length > i * STRING_DEFAULT_SIZE; ++i);

	new_string.in_memory = i * STRING_DEFAULT_SIZE;
	new_string.c_str = new char[i * STRING_DEFAULT_SIZE];
	memset(new_string.c_str, 0, i * STRING_DEFAULT_SIZE);
	memcpy(new_string.c_str, first_string.c_str, first_string.length);

	for (unsigned j = 0; j < second_string.length; ++j) {
		try {
			new_string[j + first_string.length] = second_string[j];
		}
		catch (int)
		{
			std::cout << j << std::endl;
		}
	}

	return new_string;
}
String& String::operator-=(const String& string)
{
	if (!string.length) return *this;
	int temp_len = this->length;

	do
	{
		char* substring = strstr(this->c_str, string.c_str);
		//std::cout << (int)substring[0] << std::endl;
		if (substring != NULL)
		{
			char* next = substring + string.length;
			//std::cout << this->c_str << std::endl;
			this->length -= string.length;
			
			strcpy_s(substring, strnlen_s(next, this->length) + 1, next);
		}
		else break;

	} while (true);

	
	for (int i = this->length; i < temp_len; ++i)
	{
		this->c_str[i] = 0;
	}

	return *this;
}

char& String::operator[](unsigned index) const
{
	if (index >= this->length) throw std::invalid_argument("Bad index!");
	else return this->c_str[index];
}

String& String::operator=(const String& string)
{
	if (this == &string) return *this;

	this->~String();
	this->length = string.length;
	this->in_memory = string.in_memory;

	this->c_str = new char[this->in_memory];
	memcpy(this->c_str, string.c_str, this->in_memory);
}

String& String::operator=(const char string[])
{
	unsigned i = 1;

	for (i; strnlen_s(string, i * STRING_DEFAULT_SIZE) == i * STRING_DEFAULT_SIZE; ++i);

	this->~String();
	this->length = strnlen_s(string, i * STRING_DEFAULT_SIZE);
	in_memory = i * STRING_DEFAULT_SIZE;
	this->c_str = new char[i * STRING_DEFAULT_SIZE];
	memset(this->c_str, 0, i * STRING_DEFAULT_SIZE);
	memcpy(this->c_str, string, this->length);

	return *this;
}

String& String::operator++()
{
	for (unsigned i = 0; i < this->length; ++i) 
	{
		this->c_str[i]++;
	}

	return *this;
}

String String::operator++(int)
{
	String prev = *this;
	++(*this);
	return prev;
}

String& String::operator--()
{
	for (unsigned i = 0; i < this->length; ++i)
	{
		this->c_str[i]--;
	}

	return *this;
}

String String::operator--(int)
{
	String prev = *this;
	--(*this);
	return prev;
}

String String::operator()(unsigned left, unsigned right)
{
	if (left >= right) throw std::invalid_argument("Bad size!");
	if (right >= this->length) throw std::invalid_argument("Bad index!");
	
	String new_string = String();
	new_string.length = right - left + 1;

	unsigned i = 1;
	for (i; new_string.length > i * STRING_DEFAULT_SIZE; ++i);
	new_string.in_memory = i * STRING_DEFAULT_SIZE;
	new_string.c_str = new char[new_string.in_memory];
	memset(new_string.c_str, 0, new_string.in_memory);

	for (unsigned i = left, j = 0; i <= right; ++i, ++j)
		new_string[j] = this->c_str[i];

	return new_string;
}

bool String::operator<(const String& right_string)
{
	if (strcmp(this->c_str, right_string.c_str) > 0) return false;
	else return true;
}

bool String::operator>(const String& right_string)
{
	if (strcmp(this->c_str, right_string.c_str) < 0) return false;
	else return true;
}

bool String::operator==(const String& right_string)
{
	if (strcmp(this->c_str, right_string.c_str)) return false;
	else return true;
}

std::ostream& operator<<(std::ostream& os, const String& string)
{
	os << (string.c_str == nullptr ? "" : string.c_str);
	return os;
}

std::istream& operator>>(std::istream& is, String& string)
{
	char* temp = new char[STRING_DEFAULT_SIZE];
	memset(temp, 0, STRING_DEFAULT_SIZE);
	char c;
	int count = 0;
	unsigned i = 1;

	while (is.get(c)) //C-style string formatting (unlimited input symbol-by-symbol)
	{
		if (c == '\n') break;
		
		if (count == STRING_DEFAULT_SIZE*i) //Reallocating
		{
			char* reallocating = new char[STRING_DEFAULT_SIZE * i];
			memcpy(reallocating, temp, STRING_DEFAULT_SIZE * i);
			delete[] temp;
			temp = new char[STRING_DEFAULT_SIZE * ++i];
			memset(temp, 0, STRING_DEFAULT_SIZE * i);
			memcpy(temp, reallocating, STRING_DEFAULT_SIZE * (i - 1));
			delete[] reallocating;
		}
		
		temp[count++] = c;
	}

	if (string.c_str != nullptr) string.~String();

	string.length = strnlen_s(temp, i * STRING_DEFAULT_SIZE);
	string.in_memory = i * STRING_DEFAULT_SIZE;
	string.c_str = new char[string.in_memory];
	memset(string.c_str, 0, string.in_memory);
	memcpy(string.c_str, temp, string.length);
	delete[] temp;
	
	return is;
}



String operator-(const String& first_string, const String& second_string)
{
	String new_string = String();

	if (!second_string.length) return first_string;
	if (first_string.length < second_string.length) return first_string;
	
	char* temp = new char[first_string.length+1];
	int count = 0;
	memset(temp, 0, first_string.length+1);
	memcpy(temp, first_string.c_str, first_string.length);

	do 
	{
		char* substring = strstr(temp, second_string.c_str);
		if (substring != NULL)
		{
			char* next = substring + second_string.length;
			++count;
			strcpy_s(substring, strnlen_s(next, first_string.length) + 1, next);
		}
		else break;
	} while (true);

	new_string.length = first_string.length - second_string.length * count;
	
	for (count = 1; new_string.length > STRING_DEFAULT_SIZE * count; ++count);

	new_string.in_memory = STRING_DEFAULT_SIZE * count;
	new_string.c_str = new char[new_string.in_memory];
	memset(new_string.c_str, 0, new_string.in_memory);
	memcpy(new_string.c_str, temp, new_string.length);
	delete[] temp;

	return new_string;
}

bool String::operator&&(const String& second_string)
{
	if (this->c_str != NULL && second_string.c_str != NULL) return true;
	return false;
}

bool String::operator||(const String& second_string)
{
	if (this->c_str != NULL || second_string.c_str != NULL) return true;
	return false;
}

bool String::operator!()
{
	if (this->c_str == NULL) return true;
	return false;
}

int String::IndexOf(const String& string)
{
	char* index = strstr(this->c_str, string.c_str);
	if (index != NULL)  return (int)(index - this->c_str);
	else return -1;
}

char* String::GetCStr()
{
	char* temp = new char[this->length + 1];
	memcpy(temp, this->c_str, this->length);
	temp[this->length] = '\0';
	return temp;
}