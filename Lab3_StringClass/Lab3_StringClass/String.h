#pragma once
#include <iostream>

class String
{
	char* c_str;
	unsigned length;
	unsigned in_memory;
public:
	String();
	String(const char symbol);
	String(const char string[]);
	String(char* string, unsigned length);
	String(const String& string);
	~String();

	unsigned Length();
	int IndexOf(const String& string);
	char* GetCStr();

	String& operator+=(const String& string);
	String& operator-=(const String& string);
	char& operator[](const unsigned index) const;
	String& operator=(const String& string);
	String& operator=(const char string[]);
	String& operator++();
	String operator++(int);
	String& operator--();
	String operator--(int);
	String operator()(unsigned left, unsigned right);
	bool operator<(const String& right_string);
	bool operator>(const String& right_string);
	bool operator==(const String& right_string);
	friend std::ostream& operator<<(std::ostream& os, const String& string);
	friend std::istream& operator>>(std::istream& is, String& string);
	friend String operator+(const String& first_string, const String& second_string);
	friend String operator-(const String& first_string, const String& second_string);
	bool operator&&(const String& second_string);
	bool operator||(const String& second_string);
	bool operator!();
};