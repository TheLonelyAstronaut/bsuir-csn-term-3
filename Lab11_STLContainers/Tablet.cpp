#include "Tablet.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include "InputSizeException.h"
using namespace std;

bool Tablet::GetWithCellurarModule()
{
	return this->WithCellurarModule;
}

void Tablet::SetWithCellurarModule(bool _withcellurarmodule)
{
	this->WithCellurarModule = _withcellurarmodule;
}

istream& operator>>(istream& in, Tablet& object)
{
	string exOnly;

	in >> static_cast<PortableMachine&>(object);
	cout << "With Cellurar Module (yes/no): ";
	Exception::isString(exOnly, 3);
	if (exOnly == "yes" || exOnly == "Yes") object.WithCellurarModule = true;
	else object.WithCellurarModule = false;

	return in;
}

ostream& operator<<(ostream& out, const Tablet& object)
{
	out << static_cast<const PortableMachine&>(object);
	out << setw(15) << (object.WithCellurarModule ? "Yes" : "No") << endl;
	return out;
}

void Tablet::printHeader()
{
	PortableMachine::printHeader();
	cout << setw(15) << "With Cellurar";
}

fstream& operator<<(fstream& out, const Tablet& object)
{
	out << static_cast<const PortableMachine&>(object);
	out << " With Sellurar Module:\"" << (object.WithCellurarModule ? "Yes" : "No") << "\"; }" << endl;
	return out;
}

fstream& operator>>(fstream& in, Tablet& object)
{
	string textedObject;
	getline(in, textedObject);

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetManufacturer(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetCPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetGPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetRAM(stoi(textedObject.substr(0, textedObject.find("\""))));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetBatteryCapacity(stoi(textedObject.substr(0, textedObject.find("\""))));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetWithCellurarModule(textedObject.substr(0, textedObject.find("\"")) == "Yes" ? true : false);
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	return in;
}