#include "Algorithm.h"
#include <iterator> 
#include "ListIterator.h"
#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;

template <class T>
void show(ListIterator<T> tss)
{
	try {
		while (true)
		{
			cout << (*tss);
			tss++;
		}
	}
	catch (...) {}

	cout << endl;
}

template <class T>
static bool Algorithm::isSorted(ListIterator<T> begin, ListIterator<T> end, bool (*comparator)(T, T), bool ascending) //Checks if sorted
{
	for (; begin != end; ++begin)
	{
		ListIterator<T> temp = ++begin;
		--begin;
		if ((*comparator)((*begin), (*temp)) == ascending)
			return false;
	}

	return true;
}

template <class T>
static void Algorithm::iteratorSwap(ListIterator<T>& lhs, ListIterator<T>& rhs) //Swaps iterators by value
{
	T value = (*rhs);
	(*rhs) = (*lhs);
	(*lhs) = value;
}


template <class T>
static void Algorithm::sort(ListIterator<T> begin, ListIterator<T> end, bool (*comparator)(T, T), bool ascending) //Quicksort realisation //Add overload with comparators
{
	int size = end - begin, i = 0, j = 0;
	
	//cout << size << endl;

	if (!size) return;

	for (ListIterator<T> frst = begin; i < size - 1; i++, ++frst, j=0) {
		for (ListIterator<T> scnd = begin; j < size - i - 1; j++, ++scnd) {
			ListIterator<T> temp = ++scnd;
			--scnd;
			if((*comparator)((*scnd), (*temp)) == ascending)
				Algorithm::iteratorSwap(scnd, temp);
		}
	}
}

template<class T, class K>
static DoublyLinkedList<T> Algorithm::search(ListIterator<T> begin, ListIterator<T> end, K value, bool (*comparator)(T, K))
{
	DoublyLinkedList<T> elements = DoublyLinkedList<T>();

	for (; begin != end; ++begin)
		if ((*comparator)(*begin, value))
			elements.push_back((*begin));

	return elements;
}

template<class T, class K>																
static DoublyLinkedList<T> Algorithm::search(ListIterator<T> begin, ListIterator<T> end, multimap<string, K> values, multimap<string, bool (*)(T, K)> comparators)
{
	DoublyLinkedList<T> elements = DoublyLinkedList<T>();

	if (values.empty() || comparators.empty()) return elements;

	for (; begin != end; ++begin)
	{
		auto valuesIt = values.begin();
		auto comparatorsIt = comparators.begin();
		bool checked = true;

		for (; valuesIt != values.end(); ++valuesIt, ++comparatorsIt)
		{
			if (valuesIt->first == comparatorsIt->first)
			{
				if (!(*comparatorsIt->second)((*begin), valuesIt->second))
				{
					checked = false;
					break;
				}
			}
		}

		if (checked) elements.push_back((*begin));
	}

	return elements;
}

