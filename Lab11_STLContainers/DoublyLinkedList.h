#pragma once
#include <iostream>
#include "Node.h"
#include "ConstListIterator.h"
#include "ListIterator.h"
#include "ConstListIterator.cpp"
#include "ListIterator.cpp"

template <class T>
class DoublyLinkedList
{
public:
    // constructor, desctructor, copy constructor
    DoublyLinkedList(); // default zero parameter constructor
    DoublyLinkedList(const DoublyLinkedList &rhs); // copy constructor
    DoublyLinkedList(DoublyLinkedList && rhs); // move constructor
    // num elements with value of val
    explicit DoublyLinkedList(int num, const T& val = T{});
    // constructs with elements [start, end)
    DoublyLinkedList(ConstListIterator<T> start, ConstListIterator<T> end);

    template <class K>
    DoublyLinkedList(const DoublyLinkedList<K>& p);

    ~DoublyLinkedList(); // destructor

    // copy assignment operator
    const DoublyLinkedList& operator=(const DoublyLinkedList &rhs);
    // move assignment operator
    DoublyLinkedList & operator=(DoublyLinkedList && rhs);

    // member functions
    int size() const; // number of elements
    bool empty() const; // check if DoublyLinkedList is empty
    void clear(); // delete all elements
    void reverse(); // reverse the order of the elements

    T &front(); // reference to the first element
    const T& front() const;
    T &back(); // reference to the last element
    const T & back() const;

    void push_front(const T & val); // insert to the beginning
    void push_front(T && val); // move version of insert
    void push_back(const T & val); // insert to the end
    void push_back(T && val); // move version of insert
    void pop_front(); // delete first element
    void pop_back(); // delete last element
    void remove(const T &val); // remove all elements with value = val
	void fill(ListIterator<T> begin, ListIterator<T> end);

    // print out all elements. ofc is deliminitor
    void print(std::ostream& os, char ofc = ' ') const;

	//sort elements
	void sort();
	//void sort();

    ListIterator<T> begin(); // iterator to first element
	ConstListIterator<T> begin() const;
	ListIterator<T> end(); // end marker iterator
	ConstListIterator<T> end() const;
	ListIterator<T> insert(ListIterator<T> itr, const T& val); // insert val ahead of itr
	ListIterator<T> insert(ListIterator<T> itr, T && val); // move version of insert
	ListIterator<T> erase(ListIterator<T> itr); // erase one element
	ListIterator<T> erase(ListIterator<T> start, ListIterator<T> end); // erase [start, end)

	friend class ListIterator<T>;
	friend class ConstListIterator<T>;
private:
    int theSize; // number of elements
    Node<T> *head; // head Node<T>
    Node<T> *tail; // tail Node<T>

    void init(); // initialization	
	void qsort(Node<T>*, Node<T>*);
	Node<T>* partition(Node<T>*, Node<T>*);
};