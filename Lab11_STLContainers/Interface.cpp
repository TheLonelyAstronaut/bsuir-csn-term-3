#include "Interface.h"
#include "PC.h"
#include "Exception.h"
#include <iostream>
#include <map>
#include <type_traits>
#include "Algorithm.h"
#include "Algorithm.cpp"
#include <vector>
#include "BinaryFileClass.h"
#include "TextFileClass.h"
#include "BinaryFileClass.cpp"
#include "TextFileClass.cpp"
#include "Comparators.h"
#include "Comparators.cpp"
using namespace std;

template<class T>
Interface<T>::Interface()
{

}

template<class T>
Interface<T>::~Interface()
{
	object.clear();
}

template<class T>
void Interface<T>::Menu()
{
	cout << "Choose action: " << endl
		<< "1. Add element to the front" << endl
		<< "2. Add element to the back" << endl
		<< "3. Pop element from the front" << endl
		<< "4. Pop element from the back" << endl
		<< "5. Clear list" << endl
		<< "6. Sort" << endl
		<< "7. Is sorted?" << endl
		<< "8. Remove element" << endl
		<< "9. Search element" << endl
		<< "10. Search element (multimap)" << endl
		<< "11. Print to console" << endl
		<< "12. Show menu" << endl
		<< "13. Working with binary file" << endl
		<< "14. Working with text file" << endl
		<< "0. Exit" << endl;
}

template<class T>
void Interface<T>::Show()
{
	int answer;
	Menu();

	do {
		cout << "Your choose: ";
		Exception::isNumber(answer, 14, 0);

		switch (answer)
		{
		case 0:
			break;
		case 1:
		{
			T toList = T();
			cin >> toList;
			object.push_front(toList);
			cout << "Success!" << endl;
			break;
		}
		case 2:
		{
			T toList = T();
			cin >> toList;
			object.push_back(toList);
			cout << "Success!" << endl;
			break;
		}
		case 3:
		{
			object.pop_front();
			cout << "Success!" << endl;
			break;
		}
		case 4:
		{
			object.pop_back();
			cout << "Success!" << endl;
			break;
		}
		case 5:
		{	
			object.clear();
			cout << "Success!" << endl;
			break;
		}
		case 6:
		{
			Sorting();
			break;
		}
		case 7:
		{
			isSorted();
			break;
		}
		case 8:
		{
			T removable;
			cin >> removable;
			object.remove(removable);
			cout << "Success!" << endl;
			break;
		}
		case 9:
		{
			Search();
			//int index = object.search(searchable);
			//cout << (index == -1 ? "Item not found!" : "Index: " + to_string(index)) << endl;
			break;
		}
		case 10:
		{
			MultimapSearch();
			break;
		}
		case 11:
		{
			if (!object.size()) cout << "Empty list!" << endl;
			else Print(object);
			break;
		}
		case 12:
		{
			Menu();
			break;
		}
		case 13:
		{
			BinaryFile();
			break;
		}
		case 14:
		{
			TextFile();
			break;
		}
		default:
			break;
		}
	} while (answer);
}

template <class T>
void Interface<T>::Sorting()
{
	int answer, type;
	bool (*comparator)(T, T) = nullptr;

	cout << "Choose parameter: " << endl
		<< "1. Manufacturer" << endl
		<< "2. CPU" << endl
		<< "3. GPU" << endl
		<< "4. RAM" << endl
		<< "0. Exit" << endl;

	Exception::isNumber(answer, 4, 0);

	cout << "Sorting type:" << endl << "0. Descending" << endl << "1. Ascending" << endl;
	

	Exception::isNumber(type, 1, 0);

	switch (answer)
	{
	case 0:
	{
		break;
	}
	case 1:
	{
		comparator = &ManufacturerComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	case 2:
	{
		comparator = &CPUComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	case 3:
	{
		comparator = &GPUComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	case 4:
	{
		comparator = &RAMComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	default:
		break;
	}
}

template <class T>
void Interface<T>::isSorted()
{
	if (!object.size()) {
		cout << "Empty list!" << endl;
		return;
	}

	int answer, type;
	bool (*comparator)(T, T) = nullptr;

	cout << "Choose parameter: " << endl
		<< "1. Manufacturer" << endl
		<< "2. CPU" << endl
		<< "3. GPU" << endl
		<< "4. RAM" << endl
		<< "0. Exit" << endl;

	Exception::isNumber(answer, 4, 0);

	cout << "Sorting type:" << endl << "0. Descending" << endl << "1. Ascending" << endl;


	Exception::isNumber(type, 1, 0);

	switch (answer)
	{
	case 0:
	{
		break;
	}
	case 1:
	{
		comparator = &ManufacturerComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	case 2:
	{
		comparator = &CPUComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	case 3:
	{
		comparator = &GPUComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	case 4:
	{
		comparator = &RAMComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	default:
		break;
	}
}

template<class T>
void Interface<T>::Search()
{
	if (!object.size()) {
		cout << "Empty list!" << endl;
		return;
	}

	int answer, type;
	DoublyLinkedList<T> temp = DoublyLinkedList<T>(object.begin(), object.end());

	do {
		cout << "Choose parameter: " << endl
			<< "1. Manufacturer" << endl
			<< "2. CPU" << endl
			<< "3. GPU" << endl
			<< "4. RAM" << endl
			<< "5. Print" << endl
			<< "0. Exit" << endl;

		Exception::isNumber(answer, 5, 0);

		switch (answer)
		{
		case 0:
		{
			break;
		}
		case 1:
		{
			bool (*comparator)(T, string) = nullptr;
			comparator = &ManufacturerSearch;
			string manufacturer;
			Exception::isString(manufacturer);
			auto bruh = Algorithm::search(temp.begin(), temp.end(), manufacturer, comparator);
			//Print(bruh);
			temp.clear();
			if (bruh.size()) temp.fill(bruh.begin(), bruh.end());
			break;
		}
		case 2:
		{
			bool (*comparator)(T, string) = nullptr;
			comparator = &CPUSearch;
			string CPU;
			Exception::isString(CPU);
			auto bruh = Algorithm::search(temp.begin(), temp.end(), CPU, comparator);
			temp.clear();
			if (bruh.size()) temp.fill(bruh.begin(), bruh.end());
			break;
		}
		case 3:
		{
			bool (*comparator)(T, string) = nullptr;
			comparator = &GPUSearch;
			string GPU;
			Exception::isString(GPU);
			auto bruh = Algorithm::search(temp.begin(), temp.end(), GPU, comparator);
			temp.clear();
			if (bruh.size()) temp.fill(bruh.begin(), bruh.end());
			break;
		}
		case 4:
		{
			bool (*comparator)(T, string) = nullptr;
			comparator = &RAMSearch;
			int RAM;
			Exception::isNumber(RAM, 256, 0);

			auto bruh = Algorithm::search(temp.begin(), temp.end(), to_string(RAM), comparator);
			//Print(bruh);
			temp.clear();
			if(bruh.size()) temp.fill(bruh.begin(), bruh.end());
			break;
		}
		case 5:
		{
			if (!temp.size()) cout << "Empty list!" << endl;
			else Print(temp);
			break;
		}
		default:
			break;
		}
	} while (answer);
}

template<class T>
void Interface<T>::MultimapSearch()
{
	//typedef function<T, string> myTempalte;

	multimap<string, bool (*)(T, string)> comparators;
	multimap<string, string> values;

	int answer;

	do {
		cout << "Choose parameter: " << endl
			<< "1. Manufacturer" << endl
			<< "2. CPU" << endl
			<< "3. GPU" << endl
			<< "4. RAM" << endl;
		
		Exception::isNumber(answer, 4, 1);

		switch (answer)
		{
		case 1:
		{
			if (!comparators.count("Manufacturer"))
			{
				comparators.emplace("Manufacturer", &ManufacturerSearch<T, string>);

				cout << "Enter search value: ";
				string value;
				Exception::isString(value);

				values.emplace("Manufacturer", value);
			}
			else
			{
				cout << "This parameter already added!" << endl;
			}

			break;
		}
		case 2:
		{
			if (!comparators.count("CPU"))
			{
				comparators.emplace("CPU", &CPUSearch<T, string>);

				cout << "Enter search value: ";
				string value;
				Exception::isString(value);

				values.emplace("CPU", value);
			}
			else
			{
				cout << "This parameter already added!" << endl;
			}

			break;
		}
		case 3:
		{
			if (!comparators.count("GPU"))
			{
				comparators.emplace("GPU", &GPUSearch<T, string>);

				cout << "Enter search value: ";
				string value;
				Exception::isString(value);

				values.emplace("GPU", value);
			}
			else
			{
				cout << "This parameter already added!" << endl;
			}

			break;
		}
		case 4:
		{
			if (!comparators.count("RAM"))
			{
				comparators.emplace("RAM", &RAMSearch<T, string>);

				cout << "Enter search value: ";
				int value;
				Exception::isNumber(value, 256, 0);

				values.emplace("RAM", to_string(value));
			}
			else
			{
				cout << "This parameter already added!" << endl;
			}

			break;
		}
		default:
			break;
		}

		cout << "Add one more paramater? (0/1): ";
		Exception::isNumber(answer, 1, 0);
	}while(answer);

	auto list = Algorithm::search(object.begin(), object.end(), values, comparators);

	comparators.clear();
	values.clear();

	if (list.size()) Print(list);
	else cout << "Empty list" << endl;
}

template<class T>
void Interface<T>::Print(DoublyLinkedList<T> lhs)
{
	cout << endl;

	T::printHeader();
	cout << endl;

	if (lhs.size())
	{
		for (auto it = lhs.begin(); it != lhs.end(); ++it)
		{
			cout << (*it) << endl;
		}
	}

	cout << endl;
}

template<class T>
void Interface<T>::BinaryFile()
{
	int answer;

	cout << "Choose one: " << endl
		<< "1. Read from binary file" << endl
		<< "2. Write to binary file" << endl
		<< "3. Clear binary file" << endl
		<< "0. Exit" << endl;

	Exception::isNumber(answer, 3, 0);

	switch (answer)
	{
	case 0:
	{
		break;
	}
	case 1:
	{
		string filename = typeid(T).name();
		BinaryFileClass<T> file(filename + ".bin", fstream::in);
		file.Open();
		file.Read(object);
		file.Close();
		break;
	}
	case 2:
	{
		if (!object.size()) break;

		string filename = typeid(T).name();
		BinaryFileClass<T> file(filename + ".bin");
		file.Open();
		file.Write(object);
		file.Close();
		break;
	}
	case 3:
	{
		string filename = typeid(T).name();
		BinaryFileClass<T> file(filename + ".bin", fstream::trunc);
		file.Open();
		file.Close();
		break;
	}
	default:
		break;
	}
}

template<class T>
void Interface<T>::TextFile()
{
	int answer;

	cout << "Choose one: " << endl
		<< "1. Read from text file" << endl
		<< "2. Write to text file" << endl
		<< "3. Clear text file" << endl
		<< "0. Exit" << endl;

	Exception::isNumber(answer, 3, 0);

	switch (answer)
	{
	case 0:
	{
		break;
	}
	case 1:
	{
		string filename = typeid(T).name();
		TextFileClass<T> file(filename + ".txt", fstream::in);
		file.Open();
		file.Read(object);
		file.Close();
		break;
	}
	case 2:
	{
		if (!object.size()) break;

		string filename = typeid(T).name();
		TextFileClass<T> file(filename + ".txt");
		file.Open();
		file.Write(object);
		file.Close();
		break;
	}
	case 3:
	{
		string filename = typeid(T).name();
		TextFileClass<T> file(filename + ".txt", fstream::trunc);
		file.Open();
		file.Close();
		break;
	}
	default:
		break;
	}
}