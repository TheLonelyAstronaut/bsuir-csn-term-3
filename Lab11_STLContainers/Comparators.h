#pragma once

template<class T, class K>
bool ManufacturerSearch(T lhs, K rhs);

template<class T, class K>
bool CPUSearch(T lhs, K rhs);

template<class T, class K>
bool GPUSearch(T lhs, K rhs);

template<class T, class K>
bool RAMSearch(T lhs, K rhs);

template<class T>
bool ManufacturerComparator(T lhs, T rhs);

template<class T>
bool CPUComparator(T lhs, T rhs);

template<class T>
bool GPUComparator(T lhs, T rhs);

template<class T>
bool RAMComparator(T lhs, T rhs);