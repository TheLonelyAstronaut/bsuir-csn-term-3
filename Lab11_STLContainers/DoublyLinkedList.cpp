#include "DoublyLinkedList.h"

// default constructor w/ no parameters

template<class T>
DoublyLinkedList<T>::DoublyLinkedList()    // constructor, no parameter and will call member function
{
	init(); // initialization private function
}

template<class T>
DoublyLinkedList<T>::~DoublyLinkedList() // destructor
{
	clear(); // housekeeping job, // delete elements as needed
	delete head;  // head node
	delete tail;  // tail node
}

template<class T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T>& rhs) // copy constructor
{
	init();  // call function to do the copy need it
	for (auto itr = rhs.begin(); itr != rhs.end(); ++itr)
		push_back(*itr); // move
}

template <class T>
template <class K>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<K>& p)
{
	init();
	auto iterator = p.begin();

	for (; iterator != p.end(); ++iterator)
	{
		this->push_back((*iterator));
	}
}

template<class T> // move constructor
DoublyLinkedList<T>::DoublyLinkedList(DoublyLinkedList<T>&& rhs) : theSize(rhs.theSize), head{ rhs.head }, tail{ rhs.tail }
{
	rhs.theSize = 0;	rhs.head = nullptr;		rhs.tail = nullptr;
}

template<class T> // copy assignment
const DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(const DoublyLinkedList<T>& rhs)
{
	auto cpy = rhs;  // swap by reference
	std::swap(*this, cpy);  	return *this;
}

template<class T>	// move assignment
DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(DoublyLinkedList<T>&& rhs)
{
	init();
	*this = std::move(rhs);	return *this;
}

template<class T>
DoublyLinkedList<T>::DoublyLinkedList(int num, const T& val)
{
	init(); int index;
	for (index = 0; index < num; index++)
		push_back(val); // note  // insert val ahead of itr
}

template<class T> // constructor w/ element [start, end)
DoublyLinkedList<T>::DoublyLinkedList(ConstListIterator<T> start, ConstListIterator<T> end)
{
	init();
	for (auto itr = start; itr != end; ++itr)
		push_back(*itr);
}

template<class T>   // number of elements
int DoublyLinkedList<T>::size() const
{
	return theSize;
}

template<class T>
void DoublyLinkedList<T>::reverse() // to reverse the order of elements if not empty
{
	if (!empty()) // check to be valid
	{
		auto current_ptr = head;
		while (current_ptr != nullptr)
		{
			std::swap(current_ptr->next, current_ptr->prev);
			current_ptr = current_ptr->prev;
		}
	}
	std::swap(head, tail); // awesome
}

template<class T> // first element
T& DoublyLinkedList<T>::front()
{
	return *begin();  // ref. iterator to first element
}

template<class T>
const T& DoublyLinkedList<T>::front() const // now, w/ no permission to modify 1st elem.
{
	return *begin();
}

template<class T> //  last element
T& DoublyLinkedList<T>::back()
{
	return *(--end());
}

template<class T> // now, w/ no permission to modify last elem.
const T& DoublyLinkedList<T>::back() const
{
	return *(--end());
}

template<class T>
void DoublyLinkedList<T>::push_front(T&& val)
{
	insert(begin(), std::move(val));
}
// insert to the beginning, alias, insert right after Node* head
template<class T>
void DoublyLinkedList<T>::push_front(const T& val)
{
	insert(begin(), val);  // insert after Node head
}

template<class T>
void DoublyLinkedList<T>::push_back(T&& val) // move version insert_back
{
	insert(end(), std::move(val));
}

template<class T>
void DoublyLinkedList<T>::pop_front() // delete first element
{
	erase(begin());
}

template<class T>
void DoublyLinkedList<T>::clear()
{
	while (!empty())
		pop_back(); // to delete all the elements
}
// insert to the end, alias, insert right before Node* tail
template<class T>
void DoublyLinkedList<T>::push_back(const T& val)
{
	insert(end(), val);
}

template<class T>
void DoublyLinkedList<T>::pop_back()
{
	erase(--end()); // erase one element, the last one.
}

template<class T>
bool DoublyLinkedList<T>::empty() const
{
	return size() == 0; // ==0  check if list is empty of nodes
}

template<class T>
void DoublyLinkedList<T>::remove(const T& val) // remove all elements with value val
{
	for (auto itr = begin(); itr != end(); ++itr)
	{
		if (*itr == val)
		{
			erase(itr);
		}
	}
}

template<class T>
void DoublyLinkedList<T>::sort()
{
	qsort(head->next, tail->prev);
}

template<class T>
void DoublyLinkedList<T>::qsort(Node<T>* left, Node<T>* right)
{	
	if (right != nullptr && left != right && left != right->next)
	{
		Node<T>* temp = partition(left, right);
		qsort(left, temp->prev);
		qsort(temp->next, right);
	}
}

template<class T>
Node<T>* DoublyLinkedList<T>::partition(Node<T>* left, Node<T>* right)
{
	T data = right->data;
	Node<T>* i = left->prev;

	for (Node<T>* temp = left; temp != right; temp = temp->next)
	{
		if (temp->data <= data)
		{
			i = (i == nullptr) ? left : i->next;
			
			T info = temp->data;
			temp->data = i->data;
			i->data = info;
		}
	}

	i = (i == nullptr) ? left : i->next;

	T info = right->data;
	right->data = i->data;
	i->data = info;

	return i;
}

template<class T>
ListIterator<T> DoublyLinkedList<T>::begin()
{
	ListIterator<T> itr{ head->next };
	return itr; // begining node (copy)
}

template<class T>
ConstListIterator<T> DoublyLinkedList<T>::begin() const
{
	if (!empty())
	{
		ConstListIterator<T> const_itr{ head->next };
		return const_itr;
	}
}

template<class T>
ListIterator<T> DoublyLinkedList<T>::end()
{
	ListIterator<T> itr{ tail };
	return itr; // return the end node (copy)
}

template<class T>
ConstListIterator<T> DoublyLinkedList<T>::end() const
{
	ConstListIterator<T> const_itr{ tail };
	return const_itr;
}

template<class T>
ListIterator<T> DoublyLinkedList<T>::insert(ListIterator<T> itr, const T& val)
{ // pointer "p" is copy of current node
	auto* p = itr.current;  theSize++;
	auto* nptr = new Node<T>{ val, p->prev, p };
	p->prev->next = nptr;
	p->prev = nptr;
	ListIterator<T> iter{ nptr };	return iter;
}

template<class T>
ListIterator<T> DoublyLinkedList<T>::insert(ListIterator<T> itr, T&& val)
{
	auto* p = itr.current;	theSize++;
	auto* nptr = new Node<T>{ std::move(val), p->prev, p };
	p->prev->next = nptr;
	p->prev = nptr;
	ListIterator<T> iter{ nptr };	return iter;
}

template<class T>
ListIterator<T> DoublyLinkedList<T>::erase(ListIterator<T> start, ListIterator<T> end)
{
	for (auto itr = start; itr != end;) // erase from start to end
		itr = erase(itr); //  not including end. Alias, erase [start, end)

	return end;
}
template<class T>
void DoublyLinkedList<T>::init()  // init. private function
{
	theSize = 0;
	head = new Node<T>();	tail = new Node<T>();
	head->next = tail;	tail->prev = head;
}
template<class T>
typename ListIterator<T> DoublyLinkedList<T>::erase(ListIterator<T> itr)
{  // erase one element
	auto* p = itr.current;
	auto* test = itr.current; ListIterator<T> value{ p->next };
	p->prev->next = test->next;
	p->next->prev = test->prev;
	theSize--;		return value;
}

template<class T>
void DoublyLinkedList<T>::fill(ListIterator<T> begin, ListIterator<T> end)
{
	this->clear();

	for (; begin != end; ++begin)
	{
		this->push_back((*begin));
	}
}
// overloading comparison
template<class T>
bool operator!=(const DoublyLinkedList<T>& lhs, const DoublyLinkedList<T>& rhs)
{
	return !(lhs == rhs);
}
template<class T>
bool operator==(const DoublyLinkedList<T>& lhs, const DoublyLinkedList<T>& rhs)
{
	bool flag = true;
	if (lhs.size() == rhs.size())
	{
		auto rhs_itr = rhs.begin();
		for (auto lhs_itr = lhs.begin(); lhs_itr != lhs.end(); ++lhs_itr)
		{
			if (*lhs_itr != *rhs_itr)
				flag = false; break;
			++rhs_itr;
		}
		return flag;
	}
	else
		return false;
}
// overloading output
template<class T>
std::ostream& operator<<(std::ostream& os, const DoublyLinkedList<T>& l)
{
	l.print(os);	return os;
}
// print out all elements
template<class T>
void DoublyLinkedList<T>::print(std::ostream& os, char ofc) const
{
	for (auto itr = begin(); itr != end(); ++itr)
		os << *itr << ofc;  // ofc is the deliminitor
}