#pragma once
#include "StationaryMachine.h"
#include <iostream>
#include <string>
using namespace std;

class PC : public StationaryMachine
{
protected:
	char Case[_STRING_MAX_LENGTH];
public:
	PC(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, string _powersupply = "", string _case = "") :
		StationaryMachine(_manufacturer, _cpu, _gpu, _ram, _powersupply, true)
	{
		SetCase(_case);
	}
	~PC() noexcept{}
	static void printHeader();
public:
	string GetCase();
public:
	void SetCase(string _case);
public:
	friend istream& operator>>(istream& in, PC& object);
	friend fstream& operator>>(fstream& in, PC& object);
	friend ostream& operator<<(ostream& out, const PC& object);
	friend fstream& operator<<(fstream& out, const PC& object);
	bool operator==(const PC& object);
	bool operator!=(const PC& object);
};

