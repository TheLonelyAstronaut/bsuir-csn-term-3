#include "InvalidLanguageException.h"

InvalidLanguageException::InvalidLanguageException(string message, string defaultLNG) : Exception(message), language(defaultLNG) {}

InvalidLanguageException::~InvalidLanguageException() {}

string InvalidLanguageException::Language() { return language; }