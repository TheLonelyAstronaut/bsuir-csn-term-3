#include "PC.h"
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include "InputSizeException.h"
#include "StationaryMachine.h"
using namespace std;

string PC::GetCase()
{
	return this->Case;
}

void PC::SetCase(string _case)
{
	memset(this->Case, 0, _STRING_MAX_LENGTH);
	strcpy_s(this->Case, _case.length() + 1, _case.c_str());
}

istream& operator>>(istream& in, PC& object)
{
	in >> static_cast<StationaryMachine&>(object);
	string exOnly;
	cout << "Case: ";
	Exception::isString(exOnly);
	object.SetCase(exOnly);

	return in;
}

ostream& operator<<(ostream& out, const PC& object)
{
	out << static_cast<const StationaryMachine&>(object);
	string _case = object.Case;
	out << setw(15) << _case;
	return out;
}

void PC::printHeader()
{
	StationaryMachine::printHeader();
	cout << setw(15) << "Case";
}

bool PC::operator==(const PC& object)
{
	if (static_cast<const StationaryMachine&>(object) == static_cast<const StationaryMachine&>(*this))
		if (!strcmp(object.Case, this->Case))
			return true;
		else
			return false;
	else
		return false;
	
}

bool PC::operator!=(const PC& object)
{
	if (object == (*this)) return false;
	else return true;
}

fstream& operator<<(fstream& out, const PC& object)
{
	out << static_cast<const StationaryMachine&>(object);
	out << " Case:\"" << object.Case << "\"; }" << endl;
	return out;
}

fstream& operator>>(fstream& in, PC& object)
{
	string textedObject;
	getline(in, textedObject);
	
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetManufacturer(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetCPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetGPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetRAM(stoi(textedObject.substr(0, textedObject.find("\""))));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetPowerSupply(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetCase(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	return in;
}