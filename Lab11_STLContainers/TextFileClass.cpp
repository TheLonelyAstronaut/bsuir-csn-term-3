#include "TextFileClass.h"
#include <iostream>
#include <string>
#include <iosfwd>
#include <fstream>

using namespace std;

template <class T>
TextFileClass<T>::~TextFileClass()
{
	if (this->isOpened())
	{
		this->Close();
	}
}

template<class T>
bool TextFileClass<T>::Open()
{
	try
	{
		fileStream.open(filename, bitMask);

		if (!this->isOpened())
		{
			fileStream.open(filename, fstream::out);
			fileStream.close();
			fileStream.open(filename, bitMask);
		}

		if (fileStream.is_open()) return true;
		else return false;
	}
	catch (...)
	{
		return false;
	}
}

template <class T>
bool TextFileClass<T>::Close()
{
	if (fileStream.is_open())
	{
		fileStream.close();
		if (fileStream.is_open())
			return false;
		else
			return true;
	}
	return true;
}

template<class T>
bool TextFileClass<T>::isEnded()
{
	int realPos = fileStream.tellg();
	fileStream.seekg(0, fstream::end);
	int eofPos = fileStream.tellg();
	fileStream.seekg(realPos, fstream::beg);

	if (realPos == eofPos) return true;
	else return false;
}

template<class T>
bool TextFileClass<T>::isOpened()
{
	if (fileStream.is_open()) return true;
	else return false;
}

template<class T>
void TextFileClass<T>::Write(DoublyLinkedList<T> _toFile)
{
	if (!fileStream.is_open()) return;

	auto it = _toFile.begin();

	for (; it != _toFile.end(); ++it)
		fileStream << (*it);
}

template<class T>
void TextFileClass<T>::Read(DoublyLinkedList<T>& list)
{
	SeekG(0);

	while (!this->isEnded())
	{
		T element;
		fileStream >> element;
		list.push_back(element);
		//getline(fileStream, test);
	}
}