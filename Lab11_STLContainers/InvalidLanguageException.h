#pragma once
#include "Exception.h"

class InvalidLanguageException : public Exception
{
protected:
	string language;
public:
	InvalidLanguageException(string meassage = "", string defaultLNG = "ENG");
	~InvalidLanguageException() throw();
	string Language();
};

