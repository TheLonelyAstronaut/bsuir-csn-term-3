#pragma once
#include "ComputingMachine.h"
#include <iostream>
#include <string>
using namespace std;

class StationaryMachine abstract : public ComputingMachine
{
protected:
	char PowerSupply[_STRING_MAX_LENGTH];
	bool WithDisplay;
public:
	StationaryMachine(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, string _powersupply = "", bool _withdisplay = false) :
		ComputingMachine(_manufacturer, _cpu, _gpu, _ram),
		WithDisplay(_withdisplay)
	{	
		SetPowerSupply(_powersupply);
	}
	virtual ~StationaryMachine() {}
	static void printHeader();
public:
	string GetPowerSupply() { return PowerSupply; }
	string GetWithDisplay() { return WithDisplay ? "Yes" : "No"; }
public:
	void SetPowerSupply(string _powersupply)
	{
		memset(PowerSupply, 0, _STRING_MAX_LENGTH);
		strcpy_s(PowerSupply, _powersupply.length() + 1, _powersupply.c_str());
	}
	void SetWithDisplay(bool _withdisplay) { WithDisplay = _withdisplay; }
public:
	friend istream& operator>>(istream& in, StationaryMachine& object);
	friend ostream& operator<<(ostream& out, const StationaryMachine& object);
	friend fstream& operator<<(fstream& out, const StationaryMachine& object);
	friend bool operator==(const StationaryMachine& lhs, const StationaryMachine& rhs);
	friend bool operator!=(const StationaryMachine& lhs, const StationaryMachine& rhs);
};