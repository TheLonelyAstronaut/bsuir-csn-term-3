#pragma once
#include "const_iterator.h"
#include "Node.h"

template <class T>
class iterator : public const_iterator<T> 
{
public:
	iterator();
	T& operator*();
	const T& operator*() const;

	// increment/decrement operators
	iterator& operator++();
	iterator operator++(int);
	iterator& operator--();
	iterator operator--(int);

protected:
	iterator(Node* p);
	friend class DoublyLinkedList<T>;
};