#pragma once
#include <iostream>
#include <string>
using namespace std;
#define _STRING_MAX_LENGTH 80

class ComputingMachine abstract
{
protected:
	char Manufacturer[_STRING_MAX_LENGTH], CPU[_STRING_MAX_LENGTH], GPU[_STRING_MAX_LENGTH];
	int RAM;
public:
	ComputingMachine(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0) : RAM(_ram)
	{
		SetManufacturer(_manufacturer);
	}
	virtual ~ComputingMachine() {}
	static void printHeader();
public:
	string GetManufacturer() { return Manufacturer; }
	string GetCPU() { return CPU; }
	string GetGPU() { return GPU; }
	int GetRam() { return RAM; }
public:
	void SetManufacturer(string _manufacturer) 
	{
		memset(Manufacturer, 0, _STRING_MAX_LENGTH);
		//Manufacturer = new char[_manufacturer.length() + 1];
		strcpy_s(Manufacturer, _manufacturer.length() + 1, _manufacturer.c_str());
	}
	void SetCPU(string _cpu)
	{
		memset(CPU, 0, _STRING_MAX_LENGTH);
		strcpy_s(CPU, _cpu.length() + 1, _cpu.c_str());
	}
	void SetGPU(string _gpu)
	{
		memset(GPU, 0, _STRING_MAX_LENGTH);
		strcpy_s(GPU, _gpu.length() + 1, _gpu.c_str());
	}
	void SetRAM(int _ram) { RAM = _ram; }
public:
	friend istream& operator>>(istream& in, ComputingMachine& object);
	friend ostream& operator<<(ostream& out, const ComputingMachine& object);
	friend fstream& operator<<(fstream& out, const ComputingMachine& object);
	friend bool operator==(const ComputingMachine& lhs, const ComputingMachine& rhs);
	friend bool operator!=(const ComputingMachine& lhs, const ComputingMachine& rhs);
};