#pragma once
#include <iostream>
#include <string>
#include <iosfwd>
#include <fstream>
using namespace std;

class FileClass abstract
{
protected:
	string filename;
	fstream fileStream;
	int bitMask;
public:
	FileClass(string _filename = "", int _bitMask = ios::in) : filename(_filename), bitMask(_bitMask) {}
	void SeekG(int quantity, int pos = ios::beg) { fileStream.seekg(quantity, pos); }
	virtual ~FileClass() {}
};

