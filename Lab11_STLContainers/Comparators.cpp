#include "Comparators.h"
#include <string>

template<class T, class K>
bool ManufacturerSearch(T lhs, K rhs)
{
	if (lhs.GetManufacturer() == rhs) return true;
	else return false;
}

template<class T, class K>
bool CPUSearch(T lhs, K rhs)
{
	if (lhs.GetCPU() == rhs) return true;
	else return false;
}

template<class T, class K>
bool GPUSearch(T lhs, K rhs)
{
	if (lhs.GetGPU() == rhs) return true;
	else return false;
}

template<class T, class K>
bool RAMSearch(T lhs, K rhs)
{
	if (lhs.GetRam() == std::stoi(rhs)) return true;
	else return false;
}

template<class T>
bool ManufacturerComparator(T lhs, T rhs)
{
	if (lhs.GetManufacturer() > rhs.GetManufacturer()) return true;
	else return false;
}

template<class T>
bool CPUComparator(T lhs, T rhs)
{
	if (lhs.GetCPU() > rhs.GetCPU()) return true;
	else return false;
}

template<class T>
bool GPUComparator(T lhs, T rhs)
{
	if (lhs.GetGPU() > rhs.GetGPU()) return true;
	else return false;
}

template<class T>
bool RAMComparator(T lhs, T rhs)
{
	if (lhs.GetRam() > rhs.GetRam()) return true;
	else return false;
}