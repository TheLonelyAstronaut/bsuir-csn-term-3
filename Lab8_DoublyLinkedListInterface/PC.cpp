#include "PC.h"
#include <iostream>
#include <string>
#include <iomanip>
#include "InputSizeException.h"
#include "StationaryMachine.h"
using namespace std;

string PC::GetCase()
{
	return this->Case;
}

void PC::SetCase(string _case)
{
	this->Case = _case;
}

istream& operator>>(istream& in, PC& object)
{
	in >> static_cast<StationaryMachine&>(object);
	cout << "Case: ";
	Exception::isString(object.Case);

	return in;
}

ostream& operator<<(ostream& out, const PC& object)
{
	out << static_cast<const StationaryMachine&>(object);
	out << setw(15) << object.Case;
	return out;
}

void PC::printHeader()
{
	StationaryMachine::printHeader();
	cout << setw(15) << "Case";
}

bool PC::operator==(const PC& object)
{
	if (static_cast<const StationaryMachine&>(object) == static_cast<const StationaryMachine&>(*this))
		if (object.Case == this->Case)
			return true;
		else
			return false;
	else
		return false;
	
}

bool PC::operator!=(const PC& object)
{
	if (object == (*this)) return false;
	else return true;
}