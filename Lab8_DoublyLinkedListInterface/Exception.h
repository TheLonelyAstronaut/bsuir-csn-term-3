#pragma once
#include <string>
using namespace std;

class Exception : public exception
{
protected:
	string message;
public:
	Exception(string _message = "");
	virtual ~Exception() throw();
	virtual string Message();
	static void isNumber(int& number, int maxValue = 0, int minValue = 0);
	static void isString(string& str, int maxLength = 80, string language = "ENG");
};

