#include "DoublyLinkedList.h"
#include "ArgumentOutOfRangeException.h"

// default constructor w/ no parameters

template<class T>
DoublyLinkedList<T>::DoublyLinkedList()    // constructor, no parameter and will call member function
{
	init(); // initialization private function
}

template<class T>
DoublyLinkedList<T>::~DoublyLinkedList() // destructor
{
	clear(); // housekeeping job, // delete elements as needed
	delete head;  // head node
	delete tail;  // tail node
}

template<class T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T>& rhs) // copy constructor
{
	init();  // call function to do the copy need it
	for (Node<T>* itr = rhs.head; itr != rhs.tail; itr=itr->next)
		push_back(itr->data); // move
}

template<class T> // move constructor
DoublyLinkedList<T>::DoublyLinkedList(DoublyLinkedList<T>&& rhs) : theSize(rhs.theSize), head{ rhs.head }, tail{ rhs.tail }
{
	rhs.theSize = 0;	rhs.head = nullptr;		rhs.tail = nullptr;
}

template<class T> // copy assignment
const DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(const DoublyLinkedList<T>& rhs)
{
	auto cpy = rhs;  // swap by reference
	std::swap(*this, cpy);  	return *this;
}

template<class T>	// move assignment
DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(DoublyLinkedList<T>&& rhs)
{
	init();
	*this = std::move(rhs);	return *this;
}

template<class T>
DoublyLinkedList<T>::DoublyLinkedList(int num, const T& val)
{
	init();
	int index;
	for (index = 0; index < num; index++)
		push_back(val); // note  // insert val ahead of itr
}

template<class T>   // number of elements
int DoublyLinkedList<T>::size() const
{
	return theSize;
}

template<class T>
void DoublyLinkedList<T>::reverse() // to reverse the order of elements if not empty
{
	if (!empty()) // check to be valid
	{
		auto current_ptr = head;
		while (current_ptr != nullptr)
		{
			std::swap(current_ptr->next, current_ptr->prev);
			current_ptr = current_ptr->prev;
		}
	}
	std::swap(head, tail); // awesome
}

template<class T> // first element
T& DoublyLinkedList<T>::front()
{
	return head->next->data;  // ref. iterator to first element
}

template<class T>
const T& DoublyLinkedList<T>::front() const // now, w/ no permission to modify 1st elem.
{
	return head->next->data;
}

template<class T> //  last element
T& DoublyLinkedList<T>::back()
{
	return tail->prev->data;
}

template<class T> // now, w/ no permission to modify last elem.
const T& DoublyLinkedList<T>::back() const
{
	return tail->prev->data;
}

// insert to the beginning, alias, insert right after Node* head
template<class T>
void DoublyLinkedList<T>::push_front(const T& val)
{
	insert(val, -1);
}

template<class T>
void DoublyLinkedList<T>::pop_front() // delete first element
{
	erase(head->next);
}

template<class T>
void DoublyLinkedList<T>::clear()
{
	while (!empty())
		pop_back(); // to delete all the elements
}
// insert to the end, alias, insert right before Node* tail
template<class T>
void DoublyLinkedList<T>::push_back(const T& val)
{
	insert(val, theSize - 1);
}

template<class T>
void DoublyLinkedList<T>::pop_back()
{
	erase(tail->prev);
}

template<class T>
bool DoublyLinkedList<T>::empty() const
{
	return size() == 0; // ==0  check if list is empty of nodes
}

template<class T>
void DoublyLinkedList<T>::remove(const T& val) // remove all elements with value val
{
	Node<T>* search = head->next;

	for (; search != tail && search; search=search->next)
	{
		if (search->data == val)
		{
			Node<T>* next = search->next;
			erase(search);
			search = next;
		}
	}
}

template<class T>
void DoublyLinkedList<T>::erase(Node<T>* pointer)
{
	pointer->prev->next = pointer->next;
	pointer->next->prev = pointer->prev;
	--theSize;
	delete pointer;
}

template<class T>
void DoublyLinkedList<T>::insert(const T& val, int index)
{
	if (index > theSize) throw ArgumentOutOfRangeException("Argument is out of range!", 0, theSize-1);

	Node<T>* element = head->next;
	//cout << element << " " << tail << endl;

	for (int i = 0; i <= index; ++i, element = element->next);

	Node<T>* insertingElement = new Node<T>(val);

	element->prev->next = insertingElement;
	insertingElement->prev = element->prev;
	insertingElement->next = element;
	element->prev = insertingElement;

	++theSize;
}

template<class T>
T& DoublyLinkedList<T>::operator[](int index)
{
	if (index >= theSize) throw ArgumentOutOfRangeException("Argument is out of range!", 0, theSize - 1);

	Node<T>* search = head->next;

	for (int i = 0; i < index; ++i, search = search->next);

	return search->data;
}

template<class T>
int DoublyLinkedList<T>::search(const T& val)
{
	Node<T>* search = head->next;
	int position = 0;

	for (; search != tail; search=search->next, ++position)
	{
		if (search->data == val)
		{
			return position;
		}
	}

	return -1;
}

template<class T>
void DoublyLinkedList<T>::init()  // init. private function
{
	theSize = 0;
	head = new Node<T>();	
	tail = new Node<T>();
	head->next = tail;	
	tail->prev = head;
}
