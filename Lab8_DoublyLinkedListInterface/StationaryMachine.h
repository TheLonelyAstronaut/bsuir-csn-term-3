#pragma once
#include "ComputingMachine.h"
#include <iostream>
#include <string>
using namespace std;

class StationaryMachine abstract : public ComputingMachine
{
protected:
	string PowerSupply;
	bool WithDisplay;
public:
	StationaryMachine(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, string _powersupply = "", bool _withdisplay = false) :
		ComputingMachine(_manufacturer, _cpu, _gpu, _ram),
		PowerSupply(_powersupply),
		WithDisplay(_withdisplay) {}
	virtual ~StationaryMachine() {}
	static void printHeader();
public:
	string GetPowerSupply() { return PowerSupply; }
	string GetWithDisplay() { return WithDisplay ? "Yes" : "No"; }
public:
	void SetPowerSupply(string _powersupply) { PowerSupply = _powersupply; }
	void SetWithDisplay(bool _withdisplay) { WithDisplay = _withdisplay; }
public:
	friend istream& operator>>(istream& in, StationaryMachine& object);
	friend ostream& operator<<(ostream& out, const StationaryMachine& object);
	friend bool operator==(const StationaryMachine& lhs, const StationaryMachine& rhs);
	friend bool operator!=(const StationaryMachine& lhs, const StationaryMachine& rhs);
};