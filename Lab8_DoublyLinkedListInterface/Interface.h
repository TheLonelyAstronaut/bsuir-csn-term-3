#pragma once
#include "DoublyLinkedList.h"
#include "DoublyLinkedList.cpp"

template<class T>
class Interface
{
private:
	DoublyLinkedList<T> object = DoublyLinkedList<T>();
	void Menu();
public:
	Interface();
	~Interface();
	void Show();
};

