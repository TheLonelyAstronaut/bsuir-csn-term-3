#include "Interface.h"
#include "PC.h"
#include "Exception.h"
#include <iostream>
#include <type_traits>
using namespace std;

template<class T>
Interface<T>::Interface()
{
	T test= T();
	decltype(&T::printHeader);
	//cout << foo << endl;
}

template<class T>
Interface<T>::~Interface()
{
	object.clear();
}

template<class T>
void Interface<T>::Menu()
{
	cout << "Choose action: " << endl
		<< "1. Add element to the front" << endl
		<< "2. Add element to the back" << endl
		<< "3. Pop element from the front" << endl
		<< "4. Pop element from the back" << endl
		<< "5. Clear list" << endl
		<< "6. Remove by index" << endl
		<< "7. Print to console" << endl
		<< "8. Show menu" << endl
		<< "0. Exit" << endl;
}

template<class T>
void Interface<T>::Show()
{
	int answer;
	Menu();

	do {
		cout << "Your choose: ";
		Exception::isNumber(answer, 8, 0);

		switch (answer)
		{
		case 0:
			break;
		case 1:
		{
			T toList = T();
			cin >> toList;
			object.push_front(toList);
			cout << "Success!" << endl;
			break;
		}
		case 2:
		{
			T toList = T();
			cin >> toList;
			object.push_back(toList);
			cout << "Success!" << endl;
			break;
		}
		case 3:
		{
			object.pop_front();
			cout << "Success!" << endl;
			break;
		}
		case 4:
		{
			object.pop_back();
			cout << "Success!" << endl;
			break;
		}
		case 5:
		{
			object.clear();
			cout << "Success!" << endl;
			break;
		}
		case 6:
		{
			cout << "Enter index: ";
			int index;
			Exception::isNumber(index, object.size()-1, 0);
			T test = object[index];
			object.remove(test);
			cout << "Success!" << endl;
			break;
		}
		case 7:
		{
			T::printHeader();
			cout << endl;

			for (int i = 0; i < object.size(); ++i) cout << object[i] << endl;

			cout << endl;

			break;
		}
		case 8:
		{
			Menu();
			break;
		}
		default:
			break;
		}
	} while (answer);
}

