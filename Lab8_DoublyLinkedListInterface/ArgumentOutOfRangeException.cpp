#include "ArgumentOutOfRangeException.h"
#include <string>

ArgumentOutOfRangeException::ArgumentOutOfRangeException(string _message, int _maxValue, int _minValue) : Exception(_message), maxValue(_maxValue), minValue(_minValue) {}

ArgumentOutOfRangeException::~ArgumentOutOfRangeException() {}

int ArgumentOutOfRangeException::MaximumValue() { return maxValue; }

int ArgumentOutOfRangeException::MinimumValue() { return minValue; }

string ArgumentOutOfRangeException::Message()
{
	return message + "\nMaxValue: " + std::to_string(maxValue) \
		+ "\nMinValue: " + std::to_string(minValue);
}