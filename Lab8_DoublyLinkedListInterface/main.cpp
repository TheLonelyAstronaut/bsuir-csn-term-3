#include <iostream>
#include "PC.h"
#include "Monoblock.h"
#include "Laptop.h"
#include "Tablet.h"
#include <string>
#include "Interface.h"
#include "Interface.cpp"
//#include "DoublyLinkedList.h";
//#include "DoublyLinkedList.cpp"

using namespace std;

int main()
{
	int answer;

	do {
		cout << "Choose struct to work with:" << endl
			<< "1. PC" << endl
			<< "2. Monoblock" << endl
			<< "3. Laptop" << endl
			<< "4. Tablet" << endl
			<< "0. Exit" << endl;

		Exception::isNumber(answer, 4, 0);

		switch (answer)
		{
		case 1:
		{
			Interface<PC> pc = Interface<PC>();
			pc.Show();
			break;
		}
		case 2:
		{
			Interface<Monoblock> monoblock = Interface<Monoblock>();
			monoblock.Show();
			break;
		}
		case 3:
		{
			Interface<Laptop> laptop = Interface<Laptop>();
			laptop.Show();
			break;
		}
		case 4:
		{
			Interface<Tablet> tablet = Interface<Tablet>();
			tablet.Show();
			break;
		}
		case 0:
		default:
			break;
		}
	} while (answer); 

	return 0;
}