#include "StationaryMachine.h"
#include <iostream>
#include <string>
using namespace std;

istream& operator>>(istream& in, StationaryMachine& object)
{
	in >> static_cast<ComputingMachine&>(object);
	cout << "PowerSupply: ";
	getline(in, object.PowerSupply);
	return in;
}

ostream& operator<<(ostream& out, StationaryMachine& object)
{
	out << static_cast<ComputingMachine&>(object);
	out << "PowerSupply: " << object.PowerSupply << endl;
	return out;
}