#include "Monoblock.h"
#include "ComputingMachine.h"
#include <iostream>
#include <string>
using namespace std;

string Monoblock::GetTouch()
{
	return this->Touch;
}

void Monoblock::SetTouch(string _touch)
{
	this->Touch = _touch;
}

istream& operator>>(istream& in, Monoblock& object)
{
	in >> static_cast<StationaryMachine&>(object);
	cout << "Touch: ";
	getline(in, object.Touch);
	return in;
}

ostream& operator<<(ostream& out, Monoblock& object)
{
	out << static_cast<StationaryMachine&>(object);
	out << "Touch: " << object.Touch << endl;
	return out;
}