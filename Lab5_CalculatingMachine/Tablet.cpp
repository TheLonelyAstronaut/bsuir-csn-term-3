#include "Tablet.h"
#include <iostream>
#include <string>
using namespace std;

bool Tablet::GetWithCellurarModule()
{
	return this->WithCellurarModule;
}

void Tablet::SetWithCellurarModule(bool _withcellurarmodule)
{
	this->WithCellurarModule = _withcellurarmodule;
}

istream& operator>>(istream& in, Tablet& object)
{
	cout << "Manufacturer: ";
	getline(in, object.Manufacturer);
	cout << "CPU: ";
	getline(in, object.CPU);
	cout << "GPU: ";
	getline(in, object.GPU);
	cout << "RAM: ";
	in >> object.RAM;
	in.ignore(INT_MAX, '\n');
	cout << "Battery Capacity: ";
	in >> object.BatteryCapacity;
	in.ignore(INT_MAX, '\n');
	cout << "With Cellurar Module (Empty if no): ";
	string answer;
	in >> answer;
	if (answer != "") object.WithCellurarModule = true;
	else object.WithCellurarModule = false;
	return in;
}

ostream& operator<<(ostream& out, const Tablet& object)
{
	out << "Manufacturer: " << object.Manufacturer << endl;
	out << "CPU: " << object.CPU << endl;
	out << "GPU: " << object.GPU << endl;
	out << "RAM: " << object.RAM << endl;
	out << "Battery Capacity: " << object.BatteryCapacity << endl;
	out << "With Celurar Module: " << (object.WithCellurarModule ? "Yes" : "No") << endl;
	return out;
}