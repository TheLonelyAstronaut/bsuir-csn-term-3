#include "Laptop.h"
#include <iostream>
#include <string>
using namespace std;

string Laptop::GetKeyboardType()
{
	return this->KeyboardType;
}

void Laptop::SetKeyboardType(string _KeyboardType)
{
	this->KeyboardType = _KeyboardType;
}

istream& operator>>(istream& in, Laptop& object)
{
	cout << "Manufacturer: ";
	getline(in, object.Manufacturer);
	cout << "CPU: ";
	getline(in, object.CPU);
	cout << "GPU: ";
	getline(in, object.GPU);
	cout << "RAM: ";
	in >> object.RAM;
	in.ignore(INT_MAX, '\n');
	cout << "Battery Capacity: ";
	in >> object.BatteryCapacity;
	in.ignore(INT_MAX, '\n');
	cout << "KeyboardType: ";
	getline(in, object.KeyboardType);
	return in;
}

ostream& operator<<(ostream& out, const Laptop& object)
{
	out << "Manufacturer: " << object.Manufacturer << endl;
	out << "CPU: " << object.CPU << endl;
	out << "GPU: " << object.GPU << endl;
	out << "RAM: " << object.RAM << endl;
	out << "Battery Capacity: " << object.BatteryCapacity << endl;
	out << "KeyboardType: " << object.KeyboardType << endl;
	return out;
}