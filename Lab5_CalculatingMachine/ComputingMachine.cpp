#include "ComputingMachine.h"
#include <iostream>
#include <string>
using namespace std;

istream& operator>>(istream& in, ComputingMachine& object)
{
	cout << "Manufacturer: ";
	getline(in, object.Manufacturer);
	cout << "CPU: ";
	getline(in, object.CPU);
	cout << "GPU: ";
	getline(in, object.GPU);
	cout << "RAM: ";
	in >> object.RAM;
	in.ignore(INT_MAX, '\n');
	return in;
}

ostream& operator<<(ostream& out, ComputingMachine& object)
{
	out << "Manufacturer: " << object.Manufacturer << endl;
	out << "CPU: " << object.CPU << endl;
	out << "GPU: " << object.GPU << endl;
	out << "RAM: " << object.RAM << endl;
	return out;
}