#include "PC.h"
#include <iostream>
#include <string>
using namespace std;

string PC::GetCase()
{
	return this->Case;
}

void PC::SetCase(string _case)
{
	this->Case = _case;
}

istream& operator>>(istream& in, PC& object)
{
	cout << "Manufacturer: ";
	getline(in, object.Manufacturer);
	cout << "CPU: ";
	getline(in, object.CPU);
	cout << "GPU: ";
	getline(in, object.GPU);
	cout << "RAM: ";
	in >> object.RAM;
	in.ignore(INT_MAX, '\n');
	cout << "Case: ";
	getline(in, object.Case);
	return in;
}

ostream& operator<<(ostream& out, const PC& object)
{
	out << "Manufacturer: " << object.Manufacturer << endl;
	out << "CPU: " << object.CPU << endl;
	out << "GPU: " << object.GPU << endl;
	out << "RAM: " << object.RAM << endl;
	out << "PowerSupply: " << object.PowerSupply << endl;
	out << "Case: " << object.Case << endl;
	return out;
}