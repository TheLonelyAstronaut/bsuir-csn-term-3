#pragma once

class Matrix
{
private:
	int** matrix;
	int sizeX;
	int sizeY;
	void Setup();
public:
	Matrix();
	Matrix(int size);
	Matrix(const Matrix& element);
	void Initialize();
	void Show();
	void RandomFill();
	void SetSize(int sizeX, int sizeY);
	//Matrix GetSum(const Matrix& term);
	//Matrix GetComposition(const Matrix& multiplier);
	Matrix operator +(const Matrix& term);
	Matrix operator *(const Matrix& multiplier);
	Matrix& operator =(const Matrix& object);
	Matrix& operator ++();
	Matrix operator++(int);
	~Matrix();
};

void MatrixControl(Matrix&);
void InputControl(int&);