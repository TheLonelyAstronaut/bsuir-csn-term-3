#include "Matrix.h"
#include <iostream>
#include <iomanip>
using namespace std;


Matrix::Matrix()
{
	matrix = nullptr;
	sizeX = 0;
	sizeY = 0;
}

Matrix::Matrix(int size)
{
	this->sizeX = size;
	this->sizeY = size;

	Setup();
}

Matrix::Matrix(const Matrix& element)
{
	this->sizeX = element.sizeX;
	this->sizeY = element.sizeY;

	matrix = new int*[this->sizeX];

	for (int x = 0; x < this->sizeX; ++x)
	{
		matrix[x] = new int[this->sizeY];

		for (int y = 0; y < this->sizeY; ++y)
		{
			matrix[x][y] = element.matrix[x][y];
		}
	}
}

Matrix::~Matrix()
{
	for (int x = 0; x < sizeX; ++x)
	{
		delete[] matrix[x];
	}

	delete[] matrix;
}

void Matrix::Initialize()
{
	if (!this->sizeX)
	{
		cout << "size: ";
		InputControl(this->sizeX);
		this->sizeY = this->sizeY;
		
		Setup();
	}

	for (int x = 0; x < this->sizeX; ++x)
	{
		for (int y = 0; y < this->sizeY; ++y)
		{
			InputControl(matrix[x][y]);
		}
	}
}

void Matrix::Show()
{
	if (!matrix)
	{
		//cout << "Uninitialized." << endl << endl;
		return;
	}

	if (!this->sizeX)
	{
		//cout << "Empty matrix." << endl << endl;
		return;
	}

	for (int x = 0; x < this->sizeX; ++x)
	{
		for (int y = 0; y < this->sizeY; ++y)
		{
			cout << setw(5) << matrix[x][y];
		}

		cout << endl;
	}
}

void Matrix::RandomFill()
{
	if (!this->sizeX)
	{
		//cout << "Unlocated" << endl << endl;
		return;
	}

	for (int x = 0; x < this->sizeX; ++x)
	{
		for (int y = 0; y < this->sizeY; ++y)
		{
			matrix[x][y] = rand() % 10;
		}
	}
}

void Matrix::Setup()
{
	matrix = new int* [this->sizeX];

	for (int x = 0; x < sizeX; ++x)
	{
		matrix[x] = new int[this->sizeY];

		for (int y = 0; y < this->sizeY; ++y)
		{
			matrix[x][y] = 0;
		}
	}
}

Matrix Matrix::operator +(const Matrix& term)
{
	if (!this->sizeX || !term.sizeY)
	{
		return Matrix();
	}
	else if (term.sizeX != this->sizeX || term.sizeY != this->sizeY)
	{
		return Matrix();
	}
	else
	{
		Matrix result = Matrix(this->sizeX);

		for (int x = 0; x < this->sizeX; ++x)
		{
			for (int y = 0; y < this->sizeY; ++y)
			{
				result.matrix[x][y] = matrix[x][y] + term.matrix[x][y];
			}
		}

		return result;
	}
}

Matrix Matrix::operator *(const Matrix& multiplier)
{
	if (!this->sizeX || !multiplier.sizeY)
	{
		return Matrix();
	}
	else if (multiplier.sizeX != this->sizeY)
	{
		return Matrix();
	}
	else
	{
		Matrix result = Matrix(this->sizeX);

		for (int x = 0; x < this->sizeX; ++x)
		{
			for (int j = 0; j < multiplier.sizeY; ++j)
			{
				for (int y = 0; y < this->sizeY; ++y)
				{
					result.matrix[x][j] += matrix[x][y] * multiplier.matrix[y][j];
				}
			}
		}

		return result;
	}
}

Matrix& Matrix::operator =(const Matrix& object)
{
	if (this == &object)
		return *this;

	this->~Matrix();

	this->sizeX = object.sizeX;
	this->sizeY = object.sizeY;

	matrix = new int* [this->sizeX];

	for (int x = 0; x < this->sizeX; ++x)
	{
		matrix[x] = new int[this->sizeY];

		for (int y = 0; y < this->sizeY; ++y)
		{
			matrix[x][y] = object.matrix[x][y];
		}
	}

	return *this;
}

Matrix& Matrix::operator ++()
{
	for (int x = 0; x < this->sizeX; ++x)
	{
		for (int y = 0; y < this->sizeY; ++y)
		{
			this->matrix[x][y]++;
		}
	}

	return *this;
}

Matrix Matrix::operator ++(int)
{
	Matrix prev = *this;
	++(*this);
	return prev;
}

/*Matrix Matrix::GetSum(const Matrix& term)
{
	if (!this->sizeX || !term.sizeY)
	{
		cout << endl << "Unable to calculate sum: size of dimension can't be 0." << endl << endl;
		return Matrix();
	}
	else if (term.sizeX != this->sizeX || term.sizeY != this->sizeY)
	{
		cout << endl << "Unable to calculate sum: different sizes. " << endl << endl;
		return Matrix();
	}
	else {
		Matrix result = Matrix(this->sizeX, this->sizeY);

		for (int x = 0; x < this->sizeX; ++x)
		{
			for (int y = 0; y < this->sizeY; ++y)
			{
				result.matrix[x][y] = matrix[x][y] + term.matrix[x][y];
			}
		}
		
		return result;
		//int** tempMatrix = new int* [this->sizeX];

		for (int x = 0; x < this->sizeX; ++x)
		{
			tempMatrix[x] = new int[sizeY];

			for (int y = 0; y < this->sizeY; ++y)
			{
				tempMatrix[x][y] = matrix[x][y] + term.matrix[x][y];
			}
		}

		//return Matrix(tempMatrix, this->sizeX, this->sizeY);
	}
}

Matrix Matrix::GetComposition(const Matrix& multiplier)
{
	if (!this->sizeX || !multiplier.sizeY)
	{
		cout << endl << "Unable to calculate composition: size of dimension can't be 0." << endl << endl;
		return Matrix();
	}
	else if (multiplier.sizeX != this->sizeY)
	{
		cout << endl << "Unable to calculate composition: different sizes in the terms of matrix multiplying." << endl << endl;
		return Matrix();
	}
	else {
		Matrix result = Matrix(this->sizeX, multiplier.sizeY);

		for (int x = 0; x < this->sizeX; ++x)
		{
			for (int j = 0; j < multiplier.sizeY; ++j)
			{
				for (int y = 0; y < this->sizeY; ++y)
				{
					result.matrix[x][j] += matrix[x][y] * multiplier.matrix[y][j];
				}
			}
		}

		return result;
	}
}*/

void Matrix::SetSize(int sizeX, int sizeY)
{
	if (this->sizeX && this->sizeY)
	{
		return;
	}
	else if (!this->sizeY && this->sizeX)
	{
		this->sizeY = sizeY;
	}
	else if (this->sizeY && !this->sizeX)
	{
		this->sizeX = sizeX;
	}
	else
	{
		this->sizeX = sizeX;
		this->sizeY = sizeY;
	}

	Setup();
}

void MatrixControl(Matrix& matrix)
{
	cout << "How do you want initialize matrix: 1. By your own; 2. By random method: ";
	int answer;
	InputControl(answer);
	cout << endl;

	switch (answer)
	{
	case 1:
	{
		matrix.Initialize();
		break;
	}
	case 2:
	{
		int sizeX, sizeY;
		
		cout << "size: ";
		InputControl(sizeX);
		sizeY = sizeX;

		matrix.SetSize(sizeX, sizeY);
		matrix.RandomFill();
		break;
	}
	default:
		cout << "There are no operations with this index!" << endl;
		break;
	}
}

void InputControl(int& param)
{
	while (true)
	{
		rewind(stdin);
		cin >> param;

		if (cin.good())
		{
			break;
		}
		else if (cin.fail())
		{
			cout << "Not an integer! Try again: ";
			cin.clear();
		}
	}
}

//Add math string parsing 