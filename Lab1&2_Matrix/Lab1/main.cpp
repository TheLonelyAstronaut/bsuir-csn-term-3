#include <iostream>
#include "Matrix.h"
using namespace std;

int main()
{
	system("color 3");

	Matrix matrix1 = Matrix(2);
	Matrix matrix2 = Matrix(2);
	matrix1.RandomFill();
	matrix2.RandomFill();

	cout << "Matrix1 before increment" << endl;
	matrix1.Show();
	cout << "Matrix2 before increment" << endl;
	matrix2.Show();

	Matrix sum = ++matrix1++ + ++matrix2++;
	sum.Show();

	Matrix test1 = ++matrix1;
	Matrix test2 = matrix2++;
	sum = test1 + test2;

	cout << "Matrix1 after increment" << endl;
	matrix1.Show();
	cout << endl << "Test1 - Matrix1 after prefix increment" << endl ;
	test1.Show();
	cout << endl << "Matrix2 after increment" << endl;
	matrix2.Show();
	cout << endl << "Test2 = Matrix2 after postfix increment" << endl;
	test2.Show();
	cout << endl << "Sum" << endl;
	sum.Show();
	cout << endl << endl;

	do {
		cout << "====================================" << endl;
		cout << "Choose action: 1. Calculate sum; 2. Calculate composition: ";
		int answer;
		InputControl(answer);
		cout << endl;

		switch (answer)
		{
		case 1:
		{
			Matrix matrix1 = Matrix();
			Matrix matrix2 = Matrix();

			cout << "First matrix:" << endl << endl;;
			MatrixControl(matrix1);
			cout << endl;
			cout << "Second matrix:" << endl << endl;;
			MatrixControl(matrix2);

			cout << endl << "Matrix1: " << endl;
			matrix1.Show();

			cout << endl << "Matrix2: " << endl;
			matrix2.Show();

			Matrix sum;
			sum = matrix1 + ++ matrix2++;

			cout << endl << "Sum: " << endl;
			sum.Show();

			break;
		}
		case 2:
		{
			Matrix matrix1 = Matrix();
			Matrix matrix2 = Matrix();

			cout << "First matrix:" << endl << endl;
			MatrixControl(matrix1);
			cout << endl;
			cout << "Second matrix:" << endl << endl;
			MatrixControl(matrix2);

			cout << endl << "Matrix1: " << endl;
			matrix1.Show();

			cout << endl << "Matrix2: " << endl;
			matrix2.Show();

			Matrix composition;
			composition = matrix1 * matrix2;

			cout << endl << "Composition: " << endl;
			composition.Show();

			cout << endl;

			break;
		}
		default:
		{
			cout << "There are no operations with this index!" << endl;
			break;
		}
		}

		cout << endl << "Do you want to continue? (1/0): ";
		InputControl(answer);
		if (!answer) break;
		system("CLS");

	} while (true);
}