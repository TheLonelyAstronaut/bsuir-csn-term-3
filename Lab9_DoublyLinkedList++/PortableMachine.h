#pragma once
#include "ComputingMachine.h"
#include <iostream>
#include <string>

class PortableMachine abstract: public ComputingMachine
{
protected:
	int BatteryCapacity;
	bool IsTouchable;
public:
	PortableMachine(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, int _batterycapacity = 0, bool _istouchable = false) :
		ComputingMachine(_manufacturer, _cpu, _gpu, _ram),
		BatteryCapacity(_batterycapacity),
		IsTouchable(_istouchable) {}
	virtual ~PortableMachine() {}
	static void printHeader();
public:
	int GetBatteryCapacity() { return BatteryCapacity; }
	string GetIsTouchable() { return IsTouchable ? "Yes" : "No"; }
public:
	void SetBatteryCapacity(int _batterycapacity) { BatteryCapacity = _batterycapacity; }
	void SetIsTouchable(bool _istouchable) { IsTouchable = _istouchable; }
public:
	friend istream& operator>>(istream& in, PortableMachine& object);
	friend ostream& operator<<(ostream& out, const PortableMachine& object);
};

