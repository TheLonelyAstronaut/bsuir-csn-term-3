#pragma once
#include <iostream>
#include <string>
using namespace std;

class ComputingMachine abstract
{
protected:
	string Manufacturer, CPU, GPU;
	int RAM;
public:
	ComputingMachine(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0) : Manufacturer(_manufacturer), CPU(_cpu), GPU(_gpu), RAM(_ram) {}
	virtual ~ComputingMachine() {}
	static void printHeader();
public:
	string GetManufacturer() { return Manufacturer; }
	string GetCPU() { return CPU; }
	string GetGPU() { return GPU; }
	int GetRam() { return RAM; }
public:
	void SetManufacturer(string _manufacturer) { Manufacturer = _manufacturer; }
	void SetCPU(string _cpu) { CPU = _cpu; }
	void SetGPU(string _gpu) { GPU = _gpu; }
	void SetRAM(int _ram) { RAM = _ram; }
public:
	friend istream& operator>>(istream& in, ComputingMachine& object);
	friend ostream& operator<<(ostream& out, const ComputingMachine& object);
	friend bool operator==(const ComputingMachine& lhs, const ComputingMachine& rhs);
	friend bool operator!=(const ComputingMachine& lhs, const ComputingMachine& rhs);
};