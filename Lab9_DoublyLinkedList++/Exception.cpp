#include "Exception.h"
#include <string>
#include <iostream>
#include "ArgumentOutOfRangeException.h"
#include "InputSizeException.h"
#include "InvalidLanguageException.h"
using namespace std;

Exception::Exception(string _message) : message(_message) {}

Exception::~Exception() {}

string Exception::Message() { return message;  }

void Exception::isNumber(int& number, int maxValue, int minValue)
{
	while (true)
	{
		try
		{
			try
			{
				rewind(stdin);
				cin >> number;

				if (cin.fail() || cin.peek() != '\n')
					//cin.clear();
					throw Exception("Not a number!");
				if (number > maxValue || number < minValue)
					throw ArgumentOutOfRangeException("Number is out of range!", maxValue, minValue);

				if (cin.good())
				{
					break;
				}
			}
			catch (Exception ex)
			{
				cin.clear();
				rewind(stdin);
				cout << ex.Message() << endl;
				throw;
			}
		}
		catch (ArgumentOutOfRangeException ex)
		{
			cout << "Minimum: " << ex.MinimumValue() << " Maximum: " << ex.MaximumValue() << endl;
		}
		catch (...)
		{
			//cout << "Unhandeled exception!" << endl;
		}
	}
}

void Exception::isString(string& str, int maxLength, string language)
{
	while (true)
	{
		try
		{
			try
			{
				rewind(stdin);
				getline(cin, str);

				if (str.length() > maxLength) throw InputSizeException("Overflow!", maxLength);

				int leftBorder = 0, rightBorder = 0;

				leftBorder = 65;
				rightBorder = 122;

				for (int i = 0; i < str.length(); ++i)
					if ((str[i] > rightBorder || str[i] < leftBorder) && str[i] != ' ')
						throw InvalidLanguageException("Invalid Symbols!", "ENG");

				break;
			}
			catch (Exception ex)
			{
				cout << ex.Message() << endl;
				throw;
			}
		}
		catch (InputSizeException ex)
		{
			cout << "Maximum string size: " << ex.MaximumSize() << endl;
		}
		catch (InvalidLanguageException ex)
		{
			cout << "Use " << ex.Language() << endl;
		}
		catch (...)
		{
			//cout << "Unhandeled exception!" << endl;
		}
		
	}
}