#include "InputSizeException.h"

InputSizeException::InputSizeException( string _message, int _maxSize) : Exception(message), maxSize(_maxSize) {}

InputSizeException::~InputSizeException() {}

int InputSizeException::MaximumSize() { return maxSize;  }