#include "PortableMachine.h"
#include <iostream>
#include <iomanip>
#include "ArgumentOutOfRangeException.h"
using namespace std;

istream& operator>>(istream& in, PortableMachine& object)
{
	string exOnly;
	in >> static_cast<ComputingMachine&>(object);

	cout << "BatteryCapacity: ";
	Exception::isNumber(object.BatteryCapacity, 40000, 0);

	cout << "Touchable: ";
	Exception::isString(exOnly, 3);
	if (exOnly == "yes" || exOnly == "Yes") object.IsTouchable = true;
	else object.IsTouchable = false;

	return in;
}

ostream& operator<<(ostream& out, const PortableMachine& object)
{
	out << static_cast<const ComputingMachine&>(object);
	out << setw(20) << object.BatteryCapacity << setw(15) << (object.IsTouchable ? "Yes" : "No");
	return out;
}

void PortableMachine::printHeader()
{
	ComputingMachine::printHeader();
	cout << setw(20) << "Battery Capacity" << setw(15) << "Touchable";
}