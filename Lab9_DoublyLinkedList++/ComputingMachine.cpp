#include "ComputingMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
#include "ArgumentOutOfRangeException.h"
using namespace std;

istream& operator>>(istream& in, ComputingMachine& object)
{
	string exOnly;

	cout << "Manufacturer: ";
	Exception::isString(object.Manufacturer, 80);

	cout << "CPU: ";
	Exception::isString(object.CPU, 80);

	cout << "GPU: ";
	Exception::isString(object.GPU, 80);

	cout << "RAM: ";
	Exception::isNumber(object.RAM, 256, 0);


	return in;
}

ostream& operator<<(ostream& out, const ComputingMachine& object)
{
	out << setw(15) << object.Manufacturer;
	out << setw(15) << object.CPU;
	out << setw(15) << object.GPU;
	out << setw(15) << object.RAM;
	return out;
}

void ComputingMachine::printHeader()
{
	cout << setw(15) << left << "Manufacturer"
		<< setw(15) << "CPU"
		<< setw(15) << "GPU"
		<< setw(15) << "RAM";
}

bool operator==(const ComputingMachine& lhs, const ComputingMachine& rhs)
{
	if (lhs.Manufacturer == rhs.Manufacturer && lhs.CPU == rhs.CPU
		&& lhs.GPU == rhs.GPU && lhs.RAM == rhs.RAM)
		return true;
	else return false;
}

bool operator!=(const ComputingMachine& lhs, const ComputingMachine& rhs)
{
	if (lhs == rhs) return false;
	else return true;
}