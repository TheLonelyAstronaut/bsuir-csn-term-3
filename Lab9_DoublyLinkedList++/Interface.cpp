#include "Interface.h"
#include "PC.h"
#include "Exception.h"
#include <iostream>
#include <type_traits>
#include "Algorithm.h"
#include "Algorithm.cpp"
using namespace std;

template<class T>
Interface<T>::Interface()
{

}

template<class T>
Interface<T>::~Interface()
{
	object.clear();
}

template<class T>
void Interface<T>::Menu()
{
	cout << "Choose action: " << endl
		<< "1. Add element to the front" << endl
		<< "2. Add element to the back" << endl
		<< "3. Pop element from the front" << endl
		<< "4. Pop element from the back" << endl
		<< "5. Clear list" << endl
		<< "6. Sort"<< endl
		<< "7. Is sorted?" << endl
		<< "8. Remove by index" << endl
		<< "9. Remove by value" << endl
		<< "10. Search element" << endl
		<< "11. Print to console" << endl
		<< "12. Show menu" << endl
		<< "0. Exit" << endl;
}

template<class T>
void Interface<T>::Show()
{
	int answer;
	Menu();

	do {
		cout << "Your choose: ";
		Exception::isNumber(answer, 12, 0);

		switch (answer)
		{
		case 0:
			break;
		case 1:
		{
			T toList = T();
			cin >> toList;
			object.push_front(toList);
			cout << "Success!" << endl;
			break;
		}
		case 2:
		{
			T toList = T();
			cin >> toList;
			object.push_back(toList);
			cout << "Success!" << endl;
			break;
		}
		case 3:
		{
			object.pop_front();
			cout << "Success!" << endl;
			break;
		}
		case 4:
		{
			object.pop_back();
			cout << "Success!" << endl;
			break;
		}
		case 5:
		{
			object.clear();
			cout << "Success!" << endl;
			break;
		}
		case 6:
		{
			Sorting();
			break;
		}
		case 7:
		{
			isSorted();
			break;
		}
		case 8:
		{
			cout << "Enter index: ";
			int index;
			Exception::isNumber(index, object.size()-1, 0);
			//T test = object[index];
			//object.remove(test);
			cout << "Success!" << endl;
			break;
		}
		case 9:
		{
			T removable;
			cin >> removable;
			object.remove(removable);
			cout << "Success!" << endl;
			break;
		}
		case 10:
		{
			T searchable;
			cin >> searchable;
			//int index = object.search(searchable);
			//cout << (index == -1 ? "Item not found!" : "Index: " + to_string(index)) << endl;
			break;
		}
		case 11:
		{
			T::printHeader();
			cout << endl;
			for (auto it = object.begin(); it != object.end(); ++it)
			{
				cout << (*it) << endl;
			}

			cout << endl;

			break;
		}
		case 12:
		{
			Menu();
			break;
		}
		default:
			break;
		}
	} while (answer);
}

template<class T>
bool ManufacturerComparator(T lhs, T rhs)
{
	if (lhs.GetManufacturer() > rhs.GetManufacturer()) return true;
	else return false;
}

template<class T>
bool CPUComparator(T lhs, T rhs)
{
	if (lhs.GetCPU() > rhs.GetCPU()) return true;
	else return false;
}

template<class T>
bool GPUComparator(T lhs, T rhs)
{
	if (lhs.GetGPU() > rhs.GetGPU()) return true;
	else return false;
}

template<class T>
bool RAMComparator(T lhs, T rhs)
{
	if (lhs.GetRam() > rhs.GetRam()) return true;
	else return false;
}

template <class T>
void Interface<T>::Sorting()
{
	int answer, type;
	bool (*comparator)(T, T) = nullptr;

	cout << "Choose parameter: " << endl
		<< "1. Manufacturer" << endl
		<< "2. CPU" << endl
		<< "3. GPU" << endl
		<< "4. RAM" << endl
		<< "0. Exit" << endl;

	Exception::isNumber(answer, 4, 0);

	cout << "Sorting type:" << endl << "0. Descending" << endl << "1. Ascending" << endl;
	

	Exception::isNumber(type, 1, 0);

	switch (answer)
	{
	case 0:
	{
		break;
	}
	case 1:
	{
		comparator = &ManufacturerComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	case 2:
	{
		comparator = &CPUComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	case 3:
	{
		comparator = &GPUComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	case 4:
	{
		comparator = &RAMComparator;
		Algorithm::sort(object.begin(), --object.end(), comparator, type);
		break;
	}
	default:
		break;
	}
}

template <class T>
void Interface<T>::isSorted()
{
	if (!object.size()) {
		cout << "Empty list!" << endl;
		return;
	}

	int answer, type;
	bool (*comparator)(T, T) = nullptr;

	cout << "Choose parameter: " << endl
		<< "1. Manufacturer" << endl
		<< "2. CPU" << endl
		<< "3. GPU" << endl
		<< "4. RAM" << endl
		<< "0. Exit" << endl;

	Exception::isNumber(answer, 4, 0);

	cout << "Sorting type:" << endl << "0. Descending" << endl << "1. Ascending" << endl;


	Exception::isNumber(type, 1, 0);

	switch (answer)
	{
	case 0:
	{
		break;
	}
	case 1:
	{
		comparator = &ManufacturerComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	case 2:
	{
		comparator = &CPUComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	case 3:
	{
		comparator = &GPUComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	case 4:
	{
		comparator = &RAMComparator;
		bool result = Algorithm::isSorted(object.begin(), --object.end(), comparator, type);
		cout << (result ? "Sorted!" : "Unsorted!") << endl;
		break;
	}
	default:
		break;
	}
}

