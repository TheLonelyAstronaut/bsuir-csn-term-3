#pragma once
#include "ListIterator.h"

class Algorithm
{
public:
	template <class T>
	static void sort(ListIterator<T> begin, ListIterator<T> end, bool (*comparator)(T, T), bool ascending = true);

	template <class T>
	static bool isSorted(ListIterator<T> begin, ListIterator<T> end, bool (*comparator)(T, T), bool ascending = true);

	template <class T>
	static ListIterator<T>& search(ListIterator<T>& begin, ListIterator<T>& end, T value);

	template <class T>
	static void iteratorSwap(ListIterator<T>& lhs, ListIterator<T>& rhs);
};
