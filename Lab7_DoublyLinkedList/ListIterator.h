#pragma once
#include "ConstListIterator.h"
#include "Node.h";

template <class T>
class ListIterator : public ConstListIterator<T>
{
public:
	ListIterator();
	T& operator*();
	const T& operator*() const;

	// increment/decrement operators
	ListIterator<T>& operator++();
	ListIterator<T> operator++(int);
	ListIterator<T>& operator--();
	ListIterator<T> operator--(int);
	bool operator>(const ListIterator<T>&);
	bool operator<(const ListIterator<T>&);
protected:
	ListIterator(typename Node<T>* p);
	friend class DoublyLinkedList<T>;
};

