#pragma once
#include "Node.h"

template <class T>
class DoublyLinkedList;

template <class T>
class ConstListIterator
{
public:
	ConstListIterator();           // default zero parameter constructor
	const T& operator*() const; // operator*() to return element

	// increment/decrement operators
	ConstListIterator& operator++();
	ConstListIterator operator++(int);
	ConstListIterator& operator--();
	ConstListIterator operator--(int);

	// comparison operators
	bool operator==(const ConstListIterator& rhs) const;
	bool operator!=(const ConstListIterator& rhs) const;

protected:
	Node<T>* current;                  // pointer to Node<T> in List
	T& retrieve() const;           // retrieve the element refers to
	ConstListIterator(Node<T>* p);        // protected constructor

	friend class DoublyLinkedList<T>;
};

