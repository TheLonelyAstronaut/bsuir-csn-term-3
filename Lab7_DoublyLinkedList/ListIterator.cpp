#include "ListIterator.h"
#include <iostream>

template<class T>
ListIterator<T>::ListIterator()  // constructor of derived iterator class
{
}

template<class T>
T& ListIterator<T>::operator*() // this operator* is the one will return a modifiable
//reference of the data in current Node
{
	return this->current->data;
}

template<class T>
const T& ListIterator<T>::operator*() const
{
	return this->retrieve();
}
// operators needed again
template<class T>
ListIterator<T>& ListIterator<T>::operator++()
{  //this->current to call base's protected data
	this->current = this->current->next; 	return *this;
}

template<class T>
ListIterator<T> ListIterator<T>::operator++(int)
{
	auto cpy = *this;
	this->current = this->current->next; 	return cpy;
}

template<class T>
ListIterator<T>& ListIterator<T>::operator--()
{
	this->current = this->current->prev;	return *this;
}

template<class T>
ListIterator<T> ListIterator<T>::operator--(int)
{
	auto cpy = *this;
	this->current = this->current->prev;	return *this;
}

template<class T>
ListIterator<T>::ListIterator(Node<T>* p) : ConstListIterator<T>{ p }
{
}

template<class T>
bool ListIterator<T>::operator>(const ListIterator<T>& element)
{
	Node<T>* pointer = element.current;

	//std::cout << "===========================" << std::endl;
	for (; pointer != this->current && pointer->next; pointer = pointer->next);
	{
		//std::cout << "Here))0 ";
		//std::cout << pointer->data << std::endl;
	}
	//std::cout << "===========================" << std::endl;

	if (pointer->next)
	{
		return true;
	}
	else return false;
}

template<class T>
bool ListIterator<T>::operator<(const ListIterator<T>& element)
{
	if ((*this) > element) return false;
	else return true;
}