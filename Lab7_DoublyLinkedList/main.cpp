#include <iostream>
#include "DoublyLinkedList.h"
#include "DoublyLinkedList.cpp"
#include <string>

using namespace std;

int main()
{
	DoublyLinkedList<string> test = DoublyLinkedList<string>();

	for (int i = 0; i < 10; ++i) test.push_back("test " + to_string(10-i));

	cout << test << endl;
	test.reverse();
	cout << test << endl;
	test.remove("test 5");
	cout << test << endl;
	test.pop_back();
	cout << test << endl;
	test.pop_front();
	cout << test << endl;
	test[5] = "oaoao";
	cout << (test != test) << endl;

	return 0;
}