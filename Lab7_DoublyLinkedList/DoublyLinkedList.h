#pragma once
#include <iostream>
#include "Node.h"

template <class T>
class DoublyLinkedList
{
public:
    // constructor, desctructor, copy constructor
    DoublyLinkedList(); // default zero parameter constructor
    DoublyLinkedList(const DoublyLinkedList &rhs); // copy constructor
    DoublyLinkedList(DoublyLinkedList && rhs); // move constructor
    // num elements with value of val
    explicit DoublyLinkedList(int num, const T& val = T{});
    // constructs with elements [start, end)

    ~DoublyLinkedList(); // destructor

    // copy assignment operator
    const DoublyLinkedList& operator=(const DoublyLinkedList &rhs);
    // move assignment operator
    DoublyLinkedList & operator=(DoublyLinkedList && rhs);

    // member functions
    int size() const; // number of elements
    bool empty() const; // check if DoublyLinkedList is empty
    void clear(); // delete all elements
    void reverse(); // reverse the order of the elements

    T &front(); // reference to the first element
    const T& front() const;
    T &back(); // reference to the last element
    const T & back() const;

    void push_front(const T & val); // insert to the beginning
    void push_back(const T & val); // insert to the end
    void pop_front(); // delete first element
    void pop_back(); // delete last element
    void remove(const T &val); // remove all elements with value = val
	void insert(const T& val, int index);
	void erase(Node<T>* pointer);
	int search(const T& val);

	T& operator[](int index);
	
	inline friend std::ostream& operator<<(std::ostream& os, const DoublyLinkedList<T>& l)
	{
		Node<T>* element = l.head->next;
		for (; element != l.tail; element = element->next) os << element->data << ' ';
		return os;
	}

	inline friend bool operator==(const DoublyLinkedList<T>& lhs, const DoublyLinkedList<T>& rhs)
	{
		if (lhs.size() == rhs.size())
		{
			Node<T>* element1 = lhs.head->next;
			Node<T>* element2 = rhs.head->next;

			for (; element1 != lhs.tail; element1=element1->next, element2=element2->next)
				if (element1->data != element2->data)
					return false;

			return true;
		}
		else
			return false;
	}

	inline friend bool operator!=(const DoublyLinkedList<T>& lhs, const DoublyLinkedList<T>& rhs)
	{
		return !(lhs == rhs);
	}
    
private:
    int theSize; // number of elements
    Node<T> *head; // head Node<T>
    Node<T> *tail; // tail Node<T>

    void init(); // initialization	
};

//#include "DoublyLinkedList.cpp"