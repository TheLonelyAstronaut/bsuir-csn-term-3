#include "ConstListIterator.h"

template<class T>
ConstListIterator<T>::ConstListIterator() : current{ nullptr }
{
}
// operator* returns the element (to be retrieve later)  (protected)
template<class T>
const T& ConstListIterator<T>::operator*() const
{
	return retrieve();
}
// operators various
template<class T>
ConstListIterator<T>& ConstListIterator<T>::operator++()
{  // increment operator, pointer to the next nodo
	current = current->next;    return *this;
	// basically returns the instance of this class, * = dereference, and "this" is the pointer to the instance
}

template<class T>
ConstListIterator<T> ConstListIterator<T>::operator++(int)  // ++(int) postfix increment
{ // return a copy of original object and then increment by 1
	auto cpy = *this;  //using dereferencing and member access.
	current = current->next; 	return cpy;
}

template<class T>
ConstListIterator<T>& ConstListIterator<T>::operator--() // decrement
{
	current = current->prev; 	return *this;
}

template<class T>
typename ConstListIterator<T> ConstListIterator<T>::operator--(int) //--(int) posfix decrement
{
	auto cpy = *this;
	current = current->prev;	return *this;
}

// comparisons
template<class T>
bool ConstListIterator<T>::operator==(const ConstListIterator<T>& rhs) const
{
	return current == rhs.current; // refer to the same element? ==
}

template<class T>
bool ConstListIterator<T>::operator!=(const ConstListIterator<T>& rhs) const
{
	return current != rhs.current; // they do not refer to the same element? !=
}
// protected constructor
template<class T>
ConstListIterator<T>::ConstListIterator(typename Node<T>* p) : current{ p }
{
}

template<class T>
T& ConstListIterator<T>::retrieve() const
{
	return current->data;
}