#include "Monoblock.h"
#include "ComputingMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include "InputSizeException.h"
using namespace std;

string Monoblock::GetTouch()
{
	return this->Touch;
}

void Monoblock::SetTouch(string _touch)
{
	memset(this->Touch, 0, _STRING_MAX_LENGTH);
	strcpy_s(this->Touch, _touch.length() + 1, _touch.c_str());
}

istream& operator>>(istream& in, Monoblock& object)
{
	in >> static_cast<StationaryMachine&>(object);
	cout << "Touch: ";
	string exOnly;
	Exception::isString(exOnly);
	object.SetTouch(exOnly);

	return in;
}

ostream& operator<<(ostream& out, const Monoblock& object)
{
	out << static_cast<const StationaryMachine&>(object);
	out << setw(15) << object.Touch << endl;
	return out;
}

void Monoblock::printHeader()
{
	StationaryMachine::printHeader();
	cout << setw(15) << "Touch";
}

fstream& operator<<(fstream& out, const Monoblock& object)
{
	out << static_cast<const StationaryMachine&>(object);
	out << " Touch:\"" << object.Touch << "\"; }" << endl;
	return out;
}

fstream& operator>>(fstream& in, Monoblock& object)
{
	string textedObject;
	getline(in, textedObject);

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetManufacturer(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetCPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetGPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetRAM(stoi(textedObject.substr(0, textedObject.find("\""))));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetPowerSupply(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetTouch(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	return in;
}