#include "StationaryMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
#include <fstream>
using namespace std;

istream& operator>>(istream& in, StationaryMachine& object)
{
	in >> static_cast<ComputingMachine&>(object);
	string exOnly;
	cout << "PowerSupply: ";
	Exception::isString(exOnly);
	object.SetPowerSupply(exOnly);

	return in;
}

ostream& operator<<(ostream& out,const StationaryMachine& object)
{
	out << static_cast<const ComputingMachine&>(object);
	string ps = object.PowerSupply;
	out << setw(15) << ps;
	return out;
}

void StationaryMachine::printHeader()
{
	ComputingMachine::printHeader();
	cout << setw(15) << "Power Supply";
}

bool operator==(const StationaryMachine& lhs, const StationaryMachine& rhs)
{
	if (static_cast<const ComputingMachine&>(lhs) == static_cast<const ComputingMachine&>(rhs))
		if (lhs.WithDisplay == rhs.WithDisplay && !strcmp(lhs.PowerSupply, rhs.PowerSupply))
			return true;
		else
			return false;
	else
		return false;
}

bool operator!=(const StationaryMachine& lhs, const StationaryMachine& rhs)
{
	if (lhs == rhs) return false;
	else return true;
}

fstream& operator<<(fstream& out, const StationaryMachine& object)
{
	out << static_cast<const ComputingMachine&>(object);
	out << " Power Supply:\"" << object.PowerSupply << "\";";
	return out;
}