#pragma once
#include "PortableMachine.h"

class Laptop : public PortableMachine
{
protected:
	char KeyboardType[_STRING_MAX_LENGTH];
public:
	Laptop(string _manufacturer = "", string _cpu = "", string _gpu = "", int _ram = 0, int _batterycapacity = 0, string _keyboardtype = "") :
		PortableMachine(_manufacturer, _cpu, _gpu, _ram, _batterycapacity, true)
	{
		SetKeyboardType(_keyboardtype);
	}
	~Laptop() {}
	static void printHeader();
public:
	string GetKeyboardType();
public:
	void SetKeyboardType(string _keyboardtype);
public:
	friend istream& operator>>(istream& in, Laptop& object);
	friend fstream& operator>>(fstream& in, Laptop& object);
	friend ostream& operator<<(ostream& out, const Laptop& object);
	friend fstream& operator<<(fstream& out, const Laptop& object);
};

