#pragma once
#include <string>
#include "Exception.h"
using namespace std;

class ArgumentOutOfRangeException : public Exception
{
protected:
	int maxValue, minValue;
public:
	ArgumentOutOfRangeException(string _message = "", int _maxValue = 0, int _minValue = 0);
	virtual ~ArgumentOutOfRangeException();
	int MaximumValue();
	int MinimumValue();
	virtual string Message();
};

