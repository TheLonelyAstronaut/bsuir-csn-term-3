#pragma once
#include "ListIterator.h"
#include <vector>
#include <type_traits>

class Algorithm
{
public:
	template <class T>
	static void sort(ListIterator<T> begin, ListIterator<T> end, bool (*comparator)(T, T), bool ascending = true);

	template <class T>
	static bool isSorted(ListIterator<T> begin, ListIterator<T> end, bool (*comparator)(T, T), bool ascending = true);

	template <class T>
	static void iteratorSwap(ListIterator<T>& lhs, ListIterator<T>& rhs);

	template <class T, class K>
	static DoublyLinkedList<T> search(ListIterator<T> begin, ListIterator<T> end, K value, bool (*comparator)(T, K) = nullptr);
	//static_assert(std::is_base_of<double, K>::value, "K must inherit from list");
};
