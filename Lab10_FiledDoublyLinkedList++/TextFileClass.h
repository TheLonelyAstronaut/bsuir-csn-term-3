#pragma once
#include "FileClass.h"
#include "DoublyLinkedList.h"

template<class T>
class TextFileClass : public FileClass
{
public:
	TextFileClass(string _filename = "", int _bitMask = fstream::out | fstream::in) : FileClass(_filename, _bitMask) {}
	~TextFileClass();
	bool Open();
	void Write(DoublyLinkedList<T> _toFile);
	void Read(DoublyLinkedList<T>& _fromFile);
	bool Close();
	bool isEnded();
	bool isOpened();
};

