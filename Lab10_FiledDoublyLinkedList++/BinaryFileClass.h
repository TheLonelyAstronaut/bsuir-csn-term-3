#pragma once
#include "FileClass.h"
#include "DoublyLinkedList.h"

template <class T>
class BinaryFileClass : public FileClass
{
public:
	BinaryFileClass(string _filename = "", int _bitMask = fstream::out | fstream::in) : FileClass(_filename, _bitMask) {}
	~BinaryFileClass();
	bool Open();
	void Write(DoublyLinkedList<T> _toFile);
	void Read(DoublyLinkedList<T>&);
	bool Close();
	bool isEnded();
	bool isOpened();
};

