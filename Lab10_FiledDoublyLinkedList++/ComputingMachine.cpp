#include "ComputingMachine.h"
#include <iostream>
#include <iomanip>
#include <string>
#include "InputSizeException.h"
#include "ArgumentOutOfRangeException.h"
#include <fstream>
using namespace std;

istream& operator>>(istream& in, ComputingMachine& object)
{
	string exOnly;

	cout << "Manufacturer: ";
	Exception::isString(exOnly, 80);
	object.SetManufacturer(exOnly);

	cout << "CPU: ";
	Exception::isString(exOnly, 80);
	object.SetCPU(exOnly);

	cout << "GPU: ";
	Exception::isString(exOnly, 80);
	object.SetGPU(exOnly);

	cout << "RAM: ";
	Exception::isNumber(object.RAM, 256, 0);


	return in;
}

ostream& operator<<(ostream& out, const ComputingMachine& object)
{
	string _manufacturer = object.Manufacturer;
	string _cpu = object.CPU;
	string _gpu = object.GPU;

	out << setw(15) << _manufacturer;
	out << setw(15) << _cpu;
	out << setw(15) << _gpu;
	out << setw(15) << object.RAM;
	return out;
}

void ComputingMachine::printHeader()
{
	cout << setw(15) << left << "Manufacturer"
		<< setw(15) << "CPU"
		<< setw(15) << "GPU"
		<< setw(15) << "RAM";
}

bool operator==(const ComputingMachine& lhs, const ComputingMachine& rhs)
{
	if (!strcmp(lhs.Manufacturer, rhs.Manufacturer) && !strcmp(lhs.CPU, rhs.CPU)
		&& !strcmp(lhs.GPU, rhs.GPU) && lhs.RAM == rhs.RAM)
		return true;
	else return false;
}

bool operator!=(const ComputingMachine& lhs, const ComputingMachine& rhs)
{
	if (lhs == rhs) return false;
	else return true;
}

fstream& operator<<(fstream& out, const ComputingMachine& object)
{
	out << "{ Manufacturer:\"" << object.Manufacturer << "\"; CPU:\""
		<< object.CPU << "\"; GPU:\"" << object.GPU << "\"; RAM:\"" << object.RAM << "\";";
	return out;
}