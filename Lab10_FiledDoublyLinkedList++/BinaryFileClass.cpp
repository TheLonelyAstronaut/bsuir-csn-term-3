#include "BinaryFileClass.h"
#include <iostream>
#include <string>
#include <iosfwd>
#include <fstream>
#include "DoublyLinkedList.h"

using namespace std;

template <class T>
BinaryFileClass<T>::~BinaryFileClass()
{
	if (this->isOpened()) 
	{
		this->Close();
	}
}

template<class T>
bool BinaryFileClass<T>::Open()
{
	try
	{
		fileStream.open(filename, bitMask | ios::binary);
		
		if (!this->isOpened())
		{
			fileStream.open(filename, fstream::out | fstream::binary);
			fileStream.close();
			fileStream.open(filename, bitMask | fstream::binary);
		}

		if (fileStream.is_open()) return true;
		else return false;
	}
	catch (...)
	{
		return false;
	}
}

template <class T>
bool BinaryFileClass<T>::Close()
{
	if (fileStream.is_open())
	{
		fileStream.close();
		if (fileStream.is_open())
			return false;
		else
			return true;
	}
	return true;
}

template<class T>
bool BinaryFileClass<T>::isEnded()
{
	int realPos = fileStream.tellg();
	fileStream.seekg(0, fstream::end);
	int eofPos = fileStream.tellg();
	fileStream.seekg(realPos, fstream::beg);

	if (realPos == eofPos) return true;
	else return false;
}

template<class T>
bool BinaryFileClass<T>::isOpened()
{
	if (fileStream.is_open()) return true;
	else return false;
}

template<class T>
void BinaryFileClass<T>::Write(DoublyLinkedList<T> _toFile)
{
	if (!fileStream.is_open()) return;

	auto it = _toFile.begin();

	for(; it != _toFile.end(); ++it)
		fileStream.write(reinterpret_cast<char*>(&(*it)), sizeof(T));
}

template<class T>
void BinaryFileClass<T>::Read(DoublyLinkedList<T>& list)
{
	SeekG(0);

	while (!this->isEnded())
	{
		try {
			T readable;
			fileStream.read(reinterpret_cast<char*>(&readable), sizeof(T));
			list.push_back(readable);
		}
		catch (...) {
			cout << "Gotcha" << endl;
		}
	}
}