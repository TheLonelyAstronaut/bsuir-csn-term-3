#pragma once
#include "Exception.h"

class InputSizeException : public Exception
{
protected:
	int maxSize;
public:
	InputSizeException(string _message = "", int _maxSize = 0);
	virtual ~InputSizeException();
	int MaximumSize();
};

