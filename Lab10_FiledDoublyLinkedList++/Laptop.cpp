#include "Laptop.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include "InputSizeException.h"
using namespace std;

string Laptop::GetKeyboardType()
{
	return this->KeyboardType;
}

void Laptop::SetKeyboardType(string _KeyboardType)
{
	memset(this->KeyboardType, 0, _STRING_MAX_LENGTH);
	strcpy_s(this->KeyboardType, _KeyboardType.length() + 1, _KeyboardType.c_str());
}

istream& operator>>(istream& in, Laptop& object)
{
	in >> static_cast<PortableMachine&>(object);
	cout << "KeyboardType: ";
	string exOnly;
	Exception::isString(exOnly);
	object.SetKeyboardType(exOnly);

	return in;
}

ostream& operator<<(ostream& out, const Laptop& object)
{
	out << static_cast<const PortableMachine&>(object);
	string _kt = object.KeyboardType;
	out << setw(15) << _kt << endl;
	return out;
}

void Laptop::printHeader()
{
	PortableMachine::printHeader();
	cout << setw(15) << "Keyboard Type";
}

fstream& operator<<(fstream& out, const Laptop& object)
{
	out << static_cast<const PortableMachine&>(object);
	out << " KeyboardType:\"" << object.KeyboardType << "\"; }" << endl;
	return out;
}

fstream& operator>>(fstream& in, Laptop& object)
{
	string textedObject;
	getline(in, textedObject);

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetManufacturer(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetCPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetGPU(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetRAM(stoi(textedObject.substr(0, textedObject.find("\""))));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetBatteryCapacity(stoi(textedObject.substr(0, textedObject.find("\""))));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());
	object.SetKeyboardType(textedObject.substr(0, textedObject.find("\"")));
	textedObject.erase(0, textedObject.substr(0, textedObject.find("\"") + 1).length());

	return in;
}