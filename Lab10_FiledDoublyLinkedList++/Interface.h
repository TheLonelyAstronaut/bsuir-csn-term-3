#pragma once
#include "DoublyLinkedList.h"
#include "DoublyLinkedList.cpp"

template<class T>
class Interface
{
private:
	DoublyLinkedList<T> object = DoublyLinkedList<T>();
	void Menu();
	void Sorting();
	void isSorted();

	void Search();
	void Print(DoublyLinkedList <T>);
	void BinaryFile();
	void TextFile();

public:
	Interface();
	~Interface();
	void Show();
};

