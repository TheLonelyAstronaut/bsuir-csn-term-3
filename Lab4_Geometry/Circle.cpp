#include "Circle.h"
#include <iostream>
#include <map>
#include <sstream>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

Point* Circle::GetPoints()
{
	Point* info = new Point[2];
	info[0] = A;
	info[1] = B;
	return info;
}

void Circle::Show()
{
	ostringstream strs;
	strs << A.x; 
	cout << "Shape type: Circle" << endl << "Center cords: " + strs.str() + ", ";
	strs.str("");
	strs << A.y;
	cout << strs.str() << endl << "Radius: ";
	strs.str("");
	strs << B.y-A.y;
	cout << strs.str() << endl << "Area: " << GetArea() << endl << "Perimeter: " << GetPerimeter() << endl << endl;
}

double Circle::GetArea()
{
	return M_PI * (B.y - A.y) * (B.y - A.y);
}

double Circle::GetPerimeter()
{
	return M_PI * 2 * (B.y - A.y);
}