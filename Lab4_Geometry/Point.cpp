#include <math.h>
#include "Point.h"

double Point::GetSideLength(Point A, Point B)
{
	return sqrt((double)powf((A.x - B.x), 2) + (double)powf((A.y - B.y), 2));
}