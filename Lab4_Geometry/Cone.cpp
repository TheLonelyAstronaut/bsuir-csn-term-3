#include <map>
#include <iostream>
#include "Cone.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <sstream>

using namespace std;

Point* Cone::GetPoints()
{
	Point* info = new Point[3];
	info[0] = A;
	info[1] = B;
	info[2] = C;
	return info;
}

void Cone::Show()
{
	ostringstream strs;
	strs << A.x;
	cout << "Shape type: Cone" << endl << "Center cords: " + strs.str() + ", ";
	strs.str("");
	strs << A.y;
	cout << strs.str() + ", ";
	strs.str("");
	strs << A.z;
	cout << strs.str() << endl << "Radius: ";
	strs.str("");
	strs << B.x-A.x;
	cout << strs.str() << endl << "Height: " << C.z - A.z << endl;
	cout << "Area: " << GetArea() << endl << "Volume: " << GetVolume() << endl << endl;
}

double Cone::GetArea()
{
	return M_PI * (B.x - A.x) * (B.x-A.x + sqrt(powf((B.x - A.x),2) + powf((C.z-A.z), 2)));
}

double Cone::GetVolume()
{
	return M_PI * (C.z-A.z) * (B.x - A.x) * (B.x - A.x)/ 3;
}
