#pragma once

class Point
{
public:
	double x, y, z;
public:
	Point(double _x = 0, double _y = 0, double _z = 0) : x(_x), y(_y), z(_z) {}
	static double GetSideLength(Point, Point);
};