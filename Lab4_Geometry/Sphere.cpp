#include <map>
#include <iostream>
#include "Sphere.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <sstream>

using namespace std;

Point* Sphere::GetPoints()
{
	Point* info = new Point[2];
	info[0] = A;
	info[1] = B;
	return info;
}

void Sphere::Show()
{
	ostringstream strs;
	strs << A.x;
	cout << "Shape type: Sphere" << endl << "Center cords: " + strs.str() + ", ";
	strs.str("");
	strs << A.y;
	cout << strs.str() + ", ";
	strs.str("");
	strs << A.z;
	cout << strs.str() << endl << "Radius: ";
	strs.str("");
	strs << B.z - A.z;
	cout << strs.str() << endl;
	cout << "Area: " << GetArea() << endl << "Volume: " << GetVolume() << endl << endl;
}

double Sphere::GetArea()
{
	return M_PI * 4 * (B.z - A.z) * (B.z - A.z);
}

double Sphere::GetVolume()
{
	return M_PI * 4 * pow((B.z - A.z), 3) / 3;
}
