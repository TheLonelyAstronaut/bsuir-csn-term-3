#pragma once
#include "TwoDimensionShape.h"
#include <iostream>
#include "Point.h"
#include <map>

class Triangle : public TwoDimensionShape
{
protected:
	Point C;
public:
	Triangle(double x1 = 0,
	double y1 = 0,
	double x2 = 0,
	double y2 = 0,
	double x3 = 0,
	double y3 = 0) : TwoDimensionShape(x1, y1, x2, y2), C(Point(x3, y3)) { std::cout << "Triangle Constructor" << std::endl << std::endl;}
	Triangle(Point _A, Point _B, Point _C) : TwoDimensionShape(_A, _B), C(_C) { std::cout << "Triangle Constructor" << std::endl << std::endl; }
	~Triangle() { std::cout << "Triangle Destructor" << std::endl; }
	Point* GetPoints();
	void Show();
	double GetArea();
	double GetPerimeter();
};