#pragma once
#include <iostream>
#include <map>
#include "Point.h"

class Shape abstract
{
public:
	Shape() { std::cout << "Shape Constructor" << std::endl; } 
	virtual ~Shape() { std::cout << "Shape Destructor" << std::endl << std::endl; }
	virtual Point* GetPoints() = 0;
	virtual void Show() = 0;
	virtual double GetArea() = 0;
};