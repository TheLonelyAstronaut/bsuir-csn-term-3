#pragma once
#include "Shape.h"
#include "Point.h"
#include <iostream>

class ThreeDimensionShape abstract : public Shape
{
protected:
	Point A, B;
public:
	ThreeDimensionShape(double x = 0, double y = 0, double z = 0, double x1 = 0, double y1 = 0, double z1 = 0) : A(Point(x, y, z)) { std::cout << "ThreeDimensionShape Constructor" << std::endl; }
	ThreeDimensionShape(Point pointA, Point pointB) : A(pointA), B(pointB) { std::cout << "ThreeDimensionShape Constructor" << std::endl; }
	virtual ~ThreeDimensionShape() { std::cout << "ThreeDimensionShape Destructor" << std::endl; };
	virtual double GetVolume() = 0;
};