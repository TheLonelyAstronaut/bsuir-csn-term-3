#pragma once
#include <iostream>
#include "Shape.h"
#include "Point.h"
#include <map>
#include <string>

class TwoDimensionShape abstract : public Shape
{
protected:
	Point A, B;
public:
	TwoDimensionShape(double x = 0, double y = 0, double x1 = 0, double y1 = 0) : A(Point(x, y)), B(Point(x1,y1)) { std::cout << "TwoDimensionShape Constructor" << std::endl; }
	TwoDimensionShape(Point pointA, Point pointB) : A(pointA), B(pointB)  { std::cout << "TwoDimensionShape Constructor" << std::endl; }
	~TwoDimensionShape() { std::cout << "TwoDimensionShape Destructor" << std::endl; };
	virtual double GetPerimeter() = 0;
};