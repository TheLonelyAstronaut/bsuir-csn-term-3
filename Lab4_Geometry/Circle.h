#pragma once
#include "TwoDimensionShape.h"
#include <string>
#include <map>
#include <iostream>

class Circle : public TwoDimensionShape
{
public:
	Circle(double x = 0, double y = 0, double r = 0) : TwoDimensionShape(x,y, x, y+r) { std::cout << "Circle Constructor" << std::endl << std::endl; }
	Circle(Point point, double r = 0) : TwoDimensionShape(point, Point(r, point.y+r)) { std::cout << "Circle Constructor" << std::endl << std::endl; }
	~Circle() { std::cout << "Circle Destructor" << std::endl; };
	Point* GetPoints();
	void Show();
	double GetArea();
	double GetPerimeter();
};