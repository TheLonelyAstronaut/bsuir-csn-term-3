#pragma once
#include <map>
#include <iostream>
#include "ThreeDimensionShape.h"

class Sphere : public ThreeDimensionShape
{
public:
	Sphere(double x = 0, double y = 0, double z = 0, double r = 0) : ThreeDimensionShape(x,y,z,x,y,z+r) { std::cout << "Sphere Constructor" << std::endl << std::endl; }
	Sphere(Point point, double r = 0) : ThreeDimensionShape(point, Point(point.x, point.y, point.z+r)) { std::cout << "Sphere Constructor" << std::endl << std::endl; }
	~Sphere() { std::cout << "Sphere Destructor" << std::endl; }
	Point* GetPoints();
	void Show();
	double GetArea();
	double GetVolume();
};