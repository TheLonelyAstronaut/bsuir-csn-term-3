#pragma once
#include <map>
#include <iostream>
#include "ThreeDimensionShape.h"

class Cone : public ThreeDimensionShape
{
protected:
	Point C;
public:
	Cone(double x = 0, double y = 0, double z = 0, double r = 0, double h = 0) : ThreeDimensionShape(x, y, z, x+r, y, z), C(Point(x,y,z+h)) { std::cout << "Cone Constructor" << std::endl << std::endl; }
	Cone(Point point, double r = 0, double h = 0) : ThreeDimensionShape(point, Point(point.x+r,point.y,point.z)), C(Point(point.x, point.y, point.z+h)) { std::cout << "Cone Constructor" << std::endl << std::endl; }
	~Cone() { std::cout << "Cone Destructor" << std::endl; }
	Point* GetPoints();
	void Show();
	double GetArea();
	double GetVolume();
};
