#include <iostream>
#include <map>
#include "TwoDimensionShape.h"
#include "Circle.h"
#include "Sphere.h"
#include "Triangle.h"
#include "Cone.h"
using namespace std;

int main()
{
	/*Circle circle = Circle(1, 1, 2);
	Sphere sphere = Sphere(Point(), 5);
	Triangle triangle = Triangle(Point(), Point(1, 1), Point(2, 3));
	Cone cone = Cone(Point(), 3, 4);
	/*circle.Show();
	triangle.Show();
	sphere.Show();
	cone.Show();*/
	Shape* test[4] = {new Circle(1,2,3), new Triangle(Point(), Point(1,1), Point(2,3)), new Cone(Point(), 3, 4), new Sphere(Point(), 5)};

	for (int i = 0; i < 4; ++i) test[i]->Show();

	for(int i=0; i<4; ++i) delete test[i];

	return 0;
}