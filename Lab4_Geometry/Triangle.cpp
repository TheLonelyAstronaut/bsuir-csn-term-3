#include "Triangle.h"
#include "Point.h"
#include <iostream>
#include <map>
#include <sstream>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

Point* Triangle::GetPoints()
{
	Point* info = new Point[3];
	info[0] = A;
	info[1] = B;
	info[2] = C;
	return info;
}

void Triangle::Show()
{
	ostringstream strs;
	strs << A.x;
	cout << "Shape type: Triangle" << endl << "A Point: " + strs.str() + ", ";
	strs.str("");
	strs << A.y;
	cout << strs.str() << endl << "B Point: ";
	strs.str("");
	strs << B.x;
	cout << strs.str() << ", ";
	strs.str("");
	strs << B.y;
	cout << strs.str() << endl << "C Point: ";
	strs.str("");
	strs << C.x;
	cout << strs.str() << ", ";
	strs.str("");
	cout << C.y << endl << "Area: " << GetArea() << endl << "Perimeter: " << GetPerimeter() << endl << endl;
}

double Triangle::GetArea()
{
	double p = GetPerimeter() / 2;
	return sqrt(p * (p - Point::GetSideLength(A, B)) * (p - Point::GetSideLength(B, C)) * (p - Point::GetSideLength(A, C)));
}

double Triangle::GetPerimeter()
{
	return Point::GetSideLength(A, B) + Point::GetSideLength(B, C) + Point::GetSideLength(A, C);
}